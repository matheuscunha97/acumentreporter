﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bunifu.Framework.UI;
using System.Windows.Forms;

namespace Acument_Reports.System_Variables
{
    class Filters_Parameters
    {
        // --------------- VARIABLES ------------- \\

        //Date filters variables
        private static BunifuDatepicker _date_From;

        private static BunifuDatepicker _date_To;

        private static BunifuCheckbox _cb_Days;

        private static CheckBox _cb_allDepartments;

        private static CheckBox _cb_allConcepts;

        private static CheckBox _cb_allLabors;

        private static string _query;

        private static string _summary_query;

        private static string _DepartmentsFilters;
                              
        private static string _ConceptsFilters;
                              
        private static string _LaborsFilters;

        private static string _TodosDepartments;

        private static string _TodosConcepts;

        private static string _TodosLabors;
        //End date filters variables


        // --------------- END VARIABLES ------------- \\


        // --------------- METHODS TO GET AND SET VALUES ------------- \\

        public static BunifuDatepicker date_From
        {
            get { return Filters_Parameters._date_From; }

            set { Filters_Parameters._date_From = value; }
        }

        public static BunifuDatepicker date_To
        {
            get { return Filters_Parameters._date_To; }

            set { Filters_Parameters._date_To = value; }

        }

        public static BunifuCheckbox checkBox_Days
        {
            get { return Filters_Parameters._cb_Days; }

            set { Filters_Parameters._cb_Days = value; }

        }

        public static string filter_query
        {
            get { return Filters_Parameters._query; }
            set { Filters_Parameters._query = value; }
        } //store the filter query

        public static string summary_query
        {
            get { return Filters_Parameters._summary_query; }
            set { Filters_Parameters._summary_query = value; }
        } //store the summary query

        public static string selected_DepartmentsFilters
        {
            get { return Filters_Parameters._DepartmentsFilters; }
            set { Filters_Parameters._DepartmentsFilters = value; }
        }

        public static string selected_ConceptsFilters
        {
            get { return Filters_Parameters._ConceptsFilters; }
            set { Filters_Parameters._ConceptsFilters = value; }
        }

        public static string selected_LaborsFilters
        {
            get { return Filters_Parameters._LaborsFilters; }
            set { Filters_Parameters._LaborsFilters = value; }
        }

        public static string todos_Departments
        {
            get { return Filters_Parameters._TodosDepartments; }
            set { Filters_Parameters._TodosDepartments = value; }
        }

        public static string todos_Concepts
        {
            get { return Filters_Parameters._TodosConcepts; }
            set { Filters_Parameters._TodosConcepts = value; }
        }

        public static string todos_Labors
        {
            get { return Filters_Parameters._TodosLabors; }
            set { Filters_Parameters._TodosLabors = value; }
        }

        public static CheckBox cb_allDepartments
        {
            get { return Filters_Parameters._cb_allDepartments; }
            set { Filters_Parameters._cb_allDepartments = value; }
        }

        public static CheckBox cb_allConcepts
        {
            get { return Filters_Parameters._cb_allConcepts; }
            set { Filters_Parameters._cb_allConcepts = value; }
        }

        public static CheckBox cb_allLabors
        {
            get { return Filters_Parameters._cb_allLabors; }
            set { Filters_Parameters._cb_allLabors = value; }
        }
        // --------------- END METHODS ------------- \\
    }
}
