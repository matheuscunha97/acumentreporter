﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acument_Reports
{
    class SystemSession
    {
        private static Int32 _userId;

        private static String _userName;

        private static String _userPass;

        public static Int32 UserID

        {

            get { return SystemSession._userId; }

            set { SystemSession._userId = value; }

        }


        public static String Username

        {

            get { return SystemSession._userName; }

            set { SystemSession._userName = value; }

        }


        public static String Password

        {

            get { return SystemSession._userPass; }

            set { SystemSession._userPass = value; }

        }
    }
}
