﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Acument_Reports;

namespace Acument_Reports_Test
{
    public partial class frmAdminDashboard : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmAdminDashboard()
        {
            InitializeComponent();
            this.CenterToScreen();
    }

        private void lblClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void frmMainScreen_Load(object sender, EventArgs e)
        {
            
        }

        private void frmMainScreen_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void frmMainScreen_Shown(object sender, EventArgs e)
        {
            
        }

        private void btnCreatUser_Click(object sender, EventArgs e)
        {
            frmCreateUser createUser = new frmCreateUser();
            createUser.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmDeleteUser deleteUser = new frmDeleteUser();
            deleteUser.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmChangePasswords changePassword = new frmChangePasswords();
            changePassword.Show();
        }

        private void btnReports_Click(object sender, EventArgs e)
        {
            frmReports reports = new frmReports();
            reports.Show();
        }

        private void frmAdminDashboard_Shown(object sender, EventArgs e)
        {
            lblWelcomeUser.Text = "Welcome, " + SystemSession.Username.ToUpper() + "!";
        }
    }
}
