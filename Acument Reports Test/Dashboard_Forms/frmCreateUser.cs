﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Acument_Reports;

namespace Acument_Reports_Test
{
    public partial class frmCreateUser : Form
    {
        DBConnection dataBaseConn = new DBConnection();
        public frmCreateUser()
        {
            InitializeComponent();
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            frmCreateUser newUser = new frmCreateUser();
            newUser.Close();
        }

        private void lblMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void frmNewUser_Shown(object sender, EventArgs e)
        {
            this.CenterToScreen();
            this.ActiveControl = txtCreateUsername;
            txtCreatePassword._TextBox.PasswordChar = '*';
            txtPassConfirm._TextBox.PasswordChar = '*';
        }

        private void lblClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblMinimize_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void cleanCreateUsersFields(Bunifu.Framework.UI.BunifuTextbox userField, Bunifu.Framework.UI.BunifuTextbox passField, Bunifu.Framework.UI.BunifuTextbox confirmField)
        {
            userField._TextBox.Clear();
            passField._TextBox.Clear();
            confirmField._TextBox.Clear();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if(txtCreateUsername.text != "admin")//Create validation to don't allow the creation of admin users
            {
                if(txtCreatePassword.text == txtPassConfirm.text) //Check if the typed passwords are equal
                {
                    if(string.IsNullOrWhiteSpace(txtCreateUsername.text) || string.IsNullOrWhiteSpace(txtCreatePassword.text) || string.IsNullOrWhiteSpace(txtPassConfirm.text)) //Check if is there some blank field.
                    {
                        MessageBox.Show("The fields can't be blank.", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cleanCreateUsersFields(txtCreateUsername, txtCreatePassword, txtPassConfirm);
                    }
                    else //Everything ok, try to create the user.
                    {
                        using (NpgsqlConnection conn = dataBaseConn.Open())
                        {
                            try
                            {
                                NpgsqlDataAdapter pda = new NpgsqlDataAdapter(string.Format("SELECT COUNT(*) FROM users WHERE username='{0}'", txtCreateUsername.text), conn); //Check if the user already exists
                                DataTable dt = new DataTable();
                                pda.Fill(dt);
                                if (dt.Rows[0][0].ToString() == "1") //If yes, display a message
                                {
                                    MessageBox.Show("This username is already in use, please choose another one.", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    cleanCreateUsersFields(txtCreateUsername, txtCreatePassword, txtPassConfirm);
                                    txtCreateUsername.Focus();
                                }
                                else //If not, create the user
                                {
                                    NpgsqlCommand queryInsert = new NpgsqlCommand(string.Format("INSERT INTO users(username,password) VALUES ('{0}','{1}')", txtCreateUsername.text, txtCreatePassword.text), conn);
                                    int i = queryInsert.ExecuteNonQuery();

                                    if (i > 0)
                                    {
                                        MessageBox.Show("User successfully created!", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        cleanCreateUsersFields(txtCreateUsername, txtCreatePassword, txtPassConfirm);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("The typed passwords do not match. Try again.", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cleanCreateUsersFields(txtCreateUsername, txtCreatePassword, txtPassConfirm);
                }
            }
            else
            {
                MessageBox.Show("You can't create another ADMIN user.", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cleanCreateUsersFields(txtCreateUsername, txtCreatePassword, txtPassConfirm);
            }
        }
    }
}
