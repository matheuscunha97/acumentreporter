﻿namespace Acument_Reports_Test
{
    partial class frmChangePasswords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChangePasswords));
            this.btnChangePassword = new Bunifu.Framework.UI.BunifuThinButton2();
            this.lbl_dpdDeleteUser = new System.Windows.Forms.Label();
            this.lbl_ChangePasswordTitle = new System.Windows.Forms.Label();
            this.txtChangePassword = new Bunifu.Framework.UI.BunifuTextbox();
            this.txtConfirmPassword = new Bunifu.Framework.UI.BunifuTextbox();
            this.lblNewPassword = new System.Windows.Forms.Label();
            this.lblConfirmNewPassword = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMinimize = new System.Windows.Forms.Label();
            this.lblClose = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.cmb_ChangePassword = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.ActiveBorderThickness = 1;
            this.btnChangePassword.ActiveCornerRadius = 20;
            this.btnChangePassword.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnChangePassword.ActiveForecolor = System.Drawing.Color.White;
            this.btnChangePassword.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnChangePassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangePassword.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnChangePassword.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnChangePassword.BackgroundImage")));
            this.btnChangePassword.ButtonText = "Save";
            this.btnChangePassword.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangePassword.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangePassword.ForeColor = System.Drawing.Color.White;
            this.btnChangePassword.IdleBorderThickness = 1;
            this.btnChangePassword.IdleCornerRadius = 20;
            this.btnChangePassword.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnChangePassword.IdleForecolor = System.Drawing.Color.White;
            this.btnChangePassword.IdleLineColor = System.Drawing.Color.White;
            this.btnChangePassword.Location = new System.Drawing.Point(73, 408);
            this.btnChangePassword.Margin = new System.Windows.Forms.Padding(5);
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Size = new System.Drawing.Size(253, 49);
            this.btnChangePassword.TabIndex = 21;
            this.btnChangePassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
            // 
            // lbl_dpdDeleteUser
            // 
            this.lbl_dpdDeleteUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_dpdDeleteUser.AutoSize = true;
            this.lbl_dpdDeleteUser.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dpdDeleteUser.Location = new System.Drawing.Point(70, 137);
            this.lbl_dpdDeleteUser.Name = "lbl_dpdDeleteUser";
            this.lbl_dpdDeleteUser.Size = new System.Drawing.Size(100, 17);
            this.lbl_dpdDeleteUser.TabIndex = 20;
            this.lbl_dpdDeleteUser.Text = "Select an user:";
            // 
            // lbl_ChangePasswordTitle
            // 
            this.lbl_ChangePasswordTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_ChangePasswordTitle.AutoSize = true;
            this.lbl_ChangePasswordTitle.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_ChangePasswordTitle.Font = new System.Drawing.Font("Century Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ChangePasswordTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.lbl_ChangePasswordTitle.Location = new System.Drawing.Point(23, 41);
            this.lbl_ChangePasswordTitle.Name = "lbl_ChangePasswordTitle";
            this.lbl_ChangePasswordTitle.Size = new System.Drawing.Size(347, 44);
            this.lbl_ChangePasswordTitle.TabIndex = 19;
            this.lbl_ChangePasswordTitle.Text = "Change Password";
            // 
            // txtChangePassword
            // 
            this.txtChangePassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChangePassword.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtChangePassword.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtChangePassword.BackgroundImage")));
            this.txtChangePassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.txtChangePassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.txtChangePassword.Icon = ((System.Drawing.Image)(resources.GetObject("txtChangePassword.Icon")));
            this.txtChangePassword.Location = new System.Drawing.Point(73, 245);
            this.txtChangePassword.Name = "txtChangePassword";
            this.txtChangePassword.Size = new System.Drawing.Size(253, 32);
            this.txtChangePassword.TabIndex = 22;
            this.txtChangePassword.text = "";
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConfirmPassword.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtConfirmPassword.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtConfirmPassword.BackgroundImage")));
            this.txtConfirmPassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.txtConfirmPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.txtConfirmPassword.Icon = ((System.Drawing.Image)(resources.GetObject("txtConfirmPassword.Icon")));
            this.txtConfirmPassword.Location = new System.Drawing.Point(73, 317);
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.Size = new System.Drawing.Size(253, 32);
            this.txtConfirmPassword.TabIndex = 23;
            this.txtConfirmPassword.text = "";
            // 
            // lblNewPassword
            // 
            this.lblNewPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNewPassword.AutoSize = true;
            this.lblNewPassword.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewPassword.Location = new System.Drawing.Point(70, 225);
            this.lblNewPassword.Name = "lblNewPassword";
            this.lblNewPassword.Size = new System.Drawing.Size(107, 17);
            this.lblNewPassword.TabIndex = 24;
            this.lblNewPassword.Text = "New Password:";
            // 
            // lblConfirmNewPassword
            // 
            this.lblConfirmNewPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblConfirmNewPassword.AutoSize = true;
            this.lblConfirmNewPassword.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfirmNewPassword.Location = new System.Drawing.Point(70, 297);
            this.lblConfirmNewPassword.Name = "lblConfirmNewPassword";
            this.lblConfirmNewPassword.Size = new System.Drawing.Size(163, 17);
            this.lblConfirmNewPassword.TabIndex = 25;
            this.lblConfirmNewPassword.Text = "Confirm New Password:";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.panel1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Location = new System.Drawing.Point(-10, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(419, 5);
            this.panel1.TabIndex = 26;
            // 
            // lblMinimize
            // 
            this.lblMinimize.AutoSize = true;
            this.lblMinimize.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblMinimize.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinimize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.lblMinimize.Location = new System.Drawing.Point(356, 7);
            this.lblMinimize.Name = "lblMinimize";
            this.lblMinimize.Size = new System.Drawing.Size(18, 21);
            this.lblMinimize.TabIndex = 28;
            this.lblMinimize.Text = "_";
            this.lblMinimize.Click += new System.EventHandler(this.lblMinimize_Click);
            // 
            // lblClose
            // 
            this.lblClose.AutoSize = true;
            this.lblClose.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblClose.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.lblClose.Location = new System.Drawing.Point(381, 9);
            this.lblClose.Name = "lblClose";
            this.lblClose.Size = new System.Drawing.Size(19, 21);
            this.lblClose.TabIndex = 27;
            this.lblClose.Text = "X";
            this.lblClose.Click += new System.EventHandler(this.lblClose_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(2, 514);
            this.panel2.TabIndex = 29;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(2, 512);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(406, 2);
            this.panel3.TabIndex = 30;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(406, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(2, 512);
            this.panel4.TabIndex = 31;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(2, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(404, 2);
            this.panel5.TabIndex = 32;
            // 
            // cmb_ChangePassword
            // 
            this.cmb_ChangePassword.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmb_ChangePassword.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ChangePassword.FormattingEnabled = true;
            this.cmb_ChangePassword.Location = new System.Drawing.Point(73, 157);
            this.cmb_ChangePassword.Name = "cmb_ChangePassword";
            this.cmb_ChangePassword.Size = new System.Drawing.Size(253, 21);
            this.cmb_ChangePassword.TabIndex = 36;
            this.cmb_ChangePassword.SelectedIndexChanged += new System.EventHandler(this.cmb_ChangePassword_SelectedIndexChanged);
            // 
            // frmChangePasswords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(408, 514);
            this.Controls.Add(this.cmb_ChangePassword);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblMinimize);
            this.Controls.Add(this.lblClose);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblConfirmNewPassword);
            this.Controls.Add(this.lblNewPassword);
            this.Controls.Add(this.txtConfirmPassword);
            this.Controls.Add(this.txtChangePassword);
            this.Controls.Add(this.btnChangePassword);
            this.Controls.Add(this.lbl_dpdDeleteUser);
            this.Controls.Add(this.lbl_ChangePasswordTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmChangePasswords";
            this.Text = "Change User Passwords";
            this.Shown += new System.EventHandler(this.frmChangePasswords_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 btnChangePassword;
        private System.Windows.Forms.Label lbl_dpdDeleteUser;
        private System.Windows.Forms.Label lbl_ChangePasswordTitle;
        private Bunifu.Framework.UI.BunifuTextbox txtChangePassword;
        private Bunifu.Framework.UI.BunifuTextbox txtConfirmPassword;
        private System.Windows.Forms.Label lblNewPassword;
        private System.Windows.Forms.Label lblConfirmNewPassword;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblMinimize;
        private System.Windows.Forms.Label lblClose;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox cmb_ChangePassword;
    }
}