﻿using Acument_Reports;
using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acument_Reports_Test
{
    public partial class frmDeleteUser : Form
    {
        DBConnection dataBaseConn = new DBConnection();
        public frmDeleteUser()
        {
            InitializeComponent();
        }

        private void frmDeleteUser_Shown(object sender, EventArgs e)
        {
            this.CenterToScreen();

            using (NpgsqlConnection conn = dataBaseConn.Open())
            {
                try
                {
                    string query = "SELECT * FROM users WHERE username NOT LIKE '%admin%' ORDER BY username ASC";
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(query, conn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Users");
                    cmb_DeleteUser.DisplayMember = "username";
                    cmb_DeleteUser.ValueMember = "user_id";
                    cmb_DeleteUser.DataSource = ds.Tables["Users"];
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }//Fill Users combobox

            cmb_DeleteUser.SelectedIndex = -1;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure want to delete the user '" + cmb_DeleteUser.Text.ToUpper() + "'?", System.Windows.Forms.Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if(result == DialogResult.Yes)
            {
                using (NpgsqlConnection conn = dataBaseConn.Open())
                {
                    try
                    {
                        NpgsqlCommand queryDelete = new NpgsqlCommand(string.Format("DELETE FROM users WHERE username = '{0}'", cmb_DeleteUser.Text), conn);
                        int i = queryDelete.ExecuteNonQuery();

                        if (i > 0)
                        {
                            MessageBox.Show("The user " + cmb_DeleteUser.Text.ToUpper() + " has been deleted!", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            try
                            {
                                string query = "SELECT * FROM users WHERE username NOT LIKE '%admin%' ORDER BY username ASC";
                                NpgsqlDataAdapter da = new NpgsqlDataAdapter(query, conn);
                                DataSet ds = new DataSet();
                                da.Fill(ds, "Users");
                                cmb_DeleteUser.DisplayMember = "username";
                                cmb_DeleteUser.ValueMember = "user_id";
                                cmb_DeleteUser.DataSource = ds.Tables["Users"];
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }//Refresh Users combobox

                            cmb_DeleteUser.SelectedIndex = -1; //Clean the combobox value
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
