﻿using Acument_Reports;
using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acument_Reports_Test
{
    public partial class frmLogin : Form
    {
        DBConnection dataBaseConn = new DBConnection();
        public frmLogin()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            this.ActiveControl = txtUsername;
            txtPassword._TextBox.PasswordChar = '*';

            //dataBaseConn.OpenSQLConn();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            //Setting the variables for create the user session
            SystemSession.Username = txtUsername.text;
            SystemSession.Password = txtPassword.text;

            try
            {
                using (NpgsqlConnection conn = dataBaseConn.Open()) //Open connection
                {
                    NpgsqlDataAdapter pda = new NpgsqlDataAdapter("SELECT COUNT(*) FROM users WHERE username='" + SystemSession.Username + "' and password ='" + SystemSession.Password + "'", conn);

                    DataTable dt = new DataTable();
                    pda.Fill(dt);


                    if (dt.Rows[0][0].ToString() == "1" && SystemSession.Username == "admin") //Check if the user is admin
                    {
                        //MessageBox.Show("Welcome "+ SystemSession.Username + "!", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        frmAdminDashboard adminDashboard = new frmAdminDashboard();
                        adminDashboard.Show();
                        this.Hide();
                    }
                    else if (dt.Rows[0][0].ToString() == "1" && SystemSession.Username != "admin") //Check if is normal user
                    {
                        //MessageBox.Show("Welcome " + SystemSession.Username + "!", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        frmNormalDashboard normalDashboard = new frmNormalDashboard();
                        normalDashboard.Show();
                        this.Hide();
                    }
                    else //User or pass not correct
                    {
                        MessageBox.Show("Username or password not correct!", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtUsername.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                //Message if there's an error
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void OnMouseEnter(object sender, EventArgs e)
        {
            //lblClose.ForeColor = new Color();
        }

        private void OnMouseLeave(object sender, EventArgs e)
        {
            //lblClose
        }

        private void lblClose_MouseEnter(object sender, EventArgs e)
        {
            lblClose.Font = new Font(lblClose.Font.Name, lblClose.Font.SizeInPoints, FontStyle.Underline);
        }

        private void lblClose_MouseLeave(object sender, EventArgs e)
        {
            lblClose.Font = new Font(lblClose.Font.Name, lblClose.Font.SizeInPoints, FontStyle.Regular);
        }

        private void txtPassword_KeyPress(object sender, EventArgs e)
        {
            
        }

        private void txtPassword_OnTextChange(object sender, EventArgs e)
        {
            txtPassword.text = txtPassword.text.Replace(Environment.NewLine, "");
        }
    }
}
