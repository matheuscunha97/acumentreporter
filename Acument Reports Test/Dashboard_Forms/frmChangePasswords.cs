﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Acument_Reports;

namespace Acument_Reports_Test
{
    public partial class frmChangePasswords : Form
    {
        DBConnection dataBaseConn = new DBConnection();
        public frmChangePasswords()
        {
            InitializeComponent();
        }

        private void showTextBoxes(Bunifu.Framework.UI.BunifuTextbox passField, Bunifu.Framework.UI.BunifuTextbox confirmField)
        {
            //Password
            lblNewPassword.Show();
            passField.Show();

            //Confirmation
            lblConfirmNewPassword.Show();
            confirmField.Show();
        }

        private void hideTextBoxes(Bunifu.Framework.UI.BunifuTextbox passField, Bunifu.Framework.UI.BunifuTextbox confirmField)
        {
            //Password
            lblNewPassword.Hide();
            passField.Hide();

            //Confirmation
            lblConfirmNewPassword.Hide();
            confirmField.Hide();
        }

        private void frmChangePasswords_Shown(object sender, EventArgs e)
        {
            this.CenterToScreen();

            txtChangePassword._TextBox.PasswordChar = '*';
            txtConfirmPassword._TextBox.PasswordChar = '*';

            using (NpgsqlConnection conn = dataBaseConn.Open())
            {
                try
                {
                    string query = "SELECT * FROM users ORDER BY username ASC";
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(query, conn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Users");
                    cmb_ChangePassword.DisplayMember = "username";
                    cmb_ChangePassword.ValueMember = "user_id";
                    cmb_ChangePassword.DataSource = ds.Tables["Users"];
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }//Fill Users combobox

            cmb_ChangePassword.SelectedIndex = -1; //Make the combobox start in blank

            //Hiding the textboxes
            hideTextBoxes(txtChangePassword, txtConfirmPassword);
        }

        private void lblMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cleanChangePassFields(Bunifu.Framework.UI.BunifuTextbox passField, Bunifu.Framework.UI.BunifuTextbox confirmField)
        {
            passField._TextBox.Clear();
            confirmField._TextBox.Clear();
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            if(txtChangePassword.text == txtConfirmPassword.text) //Check if the typed passwords are equal
            {
                if(string.IsNullOrWhiteSpace(txtChangePassword.text) || string.IsNullOrWhiteSpace(txtConfirmPassword.text)) //Check if the passwords are blank
                {
                    MessageBox.Show("The fields can't be blank.", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cleanChangePassFields(txtChangePassword, txtConfirmPassword);
                }
                else //If not, try to change the passwords
                {
                    using (NpgsqlConnection conn = dataBaseConn.Open())
                    {
                        try
                        {
                            NpgsqlCommand queryDelete = new NpgsqlCommand(string.Format("UPDATE users SET password='{0}' WHERE username='{1}'", txtChangePassword.text,cmb_ChangePassword.Text), conn);
                            int i = queryDelete.ExecuteNonQuery();

                            if (i > 0)
                            {
                                MessageBox.Show("The password for " + cmb_ChangePassword.Text.ToUpper() + " has been changed!", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                cleanChangePassFields(txtChangePassword, txtConfirmPassword);
                                cmb_ChangePassword.SelectedIndex = -1; //Clean the combobox value
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("The typed passwords do not match. Try again.", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cleanChangePassFields(txtChangePassword, txtConfirmPassword);
            }
        }

        private void cmb_ChangePassword_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_ChangePassword.SelectedIndex != -1) //Mean that the user selected some name to change the password, then show the textboxes
            {
                showTextBoxes(txtChangePassword, txtConfirmPassword);
            }
        }
    }
}
