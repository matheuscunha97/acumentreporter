﻿namespace Acument_Reports_Test
{
    partial class frmReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReports));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblClose = new System.Windows.Forms.Label();
            this.lblMinimize = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnTimer = new Bunifu.Framework.UI.BunifuThinButton2();
            this.lblTimerDesc = new System.Windows.Forms.Label();
            this.lblTimer = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnManager = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label1 = new System.Windows.Forms.Label();
            this.lblManager = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnFinancials = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFinancials = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(2, 471);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(2, 469);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(615, 2);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(615, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(2, 469);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(2, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(613, 2);
            this.panel4.TabIndex = 3;
            // 
            // lblClose
            // 
            this.lblClose.AutoSize = true;
            this.lblClose.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblClose.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.lblClose.Location = new System.Drawing.Point(586, 9);
            this.lblClose.Name = "lblClose";
            this.lblClose.Size = new System.Drawing.Size(19, 21);
            this.lblClose.TabIndex = 6;
            this.lblClose.Text = "X";
            this.lblClose.Click += new System.EventHandler(this.lblClose_Click);
            // 
            // lblMinimize
            // 
            this.lblMinimize.AutoSize = true;
            this.lblMinimize.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblMinimize.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinimize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.lblMinimize.Location = new System.Drawing.Point(562, 7);
            this.lblMinimize.Name = "lblMinimize";
            this.lblMinimize.Size = new System.Drawing.Size(18, 21);
            this.lblMinimize.TabIndex = 7;
            this.lblMinimize.Text = "_";
            this.lblMinimize.Click += new System.EventHandler(this.lblMinimize_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.panel5.Controls.Add(this.btnTimer);
            this.panel5.Controls.Add(this.lblTimerDesc);
            this.panel5.Controls.Add(this.lblTimer);
            this.panel5.Controls.Add(this.pictureBox3);
            this.panel5.Location = new System.Drawing.Point(28, 54);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(183, 387);
            this.panel5.TabIndex = 8;
            // 
            // btnTimer
            // 
            this.btnTimer.ActiveBorderThickness = 1;
            this.btnTimer.ActiveCornerRadius = 20;
            this.btnTimer.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnTimer.ActiveForecolor = System.Drawing.Color.White;
            this.btnTimer.ActiveLineColor = System.Drawing.Color.White;
            this.btnTimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnTimer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTimer.BackgroundImage")));
            this.btnTimer.ButtonText = "Show Report";
            this.btnTimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTimer.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnTimer.IdleBorderThickness = 1;
            this.btnTimer.IdleCornerRadius = 20;
            this.btnTimer.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnTimer.IdleForecolor = System.Drawing.Color.White;
            this.btnTimer.IdleLineColor = System.Drawing.Color.White;
            this.btnTimer.Location = new System.Drawing.Point(7, 275);
            this.btnTimer.Margin = new System.Windows.Forms.Padding(5);
            this.btnTimer.Name = "btnTimer";
            this.btnTimer.Size = new System.Drawing.Size(167, 43);
            this.btnTimer.TabIndex = 8;
            this.btnTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnTimer.Click += new System.EventHandler(this.btnTimer_Click);
            this.btnTimer.MouseEnter += new System.EventHandler(this.btnTimer_MouseEnter);
            this.btnTimer.MouseLeave += new System.EventHandler(this.btnTimer_MouseLeave);
            // 
            // lblTimerDesc
            // 
            this.lblTimerDesc.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimerDesc.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTimerDesc.Location = new System.Drawing.Point(16, 144);
            this.lblTimerDesc.Name = "lblTimerDesc";
            this.lblTimerDesc.Size = new System.Drawing.Size(151, 82);
            this.lblTimerDesc.TabIndex = 4;
            this.lblTimerDesc.Text = "Create a report to control the time flow of each area.";
            this.lblTimerDesc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimer.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTimer.Location = new System.Drawing.Point(49, 96);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(91, 36);
            this.lblTimer.TabIndex = 3;
            this.lblTimer.Text = "Timer";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Location = new System.Drawing.Point(64, 38);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(58, 55);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panel6.Controls.Add(this.btnManager);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.lblManager);
            this.panel6.Controls.Add(this.pictureBox2);
            this.panel6.Location = new System.Drawing.Point(217, 54);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(183, 387);
            this.panel6.TabIndex = 9;
            // 
            // btnManager
            // 
            this.btnManager.ActiveBorderThickness = 1;
            this.btnManager.ActiveCornerRadius = 20;
            this.btnManager.ActiveFillColor = System.Drawing.Color.LightSeaGreen;
            this.btnManager.ActiveForecolor = System.Drawing.Color.White;
            this.btnManager.ActiveLineColor = System.Drawing.Color.White;
            this.btnManager.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnManager.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnManager.BackgroundImage")));
            this.btnManager.ButtonText = "Coming Soon";
            this.btnManager.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnManager.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManager.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.btnManager.IdleBorderThickness = 1;
            this.btnManager.IdleCornerRadius = 20;
            this.btnManager.IdleFillColor = System.Drawing.Color.LightSeaGreen;
            this.btnManager.IdleForecolor = System.Drawing.Color.White;
            this.btnManager.IdleLineColor = System.Drawing.Color.White;
            this.btnManager.Location = new System.Drawing.Point(7, 275);
            this.btnManager.Margin = new System.Windows.Forms.Padding(5);
            this.btnManager.Name = "btnManager";
            this.btnManager.Size = new System.Drawing.Size(167, 43);
            this.btnManager.TabIndex = 9;
            this.btnManager.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnManager.Click += new System.EventHandler(this.btnManager_Click);
            this.btnManager.MouseEnter += new System.EventHandler(this.btnManager_MouseEnter);
            this.btnManager.MouseLeave += new System.EventHandler(this.btnManager_MouseLeave);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(20, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 82);
            this.label1.TabIndex = 5;
            this.label1.Text = "Create a report for each department of your plant.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblManager
            // 
            this.lblManager.AutoSize = true;
            this.lblManager.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManager.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblManager.Location = new System.Drawing.Point(18, 96);
            this.lblManager.Name = "lblManager";
            this.lblManager.Size = new System.Drawing.Size(150, 36);
            this.lblManager.TabIndex = 3;
            this.lblManager.Text = "Manager";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(54, 27);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(70, 75);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.SeaGreen;
            this.panel7.Controls.Add(this.btnFinancials);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.lblFinancials);
            this.panel7.Controls.Add(this.pictureBox1);
            this.panel7.Location = new System.Drawing.Point(406, 54);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(183, 387);
            this.panel7.TabIndex = 9;
            // 
            // btnFinancials
            // 
            this.btnFinancials.ActiveBorderThickness = 1;
            this.btnFinancials.ActiveCornerRadius = 20;
            this.btnFinancials.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.btnFinancials.ActiveForecolor = System.Drawing.Color.White;
            this.btnFinancials.ActiveLineColor = System.Drawing.Color.White;
            this.btnFinancials.BackColor = System.Drawing.Color.SeaGreen;
            this.btnFinancials.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFinancials.BackgroundImage")));
            this.btnFinancials.ButtonText = "Coming Soon";
            this.btnFinancials.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFinancials.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinancials.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnFinancials.IdleBorderThickness = 1;
            this.btnFinancials.IdleCornerRadius = 20;
            this.btnFinancials.IdleFillColor = System.Drawing.Color.SeaGreen;
            this.btnFinancials.IdleForecolor = System.Drawing.Color.White;
            this.btnFinancials.IdleLineColor = System.Drawing.Color.White;
            this.btnFinancials.Location = new System.Drawing.Point(7, 275);
            this.btnFinancials.Margin = new System.Windows.Forms.Padding(5);
            this.btnFinancials.Name = "btnFinancials";
            this.btnFinancials.Size = new System.Drawing.Size(167, 43);
            this.btnFinancials.TabIndex = 10;
            this.btnFinancials.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnFinancials.MouseEnter += new System.EventHandler(this.btnFinancials_MouseEnter);
            this.btnFinancials.MouseLeave += new System.EventHandler(this.btnFinancials_MouseLeave);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(20, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 82);
            this.label2.TabIndex = 6;
            this.label2.Text = "Create a report to have an overview of your finances.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblFinancials
            // 
            this.lblFinancials.AutoSize = true;
            this.lblFinancials.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinancials.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblFinancials.Location = new System.Drawing.Point(18, 96);
            this.lblFinancials.Name = "lblFinancials";
            this.lblFinancials.Size = new System.Drawing.Size(155, 36);
            this.lblFinancials.TabIndex = 1;
            this.lblFinancials.Text = "Financials";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(60, 38);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 55);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // frmReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(617, 471);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.lblMinimize);
            this.Controls.Add(this.lblClose);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmReports";
            this.Text = "frmReports";
            this.Shown += new System.EventHandler(this.frmReports_Shown);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblClose;
        private System.Windows.Forms.Label lblMinimize;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblFinancials;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblManager;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lblTimerDesc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuThinButton2 btnTimer;
        private Bunifu.Framework.UI.BunifuThinButton2 btnManager;
        private Bunifu.Framework.UI.BunifuThinButton2 btnFinancials;
    }
}