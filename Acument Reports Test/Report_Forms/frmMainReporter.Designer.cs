﻿namespace Acument_Reports_Test
{
    partial class frmMainReporter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainReporter));
            this.lst_Report = new System.Windows.Forms.ListView();
            this.field_one = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.field_two = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.field_three = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.account = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.area = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.department = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labor_type = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pay_code = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.money = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hours = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.wages = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.days = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.date_begin = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.date_end = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnLoad = new Bunifu.Framework.UI.BunifuThinButton2();
            this.lblExportToCSV = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.dtp_From = new Bunifu.Framework.UI.BunifuDatepicker();
            this.lblFrom = new System.Windows.Forms.Label();
            this.grpFilters = new System.Windows.Forms.GroupBox();
            this.lbl_cb_days = new System.Windows.Forms.Label();
            this.cb_Days = new Bunifu.Framework.UI.BunifuCheckbox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmb_PayCode = new System.Windows.Forms.ComboBox();
            this.lblCostCenter = new System.Windows.Forms.Label();
            this.cmb_Department = new System.Windows.Forms.ComboBox();
            this.dtp_To = new Bunifu.Framework.UI.BunifuDatepicker();
            this.btnFilters = new Bunifu.Framework.UI.BunifuImageButton();
            this.lblTo = new System.Windows.Forms.Label();
            this.btnExportToCSV = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnExportToPDF = new Bunifu.Framework.UI.BunifuImageButton();
            this.lblExportToPDF = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnExportToExcel = new Bunifu.Framework.UI.BunifuImageButton();
            this.lblExportToExcel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.grpFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToCSV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToPDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToExcel)).BeginInit();
            this.SuspendLayout();
            // 
            // lst_Report
            // 
            this.lst_Report.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lst_Report.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.field_one,
            this.field_two,
            this.field_three,
            this.account,
            this.area,
            this.department,
            this.labor_type,
            this.pay_code,
            this.money,
            this.hours,
            this.wages,
            this.days,
            this.date_begin,
            this.date_end});
            this.lst_Report.GridLines = true;
            this.lst_Report.Location = new System.Drawing.Point(12, 129);
            this.lst_Report.MultiSelect = false;
            this.lst_Report.Name = "lst_Report";
            this.lst_Report.Size = new System.Drawing.Size(759, 412);
            this.lst_Report.TabIndex = 1;
            this.lst_Report.UseCompatibleStateImageBehavior = false;
            this.lst_Report.View = System.Windows.Forms.View.Details;
            // 
            // field_one
            // 
            this.field_one.Text = "First Field";
            // 
            // field_two
            // 
            this.field_two.Text = "Field Two";
            // 
            // field_three
            // 
            this.field_three.Text = "Third Field";
            // 
            // account
            // 
            this.account.Text = "Account";
            this.account.Width = 82;
            // 
            // area
            // 
            this.area.Text = "Area";
            // 
            // department
            // 
            this.department.Text = "Department";
            this.department.Width = 75;
            // 
            // labor_type
            // 
            this.labor_type.Text = "Labor Type";
            // 
            // pay_code
            // 
            this.pay_code.Text = "Pay Code";
            // 
            // money
            // 
            this.money.Text = "Money";
            // 
            // hours
            // 
            this.hours.Text = "Hours";
            // 
            // wages
            // 
            this.wages.Text = "Wages";
            // 
            // days
            // 
            this.days.Text = "Days";
            // 
            // date_begin
            // 
            this.date_begin.Text = "Date Start";
            this.date_begin.Width = 80;
            // 
            // date_end
            // 
            this.date_end.Text = "Date Finish";
            this.date_end.Width = 80;
            // 
            // btnLoad
            // 
            this.btnLoad.ActiveBorderThickness = 1;
            this.btnLoad.ActiveCornerRadius = 20;
            this.btnLoad.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnLoad.ActiveForecolor = System.Drawing.Color.White;
            this.btnLoad.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoad.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLoad.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLoad.BackgroundImage")));
            this.btnLoad.ButtonText = "Load";
            this.btnLoad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLoad.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnLoad.IdleBorderThickness = 1;
            this.btnLoad.IdleCornerRadius = 20;
            this.btnLoad.IdleFillColor = System.Drawing.Color.White;
            this.btnLoad.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnLoad.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnLoad.Location = new System.Drawing.Point(646, 549);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(5);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(125, 41);
            this.btnLoad.TabIndex = 1;
            this.btnLoad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // lblExportToCSV
            // 
            this.lblExportToCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblExportToCSV.AutoSize = true;
            this.lblExportToCSV.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExportToCSV.Location = new System.Drawing.Point(212, 558);
            this.lblExportToCSV.Name = "lblExportToCSV";
            this.lblExportToCSV.Size = new System.Drawing.Size(94, 20);
            this.lblExportToCSV.TabIndex = 2;
            this.lblExportToCSV.Text = "Export to &CSV";
            // 
            // dtp_From
            // 
            this.dtp_From.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.dtp_From.BorderRadius = 0;
            this.dtp_From.ForeColor = System.Drawing.Color.White;
            this.dtp_From.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_From.FormatCustom = null;
            this.dtp_From.Location = new System.Drawing.Point(38, 69);
            this.dtp_From.Name = "dtp_From";
            this.dtp_From.Size = new System.Drawing.Size(197, 34);
            this.dtp_From.TabIndex = 4;
            this.dtp_From.Value = new System.DateTime(2017, 12, 20, 0, 0, 0, 0);
            this.dtp_From.onValueChanged += new System.EventHandler(this.dtp_From_onValueChanged);
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(6, 77);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(33, 13);
            this.lblFrom.TabIndex = 5;
            this.lblFrom.Text = "From:";
            // 
            // grpFilters
            // 
            this.grpFilters.Controls.Add(this.lbl_cb_days);
            this.grpFilters.Controls.Add(this.cb_Days);
            this.grpFilters.Controls.Add(this.label1);
            this.grpFilters.Controls.Add(this.cmb_PayCode);
            this.grpFilters.Controls.Add(this.lblCostCenter);
            this.grpFilters.Controls.Add(this.cmb_Department);
            this.grpFilters.Controls.Add(this.dtp_To);
            this.grpFilters.Controls.Add(this.btnFilters);
            this.grpFilters.Controls.Add(this.lblTo);
            this.grpFilters.Controls.Add(this.dtp_From);
            this.grpFilters.Controls.Add(this.lblFrom);
            this.grpFilters.Location = new System.Drawing.Point(12, 2);
            this.grpFilters.Name = "grpFilters";
            this.grpFilters.Size = new System.Drawing.Size(759, 121);
            this.grpFilters.TabIndex = 6;
            this.grpFilters.TabStop = false;
            this.grpFilters.Text = "Filters";
            // 
            // lbl_cb_days
            // 
            this.lbl_cb_days.AutoSize = true;
            this.lbl_cb_days.Location = new System.Drawing.Point(60, 36);
            this.lbl_cb_days.Name = "lbl_cb_days";
            this.lbl_cb_days.Size = new System.Drawing.Size(90, 13);
            this.lbl_cb_days.TabIndex = 20;
            this.lbl_cb_days.Text = "Enable Calendars";
            // 
            // cb_Days
            // 
            this.cb_Days.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.cb_Days.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.cb_Days.Checked = true;
            this.cb_Days.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.cb_Days.ForeColor = System.Drawing.Color.White;
            this.cb_Days.Location = new System.Drawing.Point(38, 32);
            this.cb_Days.Name = "cb_Days";
            this.cb_Days.Size = new System.Drawing.Size(20, 20);
            this.cb_Days.TabIndex = 19;
            this.cb_Days.OnChange += new System.EventHandler(this.cb_Days_OnChange);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(510, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Pay Code:";
            // 
            // cmb_PayCode
            // 
            this.cmb_PayCode.DropDownWidth = 118;
            this.cmb_PayCode.FormattingEnabled = true;
            this.cmb_PayCode.Location = new System.Drawing.Point(568, 82);
            this.cmb_PayCode.Name = "cmb_PayCode";
            this.cmb_PayCode.Size = new System.Drawing.Size(112, 21);
            this.cmb_PayCode.TabIndex = 17;
            this.cmb_PayCode.SelectedIndexChanged += new System.EventHandler(this.cmb_PayCode_SelectedIndexChanged);
            // 
            // lblCostCenter
            // 
            this.lblCostCenter.AutoSize = true;
            this.lblCostCenter.Location = new System.Drawing.Point(501, 42);
            this.lblCostCenter.Name = "lblCostCenter";
            this.lblCostCenter.Size = new System.Drawing.Size(65, 13);
            this.lblCostCenter.TabIndex = 16;
            this.lblCostCenter.Text = "Department:";
            // 
            // cmb_Department
            // 
            this.cmb_Department.FormattingEnabled = true;
            this.cmb_Department.Location = new System.Drawing.Point(568, 39);
            this.cmb_Department.Name = "cmb_Department";
            this.cmb_Department.Size = new System.Drawing.Size(112, 21);
            this.cmb_Department.TabIndex = 15;
            this.cmb_Department.SelectedIndexChanged += new System.EventHandler(this.cmb_Department_SelectedIndexChanged);
            // 
            // dtp_To
            // 
            this.dtp_To.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.dtp_To.BorderRadius = 0;
            this.dtp_To.ForeColor = System.Drawing.Color.White;
            this.dtp_To.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_To.FormatCustom = null;
            this.dtp_To.Location = new System.Drawing.Point(270, 69);
            this.dtp_To.Name = "dtp_To";
            this.dtp_To.Size = new System.Drawing.Size(197, 34);
            this.dtp_To.TabIndex = 9;
            this.dtp_To.Value = new System.DateTime(2017, 12, 6, 0, 0, 0, 0);
            this.dtp_To.onValueChanged += new System.EventHandler(this.dtp_To_onValueChanged);
            // 
            // btnFilters
            // 
            this.btnFilters.BackColor = System.Drawing.Color.White;
            this.btnFilters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnFilters.Image = ((System.Drawing.Image)(resources.GetObject("btnFilters.Image")));
            this.btnFilters.ImageActive = null;
            this.btnFilters.Location = new System.Drawing.Point(703, 50);
            this.btnFilters.Margin = new System.Windows.Forms.Padding(0);
            this.btnFilters.Name = "btnFilters";
            this.btnFilters.Padding = new System.Windows.Forms.Padding(3);
            this.btnFilters.Size = new System.Drawing.Size(40, 40);
            this.btnFilters.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btnFilters.TabIndex = 8;
            this.btnFilters.TabStop = false;
            this.btnFilters.Zoom = 10;
            this.btnFilters.Click += new System.EventHandler(this.btnFilters_Click);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(247, 77);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(23, 13);
            this.lblTo.TabIndex = 6;
            this.lblTo.Text = "To:";
            // 
            // btnExportToCSV
            // 
            this.btnExportToCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportToCSV.BackColor = System.Drawing.Color.SeaGreen;
            this.btnExportToCSV.Image = ((System.Drawing.Image)(resources.GetObject("btnExportToCSV.Image")));
            this.btnExportToCSV.ImageActive = null;
            this.btnExportToCSV.Location = new System.Drawing.Point(169, 547);
            this.btnExportToCSV.Name = "btnExportToCSV";
            this.btnExportToCSV.Size = new System.Drawing.Size(42, 41);
            this.btnExportToCSV.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExportToCSV.TabIndex = 7;
            this.btnExportToCSV.TabStop = false;
            this.btnExportToCSV.Zoom = 10;
            this.btnExportToCSV.Click += new System.EventHandler(this.btnExportToCSV_Click);
            // 
            // btnExportToPDF
            // 
            this.btnExportToPDF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportToPDF.BackColor = System.Drawing.Color.Red;
            this.btnExportToPDF.Image = ((System.Drawing.Image)(resources.GetObject("btnExportToPDF.Image")));
            this.btnExportToPDF.ImageActive = null;
            this.btnExportToPDF.Location = new System.Drawing.Point(329, 547);
            this.btnExportToPDF.Name = "btnExportToPDF";
            this.btnExportToPDF.Size = new System.Drawing.Size(42, 41);
            this.btnExportToPDF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExportToPDF.TabIndex = 8;
            this.btnExportToPDF.TabStop = false;
            this.btnExportToPDF.Zoom = 10;
            this.btnExportToPDF.Click += new System.EventHandler(this.btnExportToPDF_Click);
            // 
            // lblExportToPDF
            // 
            this.lblExportToPDF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblExportToPDF.AutoSize = true;
            this.lblExportToPDF.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExportToPDF.Location = new System.Drawing.Point(373, 559);
            this.lblExportToPDF.Name = "lblExportToPDF";
            this.lblExportToPDF.Size = new System.Drawing.Size(93, 20);
            this.lblExportToPDF.TabIndex = 9;
            this.lblExportToPDF.Text = "Export to &PDF";
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportToExcel.BackColor = System.Drawing.Color.SeaGreen;
            this.btnExportToExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportToExcel.Image")));
            this.btnExportToExcel.ImageActive = null;
            this.btnExportToExcel.Location = new System.Drawing.Point(12, 547);
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.Size = new System.Drawing.Size(42, 41);
            this.btnExportToExcel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExportToExcel.TabIndex = 11;
            this.btnExportToExcel.TabStop = false;
            this.btnExportToExcel.Zoom = 10;
            this.btnExportToExcel.Click += new System.EventHandler(this.btnExportToExcel_Click);
            // 
            // lblExportToExcel
            // 
            this.lblExportToExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblExportToExcel.AutoSize = true;
            this.lblExportToExcel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExportToExcel.Location = new System.Drawing.Point(55, 558);
            this.lblExportToExcel.Name = "lblExportToExcel";
            this.lblExportToExcel.Size = new System.Drawing.Size(101, 20);
            this.lblExportToExcel.TabIndex = 10;
            this.lblExportToExcel.Text = "Export to &Excel";
            // 
            // frmMainReporter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(783, 603);
            this.Controls.Add(this.btnExportToExcel);
            this.Controls.Add(this.lblExportToExcel);
            this.Controls.Add(this.lblExportToPDF);
            this.Controls.Add(this.btnExportToPDF);
            this.Controls.Add(this.btnExportToCSV);
            this.Controls.Add(this.grpFilters);
            this.Controls.Add(this.lblExportToCSV);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.lst_Report);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMainReporter";
            this.Text = "Main Reporter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMainReporter_FormClosing);
            this.Load += new System.EventHandler(this.frmMainReporter_Load);
            this.Shown += new System.EventHandler(this.frmMainReporter_Shown);
            this.grpFilters.ResumeLayout(false);
            this.grpFilters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToCSV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToPDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToExcel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lst_Report;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLoad;
        private System.Windows.Forms.ColumnHeader field_one;
        private System.Windows.Forms.ColumnHeader field_two;
        private System.Windows.Forms.ColumnHeader field_three;
        private System.Windows.Forms.ColumnHeader account;
        private System.Windows.Forms.ColumnHeader area;
        private System.Windows.Forms.ColumnHeader department;
        private System.Windows.Forms.ColumnHeader labor_type;
        private System.Windows.Forms.ColumnHeader pay_code;
        private System.Windows.Forms.ColumnHeader money;
        private System.Windows.Forms.ColumnHeader hours;
        private System.Windows.Forms.ColumnHeader wages;
        private System.Windows.Forms.ColumnHeader days;
        private Bunifu.Framework.UI.BunifuCustomLabel lblExportToCSV;
        private Bunifu.Framework.UI.BunifuDatepicker dtp_From;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.GroupBox grpFilters;
        private System.Windows.Forms.Label lblTo;
        private Bunifu.Framework.UI.BunifuImageButton btnExportToCSV;
        private Bunifu.Framework.UI.BunifuImageButton btnExportToPDF;
        private Bunifu.Framework.UI.BunifuCustomLabel lblExportToPDF;
        private Bunifu.Framework.UI.BunifuImageButton btnExportToExcel;
        private Bunifu.Framework.UI.BunifuCustomLabel lblExportToExcel;
        private Bunifu.Framework.UI.BunifuImageButton btnFilters;
        private System.Windows.Forms.ColumnHeader date_begin;
        private System.Windows.Forms.ColumnHeader date_end;
        private Bunifu.Framework.UI.BunifuDatepicker dtp_To;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmb_PayCode;
        private System.Windows.Forms.Label lblCostCenter;
        private System.Windows.Forms.ComboBox cmb_Department;
        private Bunifu.Framework.UI.BunifuCheckbox cb_Days;
        private System.Windows.Forms.Label lbl_cb_days;
    }
}