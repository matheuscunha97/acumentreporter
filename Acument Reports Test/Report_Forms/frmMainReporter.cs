﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;
using Acument_Reports;

namespace Acument_Reports_Test
{
    public partial class frmMainReporter : Form
    {
        DBConnection dataBaseConn = new DBConnection();
        public frmMainReporter()
        {
            InitializeComponent();
        }

        private void frmMainReporter_Load(object sender, EventArgs e)
        {

        }

        private void frmMainReporter_Shown(object sender, EventArgs e)
        {
            this.CenterToScreen();

            using (NpgsqlConnection conn = dataBaseConn.Open()) 
            {
                try
                {
                    string query = "SELECT DISTINCT dept_rep FROM final_report ORDER BY dept_rep ASC";
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(query,conn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Department");
                    cmb_Department.DisplayMember = "dept_rep";
                    cmb_Department.ValueMember = "dept_rep";
                    cmb_Department.DataSource = ds.Tables["Department"];
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }//Fill Department combobox

            using (NpgsqlConnection conn = dataBaseConn.Open()) 
            {
                try
                {
                    string query = "SELECT DISTINCT payc_rep FROM final_report ORDER BY payc_rep ASC";
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(query, conn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Pay Code");
                    cmb_PayCode.DisplayMember = "payc_rep";
                    cmb_PayCode.ValueMember = "payc_rep";
                    cmb_PayCode.DataSource = ds.Tables["Pay Code"];
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }//Fill Pay Code combobox

            cb_Days.Checked = false;

            dtp_From.Value = DateTime.Now;
            dtp_From.Format = DateTimePickerFormat.Custom; 
            dtp_From.FormatCustom = " ";
            dtp_From.Enabled = false;

            dtp_To.Value = DateTime.Now;
            dtp_To.Format = DateTimePickerFormat.Custom;   
            dtp_To.FormatCustom = " ";
            dtp_To.Enabled = false;

            cmb_Department.SelectedIndex = -1; //Make the ComboBox show empty blank when get load. 
            cmb_PayCode.SelectedIndex = -1;    //

            lst_Report.Columns.RemoveAt(13); //Remove Datetime columns
            lst_Report.Columns.RemoveAt(12);
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            LstMainRepoData MainRepoData = new LstMainRepoData();
            MainRepoData.populateMainRepoLST(lst_Report);
        }//Fill the ListView with all records

        private void btnFilters_Click(object sender, EventArgs e)
        {
            MainReport_Filters filters = new MainReport_Filters();
            string dateFrom = dtp_From.Value.ToShortDateString();
            string dateTo = dtp_To.Value.ToShortDateString();
            string department = cmb_Department.Text;
            string pay_code = cmb_PayCode.Text;
            //MessageBox.Show(dateFrom, dateTo);

            try
            {
                //Check if there's at least 1 filter selected
                if (cb_Days.Checked == false && string.IsNullOrEmpty(department) && string.IsNullOrEmpty(pay_code))
                {
                    MessageBox.Show("Please select at least 1 filter.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                //Date
                else if (cb_Days.Checked == true && (string.IsNullOrEmpty(department) && string.IsNullOrEmpty(pay_code)))
                {
                    //MessageBox.Show("Entrou no if");
                    filters.date_Filter(dtp_From, dtp_To, lst_Report);
                }
                //Date + Department
                else if (cb_Days.Checked == true && (!string.IsNullOrEmpty(department)) && string.IsNullOrEmpty(pay_code))
                {
                    //MessageBox.Show("Entrou no 1 elseif");
                    filters.date_dept_Filter(dtp_From, dtp_To, department, lst_Report);
                }
                //Date + Pay Code
                else if (cb_Days.Checked == true && (!(string.IsNullOrEmpty(dateFrom) && string.IsNullOrEmpty(dateTo) && string.IsNullOrEmpty(pay_code))) && string.IsNullOrEmpty(department))
                {
                    //MessageBox.Show("Entrou no 2 elseif");
                    filters.date_payc_Filter(dtp_From, dtp_To, pay_code, lst_Report);
                }
                //Date + Department + Pay Code
                else if (cb_Days.Checked == true && !(string.IsNullOrEmpty(department) && string.IsNullOrEmpty(pay_code)))
                {
                    //MessageBox.Show("Entrou no 3 elseif");
                    filters.allFilters(dtp_From, dtp_To, department, pay_code, lst_Report);
                }
                //Department
                else if (cb_Days.Checked == false && !(string.IsNullOrEmpty(department)) && string.IsNullOrEmpty(pay_code))
                {
                    //MessageBox.Show("Entrou no 4 elseif");
                    filters.department_Filter(department, lst_Report);
                }
                //Department + Pay Code
                else if (cb_Days.Checked == false && !string.IsNullOrEmpty(department) && !string.IsNullOrEmpty(pay_code))
                {
                    //MessageBox.Show("Entrou no 5 elseif");
                    filters.department_payc_Filter(department, pay_code, lst_Report);
                }
                //Pay Code
                else if (cb_Days.Checked == false && string.IsNullOrEmpty(department) && !(string.IsNullOrEmpty(pay_code)))
                {
                    //MessageBox.Show("Entrou no 6 elseif");
                    filters.payc_Filter(pay_code, lst_Report);
                }
            }
            catch (Exception ex)
            {
                //Message if there's an error
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }//Filters for ListView.

        private void btnExportToCSV_Click(object sender, EventArgs e)
        {
            ExportToCSV csv = new ExportToCSV();
            csv.Export(lst_Report);
        } //Export file to CSV button

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToEXCEL excel = new ExportToEXCEL();
            excel.Export(lst_Report);
        } //Export file to Excel button

        private void btnExportToPDF_Click(object sender, EventArgs e)
        {
            ExportToPDF pdf = new ExportToPDF();
            pdf.Export(lst_Report);
        } //Export file to PDF button

        private void frmMainReporter_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void dtp_From_onValueChanged(object sender, EventArgs e)
        {
            dtp_From.Format = DateTimePickerFormat.Short;
        }

        private void dtp_To_onValueChanged(object sender, EventArgs e)
        {
            dtp_To.Format = DateTimePickerFormat.Short;
        }

        private void cmb_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cmb_PayCode_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cb_Days_OnChange(object sender, EventArgs e)
        {
            if(cb_Days.Checked == false)
            {
                dtp_From.Format = DateTimePickerFormat.Custom;
                dtp_From.FormatCustom = " ";
                dtp_To.Format = DateTimePickerFormat.Custom;
                dtp_To.FormatCustom = " ";
                dtp_From.Enabled = false;
                dtp_To.Enabled = false;
                lbl_cb_days.Text = "Enable Calendars";
            }
            else
            {
                dtp_From.Format = DateTimePickerFormat.Custom;
                dtp_From.FormatCustom = " ";
                dtp_To.Format = DateTimePickerFormat.Custom;
                dtp_To.FormatCustom = " ";
                dtp_From.Enabled = true;
                dtp_To.Enabled = true;
                lbl_cb_days.Text = "Disable Calendars";
            }
        }
    }
}
