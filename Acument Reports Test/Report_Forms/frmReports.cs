﻿using Acument_Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acument_Reports_Test
{
    public partial class frmReports : Form
    {
        public frmReports()
        {
            InitializeComponent();
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void frmReports_Shown(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }

        private void btnTimer_MouseEnter(object sender, EventArgs e)
        {
            btnTimer.Font = new Font(label1.Font.Name, label1.Font.SizeInPoints, FontStyle.Underline);
        }

        private void btnTimer_MouseLeave(object sender, EventArgs e)
        {

            btnTimer.Font = new Font(label1.Font.Name, label1.Font.SizeInPoints, FontStyle.Regular);
        }

        private void btnManager_MouseEnter(object sender, EventArgs e)
        {
            btnManager.Font = new Font(label1.Font.Name, label1.Font.SizeInPoints, FontStyle.Underline);
        }

        private void btnManager_MouseLeave(object sender, EventArgs e)
        {
            btnManager.Font = new Font(label1.Font.Name, label1.Font.SizeInPoints, FontStyle.Regular);
        }

        private void btnFinancials_MouseEnter(object sender, EventArgs e)
        {
            btnFinancials.Font = new Font(label1.Font.Name, label1.Font.SizeInPoints, FontStyle.Underline);
        }

        private void btnFinancials_MouseLeave(object sender, EventArgs e)
        {
            btnFinancials.Font = new Font(label1.Font.Name, label1.Font.SizeInPoints, FontStyle.Regular);
        }

        private void btnTimer_Click(object sender, EventArgs e)
        {
            frmMainReporter reporter = new frmMainReporter();
            reporter.Show();
        }

        private void btnManager_Click(object sender, EventArgs e)
        {
            frmManagerReport managerRepo = new frmManagerReport();
            managerRepo.Show();
        }
    }
}
