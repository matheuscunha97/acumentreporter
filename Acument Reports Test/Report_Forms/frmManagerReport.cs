﻿using Acument_Reports.System_Variables;
using Acument_Reports_Test;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;

namespace Acument_Reports
{
    public partial class frmManagerReport : Form
    {
        DBConnection dataBaseConn = new DBConnection();
        frmFilters formFilters = new frmFilters();
        ManagerReport_Data managerData = new ManagerReport_Data();
        Bunifu.Framework.UI.BunifuDatepicker dtpJoker = new Bunifu.Framework.UI.BunifuDatepicker(); //Created only to iniciate one instance for the Global Datepicker (used on exportations)
        Bunifu.Framework.UI.BunifuCheckbox bunifu_cb_joker = new Bunifu.Framework.UI.BunifuCheckbox(); //Created only to iniciate one instance for the Global Datepicker (used on exportations)
        CheckBox cb_Joker = new CheckBox(); //Created only to iniciate one instance for the Global Checkbox (used on exportations)
        string string_joker = ""; //Created only to iniciate one instance for the Global string (used on exportations)

        public frmManagerReport()
        {
            InitializeComponent();
        }

        private void frmManagerReport_Shown(object sender, EventArgs e)
        {
            this.CenterToScreen();

            //Dtp joker
            dtpJoker.Value = DateTime.Today;
            Filters_Parameters.date_From = dtpJoker;
            Filters_Parameters.date_To = dtpJoker;

            //Checkbox Joker
            cb_Joker.Checked = false;
            bunifu_cb_joker.Checked = false;
            Filters_Parameters.cb_allDepartments = cb_Joker;
            Filters_Parameters.cb_allConcepts = cb_Joker;
            Filters_Parameters.cb_allLabors = cb_Joker;
            Filters_Parameters.checkBox_Days = bunifu_cb_joker;

            //String joker
            Filters_Parameters.selected_DepartmentsFilters = string_joker;
            Filters_Parameters.selected_ConceptsFilters = string_joker;
            Filters_Parameters.selected_LaborsFilters = string_joker;
            //Filters_Parameters.summary_query = string_joker;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Filters_Parameters.filter_query))
            {
                //true - Fill ListView with filtered data
                managerData.execute_with_filter(Filters_Parameters.filter_query, lst_ManagerReport);
            }
            else
            {
                //false - Fill Listview with all data from DB
                Filters_Parameters.summary_query = formFilters.save_summary_query_without_filter();
                managerData.execute_without_filter(lst_ManagerReport);
            }
        }

        private void btn_OpenFiltersForm_Click(object sender, EventArgs e)
        {
            frmFilters filters = new frmFilters();
            filters.ShowDialog();
        }

        private void btnExportToCSV_Click(object sender, EventArgs e)
        {
            ExportToCSV csv = new ExportToCSV();
            csv.Export(lst_ManagerReport);
        }

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToEXCEL excel = new ExportToEXCEL();
            excel.Export(lst_ManagerReport);
        }

        private void btnExportToPDF_Click(object sender, EventArgs e)
        {
            ExportToPDF pdf = new ExportToPDF();
            pdf.Export(lst_ManagerReport);
        }
    }
}
