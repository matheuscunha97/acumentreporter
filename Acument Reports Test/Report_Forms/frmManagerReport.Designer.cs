﻿namespace Acument_Reports
{
    partial class frmManagerReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManagerReport));
            this.btnExportToExcel = new Bunifu.Framework.UI.BunifuImageButton();
            this.grpFilters = new System.Windows.Forms.GroupBox();
            this.btn_OpenFiltersForm = new Bunifu.Framework.UI.BunifuThinButton2();
            this.lblExportToPDF = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnExportToPDF = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnExportToCSV = new Bunifu.Framework.UI.BunifuImageButton();
            this.lblExportToExcel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lblExportToCSV = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.dias = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.wages = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.horas = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.importe = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.concepto = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.num_conc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.manoobra = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.centro = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.account = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.field_three = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.field_two = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.field_one = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnLoad = new Bunifu.Framework.UI.BunifuThinButton2();
            this.lst_ManagerReport = new System.Windows.Forms.ListView();
            this.date = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToExcel)).BeginInit();
            this.grpFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToPDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToCSV)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportToExcel.BackColor = System.Drawing.Color.SeaGreen;
            this.btnExportToExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportToExcel.Image")));
            this.btnExportToExcel.ImageActive = null;
            this.btnExportToExcel.Location = new System.Drawing.Point(12, 552);
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.Size = new System.Drawing.Size(42, 41);
            this.btnExportToExcel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExportToExcel.TabIndex = 20;
            this.btnExportToExcel.TabStop = false;
            this.btnExportToExcel.Zoom = 10;
            this.btnExportToExcel.Click += new System.EventHandler(this.btnExportToExcel_Click);
            // 
            // grpFilters
            // 
            this.grpFilters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpFilters.Controls.Add(this.btn_OpenFiltersForm);
            this.grpFilters.Location = new System.Drawing.Point(12, 7);
            this.grpFilters.Name = "grpFilters";
            this.grpFilters.Size = new System.Drawing.Size(866, 73);
            this.grpFilters.TabIndex = 15;
            this.grpFilters.TabStop = false;
            this.grpFilters.Text = "Filters";
            // 
            // btn_OpenFiltersForm
            // 
            this.btn_OpenFiltersForm.ActiveBorderThickness = 1;
            this.btn_OpenFiltersForm.ActiveCornerRadius = 20;
            this.btn_OpenFiltersForm.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btn_OpenFiltersForm.ActiveForecolor = System.Drawing.Color.White;
            this.btn_OpenFiltersForm.ActiveLineColor = System.Drawing.Color.White;
            this.btn_OpenFiltersForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_OpenFiltersForm.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_OpenFiltersForm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OpenFiltersForm.BackgroundImage")));
            this.btn_OpenFiltersForm.ButtonText = "Select Filters";
            this.btn_OpenFiltersForm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_OpenFiltersForm.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_OpenFiltersForm.ForeColor = System.Drawing.Color.White;
            this.btn_OpenFiltersForm.IdleBorderThickness = 1;
            this.btn_OpenFiltersForm.IdleCornerRadius = 20;
            this.btn_OpenFiltersForm.IdleFillColor = System.Drawing.Color.White;
            this.btn_OpenFiltersForm.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btn_OpenFiltersForm.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btn_OpenFiltersForm.Location = new System.Drawing.Point(290, 15);
            this.btn_OpenFiltersForm.Margin = new System.Windows.Forms.Padding(5);
            this.btn_OpenFiltersForm.Name = "btn_OpenFiltersForm";
            this.btn_OpenFiltersForm.Size = new System.Drawing.Size(293, 50);
            this.btn_OpenFiltersForm.TabIndex = 21;
            this.btn_OpenFiltersForm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_OpenFiltersForm.Click += new System.EventHandler(this.btn_OpenFiltersForm_Click);
            // 
            // lblExportToPDF
            // 
            this.lblExportToPDF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblExportToPDF.AutoSize = true;
            this.lblExportToPDF.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExportToPDF.Location = new System.Drawing.Point(373, 564);
            this.lblExportToPDF.Name = "lblExportToPDF";
            this.lblExportToPDF.Size = new System.Drawing.Size(93, 20);
            this.lblExportToPDF.TabIndex = 18;
            this.lblExportToPDF.Text = "Export to &PDF";
            // 
            // btnExportToPDF
            // 
            this.btnExportToPDF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportToPDF.BackColor = System.Drawing.Color.Red;
            this.btnExportToPDF.Image = ((System.Drawing.Image)(resources.GetObject("btnExportToPDF.Image")));
            this.btnExportToPDF.ImageActive = null;
            this.btnExportToPDF.Location = new System.Drawing.Point(329, 552);
            this.btnExportToPDF.Name = "btnExportToPDF";
            this.btnExportToPDF.Size = new System.Drawing.Size(42, 41);
            this.btnExportToPDF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExportToPDF.TabIndex = 17;
            this.btnExportToPDF.TabStop = false;
            this.btnExportToPDF.Zoom = 10;
            this.btnExportToPDF.Click += new System.EventHandler(this.btnExportToPDF_Click);
            // 
            // btnExportToCSV
            // 
            this.btnExportToCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportToCSV.BackColor = System.Drawing.Color.SeaGreen;
            this.btnExportToCSV.Image = ((System.Drawing.Image)(resources.GetObject("btnExportToCSV.Image")));
            this.btnExportToCSV.ImageActive = null;
            this.btnExportToCSV.Location = new System.Drawing.Point(169, 552);
            this.btnExportToCSV.Name = "btnExportToCSV";
            this.btnExportToCSV.Size = new System.Drawing.Size(42, 41);
            this.btnExportToCSV.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExportToCSV.TabIndex = 16;
            this.btnExportToCSV.TabStop = false;
            this.btnExportToCSV.Zoom = 10;
            this.btnExportToCSV.Click += new System.EventHandler(this.btnExportToCSV_Click);
            // 
            // lblExportToExcel
            // 
            this.lblExportToExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblExportToExcel.AutoSize = true;
            this.lblExportToExcel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExportToExcel.Location = new System.Drawing.Point(55, 563);
            this.lblExportToExcel.Name = "lblExportToExcel";
            this.lblExportToExcel.Size = new System.Drawing.Size(101, 20);
            this.lblExportToExcel.TabIndex = 19;
            this.lblExportToExcel.Text = "Export to &Excel";
            // 
            // lblExportToCSV
            // 
            this.lblExportToCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblExportToCSV.AutoSize = true;
            this.lblExportToCSV.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExportToCSV.Location = new System.Drawing.Point(212, 563);
            this.lblExportToCSV.Name = "lblExportToCSV";
            this.lblExportToCSV.Size = new System.Drawing.Size(94, 20);
            this.lblExportToCSV.TabIndex = 14;
            this.lblExportToCSV.Text = "Export to &CSV";
            // 
            // dias
            // 
            this.dias.Text = "Days";
            this.dias.Width = 41;
            // 
            // wages
            // 
            this.wages.Text = "Wages";
            // 
            // horas
            // 
            this.horas.Text = "Hours";
            // 
            // importe
            // 
            this.importe.Text = "Money";
            // 
            // concepto
            // 
            this.concepto.Text = "Concept";
            this.concepto.Width = 174;
            // 
            // num_conc
            // 
            this.num_conc.Text = "Nº Concept";
            this.num_conc.Width = 68;
            // 
            // manoobra
            // 
            this.manoobra.Text = "Labor";
            this.manoobra.Width = 42;
            // 
            // centro
            // 
            this.centro.Text = "Department";
            this.centro.Width = 75;
            // 
            // account
            // 
            this.account.Text = "Account";
            this.account.Width = 67;
            // 
            // field_three
            // 
            this.field_three.Text = "Field3";
            this.field_three.Width = 46;
            // 
            // field_two
            // 
            this.field_two.Text = "Field2";
            this.field_two.Width = 42;
            // 
            // field_one
            // 
            this.field_one.Text = "Field1";
            this.field_one.Width = 43;
            // 
            // btnLoad
            // 
            this.btnLoad.ActiveBorderThickness = 1;
            this.btnLoad.ActiveCornerRadius = 20;
            this.btnLoad.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnLoad.ActiveForecolor = System.Drawing.Color.White;
            this.btnLoad.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoad.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLoad.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLoad.BackgroundImage")));
            this.btnLoad.ButtonText = "Load";
            this.btnLoad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLoad.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnLoad.IdleBorderThickness = 1;
            this.btnLoad.IdleCornerRadius = 20;
            this.btnLoad.IdleFillColor = System.Drawing.Color.White;
            this.btnLoad.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnLoad.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnLoad.Location = new System.Drawing.Point(753, 554);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(5);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(125, 41);
            this.btnLoad.TabIndex = 12;
            this.btnLoad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // lst_ManagerReport
            // 
            this.lst_ManagerReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lst_ManagerReport.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.field_one,
            this.field_two,
            this.field_three,
            this.account,
            this.centro,
            this.manoobra,
            this.num_conc,
            this.concepto,
            this.importe,
            this.horas,
            this.wages,
            this.dias,
            this.date});
            this.lst_ManagerReport.GridLines = true;
            this.lst_ManagerReport.Location = new System.Drawing.Point(12, 86);
            this.lst_ManagerReport.MultiSelect = false;
            this.lst_ManagerReport.Name = "lst_ManagerReport";
            this.lst_ManagerReport.Size = new System.Drawing.Size(866, 460);
            this.lst_ManagerReport.TabIndex = 13;
            this.lst_ManagerReport.UseCompatibleStateImageBehavior = false;
            this.lst_ManagerReport.View = System.Windows.Forms.View.Details;
            // 
            // date
            // 
            this.date.Text = "Date";
            this.date.Width = 86;
            // 
            // frmManagerReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(890, 603);
            this.Controls.Add(this.btnExportToExcel);
            this.Controls.Add(this.grpFilters);
            this.Controls.Add(this.lblExportToPDF);
            this.Controls.Add(this.btnExportToPDF);
            this.Controls.Add(this.btnExportToCSV);
            this.Controls.Add(this.lblExportToExcel);
            this.Controls.Add(this.lblExportToCSV);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.lst_ManagerReport);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmManagerReport";
            this.Text = "Manager Report";
            this.Shown += new System.EventHandler(this.frmManagerReport_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToExcel)).EndInit();
            this.grpFilters.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToPDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExportToCSV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuImageButton btnExportToExcel;
        private System.Windows.Forms.GroupBox grpFilters;
        private Bunifu.Framework.UI.BunifuCustomLabel lblExportToPDF;
        private Bunifu.Framework.UI.BunifuImageButton btnExportToPDF;
        private Bunifu.Framework.UI.BunifuImageButton btnExportToCSV;
        private Bunifu.Framework.UI.BunifuCustomLabel lblExportToExcel;
        private Bunifu.Framework.UI.BunifuCustomLabel lblExportToCSV;
        private System.Windows.Forms.ColumnHeader dias;
        private System.Windows.Forms.ColumnHeader wages;
        private System.Windows.Forms.ColumnHeader horas;
        private System.Windows.Forms.ColumnHeader importe;
        private System.Windows.Forms.ColumnHeader concepto;
        private System.Windows.Forms.ColumnHeader num_conc;
        private System.Windows.Forms.ColumnHeader manoobra;
        private System.Windows.Forms.ColumnHeader centro;
        private System.Windows.Forms.ColumnHeader account;
        private System.Windows.Forms.ColumnHeader field_three;
        private System.Windows.Forms.ColumnHeader field_two;
        private System.Windows.Forms.ColumnHeader field_one;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLoad;
        private System.Windows.Forms.ListView lst_ManagerReport;
        private Bunifu.Framework.UI.BunifuThinButton2 btn_OpenFiltersForm;
        private System.Windows.Forms.ColumnHeader date;
    }
}