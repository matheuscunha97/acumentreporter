﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Windows.Forms;
using Bunifu.Framework.UI;
using Acument_Reports;

namespace Acument_Reports_Test
{
    class MainReport_Filters
    {
        DBConnection dataBaseConn = new DBConnection(); //Db Class Instance
        LstMainRepoData data = new LstMainRepoData();

        //Date
        public void date_Filter(BunifuDatepicker dtpFrom, BunifuDatepicker dtpTo,ListView listView)
        {
            if (dtpFrom.Value > dtpTo.Value)
            {
                MessageBox.Show("The final date should be greater then the initial.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                listView.Items.Clear();

                try
                {
                    using (NpgsqlConnection conn = dataBaseConn.Open())//Open connection
                    {
                        NpgsqlCommand Query = new NpgsqlCommand(string.Format("SELECT * FROM final_report WHERE dat_beg_rep BETWEEN '{0}' AND '{1}' ORDER BY dat_beg_rep ASC", dtpFrom.Value, dtpTo.Value), conn);
                        
                        //Load the data from the database on the ListView
                        NpgsqlDataReader dataReader = Query.ExecuteReader();
                        data.Fill(dataReader, listView);
                    }
                }
                catch (Exception ex)
                {
                    //Message if there's an error
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //If there's no records between the dates, display a message.
                if (listView.Items.Count == 0)
                {
                    MessageBox.Show("There's no records between this intervals.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        //Date + Department
        public void date_dept_Filter(BunifuDatepicker dtpFrom, BunifuDatepicker dtpTo, string department, ListView listView)
        {
            if (dtpFrom.Value > dtpTo.Value)
            {
                MessageBox.Show("The final date should be greater then the initial.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                listView.Items.Clear();

                try
                {
                    using (NpgsqlConnection conn = dataBaseConn.Open())//Open connection
                    {
                        NpgsqlCommand Query = new NpgsqlCommand(string.Format("SELECT * FROM final_report WHERE dat_beg_rep BETWEEN '{0}' AND '{1}' AND dept_rep = '{2}' ORDER BY dept_rep ASC", dtpFrom.Value, dtpTo.Value, department), conn);

                        //Load the data from the database on the ListView
                        NpgsqlDataReader dataReader = Query.ExecuteReader();
                        data.Fill(dataReader, listView);
                    }
                }
                catch (Exception ex)
                {
                    //Message if there's an error
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //If there's no records between the dates, display a message.
                if (listView.Items.Count == 0)
                {
                    MessageBox.Show("There's no records between this intervals.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        //Date + Pay Code
        public void date_payc_Filter(BunifuDatepicker dtpFrom, BunifuDatepicker dtpTo, string pay_code, ListView listView)
        {
            if (dtpFrom.Value > dtpTo.Value)
            {
                MessageBox.Show("The final date should be greater then the initial.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                listView.Items.Clear();

                try
                {
                    using (NpgsqlConnection conn = dataBaseConn.Open())//Open connection
                    {
                        NpgsqlCommand Query = new NpgsqlCommand(string.Format("SELECT * FROM final_report WHERE dat_beg_rep BETWEEN '{0}' AND '{1}' AND payc_rep = '{2}' ORDER BY dept_rep ASC", dtpFrom.Value, dtpTo.Value, pay_code), conn);

                        //Load the data from the database on the ListView
                        NpgsqlDataReader dataReader = Query.ExecuteReader();
                        data.Fill(dataReader, listView);
                    }
                }
                catch (Exception ex)
                {
                    //Message if there's an error
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //If there's no records between the dates, display a message.
                if (listView.Items.Count == 0)
                {
                    MessageBox.Show("There's no records between this intervals.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        //Date + Department + Pay Code
        public void allFilters(BunifuDatepicker dtpFrom, BunifuDatepicker dtpTo, string department, string pay_code, ListView listView)
        {
            if (dtpFrom.Value > dtpTo.Value)
            {
                MessageBox.Show("The final date should be greater then the initial.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                listView.Items.Clear();

                try
                {
                    using (NpgsqlConnection conn = dataBaseConn.Open())//Open connection
                    {
                        NpgsqlCommand Query = new NpgsqlCommand(string.Format("SELECT * FROM final_report WHERE dat_beg_rep BETWEEN '{0}' AND '{1}' AND dept_rep = '{2}' AND payc_rep = '{3}' ORDER BY dept_rep ASC", dtpFrom.Value, dtpTo.Value, department, pay_code), conn);

                        //Load the data from the database on the ListView
                        NpgsqlDataReader dataReader = Query.ExecuteReader();
                        data.Fill(dataReader, listView);
                    }
                }
                catch (Exception ex)
                {
                    //Message if there's an error
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //If there's no records between the dates, display a message.
                if (listView.Items.Count == 0)
                {
                    MessageBox.Show("There's no records between this intervals.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        //Department
        public void department_Filter(string department, ListView listView)
        {
            listView.Items.Clear();

            try
            {
                using (NpgsqlConnection conn = dataBaseConn.Open())//Open connection
                {
                    NpgsqlCommand Query = new NpgsqlCommand(string.Format("SELECT * FROM final_report WHERE dept_rep = '{0}' ORDER BY dept_rep ASC", department), conn);

                    //Load the data from the database on the ListView
                    NpgsqlDataReader dataReader = Query.ExecuteReader();
                    data.Fill(dataReader, listView);
                }
            }
            catch (Exception ex)
            {
                //Message if there's an error
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //If there's no records between the dates, display a message.
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("There's no records between this intervals.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //Department + Pay Code
        public void department_payc_Filter(string department,string pay_code, ListView listView)
        {
            listView.Items.Clear();

            try
            {
                using (NpgsqlConnection conn = dataBaseConn.Open())//Open connection
                {
                    NpgsqlCommand Query = new NpgsqlCommand(string.Format("SELECT * FROM final_report WHERE dept_rep = '{0}' AND payc_rep = '{1}' ORDER BY dept_rep ASC", department, pay_code), conn);

                    //Load the data from the database on the ListView
                    NpgsqlDataReader dataReader = Query.ExecuteReader();
                    data.Fill(dataReader, listView);
                }
            }
            catch (Exception ex)
            {
                //Message if there's an error
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //If there's no records between the dates, display a message.
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("There's no records between this intervals.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //Pay Code
        public void payc_Filter(string pay_code, ListView listView)
        {
            listView.Items.Clear();

            try
            {
                using (NpgsqlConnection conn = dataBaseConn.Open())//Open connection
                {
                    NpgsqlCommand Query = new NpgsqlCommand(string.Format("SELECT * FROM final_report WHERE payc_rep = '{0}' ORDER BY dept_rep ASC", pay_code), conn);

                    //Load the data from the database on the ListView
                    NpgsqlDataReader dataReader = Query.ExecuteReader();
                    data.Fill(dataReader, listView);
                }
            }
            catch (Exception ex)
            {
                //Message if there's an error
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //If there's no records between the dates, display a message.
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("There's no records between this intervals.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
