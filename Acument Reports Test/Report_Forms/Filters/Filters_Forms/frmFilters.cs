﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Acument_Reports_Test;
using Acument_Reports.System_Variables;
using System.Data.SqlClient;

namespace Acument_Reports
{
    public partial class frmFilters : Form
    {
        ManagerReport_Data manager_filters = new ManagerReport_Data();
        int cont_Departments = 8;
        int cont_Concepts = 8;
        DBConnection db = new DBConnection();
        string allConcepts;
        string allDepartments;

        public frmFilters()
        {
            InitializeComponent();
        }

        public bool AnyFilterSelected() //Function to check if some filter were selected
        {
            if (!string.IsNullOrEmpty(constructQuery()))
            {
                return true;
            }
            else //else return false
            {
                return false;
            }
        }

        public void SetGlobalComponents()
        {
            ////Date
            //Filters_Parameters.date_From = dtp_From;
            //Filters_Parameters.date_To = dtp_To;
            //Filter
            
        } //Function to set the local components to the global component

        public List<ComboBox> lst_cmbUsed() 
        {
            List<ComboBox> useds = new List<ComboBox>();
            List<ComboBox> all = new List<ComboBox>();

            all.Add(cmb_Concept1);
            all.Add(cmb_Concept2);
            all.Add(cmb_Concept3);
            all.Add(cmb_Concept4);
            all.Add(cmb_Concept5);
            all.Add(cmb_Concept6);
            all.Add(cmb_Concept7);
            all.Add(cmb_Concept8);
            all.Add(cmb_Department1);
            all.Add(cmb_Department2);
            all.Add(cmb_Department3);
            all.Add(cmb_Department4);
            all.Add(cmb_Department5);
            all.Add(cmb_Department6);
            all.Add(cmb_Department7);
            all.Add(cmb_Department8);

            foreach(ComboBox combobox in all)
            {
                if(combobox.Visible == true)
                {
                    useds.Add(combobox);
                }
            }

            return useds;
        } //Grab all ComboBoxes and create a new List only with the ComboBoxes added by the user

        public List<CheckBox> lst_cbUseds() //Grab all Checkboxes and create a new List only with the CheckBoxes added by the user
        {
            List<CheckBox> all = new List<CheckBox>();
            List<CheckBox> useds = new List<CheckBox>();

            all.Add(cb_AllConcepts);
            all.Add(cb_AllDepartments);
            all.Add(cb_AllLabors);
            all.Add(cb_LaborDirecto);
            all.Add(cb_LaborIndirecto);
            all.Add(cb_LaborSalario);

            foreach(CheckBox checkbox in all)
            {
                if(checkbox.Checked == true)
                {
                    useds.Add(checkbox);
                }
            }

            
            return useds;
        }

        public string constructQuery() //method to construct the query with the filters
        {
            //Common variables
            char identifier;
            string cmbName;
            string cbName;
            string query = "";
            var inDepartments = "";
            var inConcepts = "";
            var inLabors = "";
            var dates = "";
            bool cb_daysState = cb_Days.Checked;

            //Load the methods with all 'TODOS' (Department, concept and labors)
            string allDepartments = selectAllDepartments();
            string allConcepts = selectAllConcepts();
            string allLabors = selectAllLabors();

            foreach (ComboBox combobox in lst_cmbUsed()) //identify if the combobox in the cmb_FilterUsed is Department or Concept
            {
                cmbName = combobox.Name.ToString();
                identifier = cmbName[4]; //Grab the character in the 4 position of the combobox name

                if (identifier.ToString() == "D") //Check if is department
                {
                    inDepartments = inDepartments + "'" + combobox.Text + "',"; //Create the SQL 'IN' clause for departments;
                    Filters_Parameters.selected_DepartmentsFilters = Filters_Parameters.selected_DepartmentsFilters + "" + combobox.Text + ","; //Add the text of the combobox to the global string. (used on exportations)
                }
                else if (identifier.ToString() == "C") //Check if is concept
                {
                    inConcepts = inConcepts + "'" + combobox.Text + "',"; //Create the SQL 'IN' clause for concepts;
                    Filters_Parameters.selected_ConceptsFilters = Filters_Parameters.selected_ConceptsFilters + "" + combobox.Text + ","; //Add the text of the combobox to the global list. (used on exportations)
                }
            }// 
                                                           // Check all Combobox and Checkboxes to find which ones were selected
            foreach (CheckBox checkbox in lst_cbUseds())   //
            {
                cbName = checkbox.Name.ToString();
                identifier = cbName[6]; //Grab the character in the 6 position of the combobox name (Only for TODOS)
                char identifierRestLabors = cbName[8]; //Grab the character in the 3 position to check if is Labor

                if (identifier.ToString() == "D") //All departments
                {
                    inDepartments = allDepartments;
                    Filters_Parameters.todos_Departments = selectAllDepartments_ForHeaderExport();
                }
                else if (identifier.ToString() == "C") //All concepts
                {
                    inConcepts = allConcepts;
                    Filters_Parameters.todos_Concepts = selectAllConcepts_ForHeaderExport();
                }

                else if (identifier.ToString() == "L") //All labors
                {
                    inLabors = allLabors;
                    Filters_Parameters.todos_Labors = selectAllLabors_ForHeaderExport();
                }
                else if(identifierRestLabors.ToString() == "D" || identifierRestLabors.ToString() == "I" || identifierRestLabors.ToString() == "S")
                {
                    inLabors = inLabors + "'" + checkbox.Text + "',";
                    Filters_Parameters.selected_LaborsFilters = Filters_Parameters.selected_LaborsFilters + "" + checkbox.Text + ",";
                }
            }//

            if (cb_daysState && dtp_From.Value != DateTime.Today && dtp_To.Value != DateTime.Today) //Check if date filter was used
            {
                if (dtp_From.Value > dtp_To.Value)
                {
                    MessageBox.Show("The final date should be greater then the initial.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    dates = dates + "fun.fch_pago BETWEEN convert(datetime, '" + dtp_From.Value.ToString("yyyy-MM-dd") + "') AND convert(datetime, '" + dtp_To.Value.ToString("yyyy-MM-dd") + "') ";
                }
            } //Check if Date filter was selected

            //Variables used to check if the Departments, Concepts or Labors was selected
            bool depState = string.IsNullOrEmpty(inDepartments);
            bool conState = string.IsNullOrEmpty(inConcepts);
            bool labState = string.IsNullOrEmpty(inLabors);
            bool datesState = string.IsNullOrEmpty(dates);

            if (!depState)
            {
                inDepartments = inDepartments.Remove(inDepartments.Length - 1); //Remove the last comma.
            }// If is not empty, remove the remanescent comma.
            if (!conState)
            {
                inConcepts = inConcepts.Remove(inConcepts.Length - 1); //Remove the last comma.
            }// If is not empty, remove the remanescent comma.
            if (!labState)
            {
                inLabors = inLabors.Remove(inLabors.Length - 1); //Remove the last comma.
                
            }// If is not empty, remove the remanescent comma.

            //Date
            if (!datesState && depState && conState && labState)
            {
                //where = where + dates;

                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND " + dates + " " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Date + Department
            else if (!datesState && !depState && conState && labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND " + dates + " " +
                        "AND fun.centro IN (" + inDepartments + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Date + Concepts
            else if (!datesState && depState && !conState && labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND " + dates + " " +
                        "AND fun.concepto IN (" + inConcepts + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Date + Labor Type
            else if (!datesState && depState && conState && !labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND " + dates + " " +
                        "AND e.manoobra IN (" + inLabors + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Date + Department + Concept
            else if (!datesState && !depState && !conState && labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND " + dates + " " +
                        "AND fun.centro IN (" + inDepartments + ") " +
                        "AND fun.concepto IN (" + inConcepts + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Date + Department + Labor Type
            else if (!datesState && !depState && conState && !labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND " + dates + " " +
                        "AND fun.centro IN (" + inDepartments + ") " +
                        "AND e.manoobra IN (" + inLabors + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Date + Department + Concept + Labor Type
            else if (!datesState && !depState && !conState && !labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND " + dates + " " +
                        "AND fun.concepto IN (" + inConcepts + ") " +
                        "AND fun.centro IN (" + inDepartments + ") " +
                        "AND e.manoobra IN (" + inLabors + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Department
            else if (datesState && !depState && conState && labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND fun.centro IN (" + inDepartments + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Department + Concept
            else if (datesState && !depState && !conState && labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND fun.centro IN (" + inDepartments + ") " +
                        "AND fun.concepto IN (" + inConcepts + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Department + Labor Type
            else if (datesState && !depState && conState && !labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND fun.centro IN (" + inDepartments + ") " +
                        "AND e.manoobra IN (" + inLabors + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Department + Concept + Labor Type
            else if (datesState && !depState && !conState && !labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND fun.centro IN (" + inDepartments + ") " +
                        "AND fun.concepto IN (" + inConcepts + ") " +
                        "AND e.manoobra IN (" + inLabors + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Concepts
            else if (datesState && depState && !conState && labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND fun.concepto IN (" + inConcepts + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Concepts + Labor Type
            else if (datesState && depState && !conState && !labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND fun.concepto IN (" + inConcepts + ") " +
                        "AND e.manoobra IN (" + inLabors + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }
            //Labor Type
            else if (datesState && depState && conState && !labState)
            {
                query = "SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                        "FROM empleados e " +
                        "INNER JOIN fun_u_01 fun " +
                        "ON fun.empresa = 1 " +
                        "AND e.manoobra IN (" + inLabors + ") " +
                        "AND e.codigo = fun.codigo " +
                        "INNER JOIN cuentas c " +
                        "ON c.empresa = fun.empresa " +
                        "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                        "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC";
            }

            return query;
        }

        public string construct_SummaryQuery() //method to construct the query with the filters
        {
            //Common variables
            char identifier;
            string cmbName;
            string cbName;
            string query_summary = "";
            var inDepartments = "";
            var inConcepts = "";
            var inLabors = "";
            var dates = "";
            bool cb_daysState = cb_Days.Checked;

            //Load the methods with all 'TODOS' (Department, concept and labors)
            string allDepartments = selectAllDepartments();
            string allConcepts = selectAllConcepts();
            string allLabors = selectAllLabors();

            foreach (ComboBox combobox in lst_cmbUsed()) //identify if the combobox in the cmb_FilterUsed is Department or Concept
            {
                cmbName = combobox.Name.ToString();
                identifier = cmbName[4]; //Grab the character in the 4 position of the combobox name

                if (identifier.ToString() == "D") //Check if is department
                {
                    inDepartments = inDepartments + "'" + combobox.Text + "',"; //Create the SQL 'IN' clause for departments;
                    Filters_Parameters.selected_DepartmentsFilters = Filters_Parameters.selected_DepartmentsFilters + "" + combobox.Text + ","; //Add the text of the combobox to the global string. (used on exportations)
                }
                else if (identifier.ToString() == "C") //Check if is concept
                {
                    inConcepts = inConcepts + "'" + combobox.Text + "',"; //Create the SQL 'IN' clause for concepts;
                    Filters_Parameters.selected_ConceptsFilters = Filters_Parameters.selected_ConceptsFilters + "" + combobox.Text + ","; //Add the text of the combobox to the global list. (used on exportations)
                }
            }// 
                                                           // Check all Combobox and Checkboxes to find which ones were selected
            foreach (CheckBox checkbox in lst_cbUseds())   //
            {
                cbName = checkbox.Name.ToString();
                identifier = cbName[6]; //Grab the character in the 6 position of the combobox name (Only for TODOS)
                char identifierRestLabors = cbName[8]; //Grab the character in the 3 position to check if is Labor

                if (identifier.ToString() == "D") //All departments
                {
                    inDepartments = allDepartments;
                    Filters_Parameters.todos_Departments = selectAllDepartments_ForHeaderExport();
                }
                else if (identifier.ToString() == "C") //All concepts
                {
                    inConcepts = allConcepts;
                    Filters_Parameters.todos_Concepts = selectAllConcepts_ForHeaderExport();
                }

                else if (identifier.ToString() == "L") //All labors
                {
                    inLabors = allLabors;
                    Filters_Parameters.todos_Labors = selectAllLabors_ForHeaderExport();
                }
                else if (identifierRestLabors.ToString() == "D" || identifierRestLabors.ToString() == "I" || identifierRestLabors.ToString() == "S")
                {
                    inLabors = inLabors + "'" + checkbox.Text + "',";
                    Filters_Parameters.selected_LaborsFilters = Filters_Parameters.selected_LaborsFilters + "" + checkbox.Text + ",";
                }
            }//

            if (cb_daysState && dtp_From.Value != DateTime.Now && dtp_To.Value != DateTime.Now) //Check if date filter was used
            {
                if (dtp_From.Value > dtp_To.Value)
                {
                    MessageBox.Show("The final date should be greater then the initial.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    dates = dates + "fun.fch_pago BETWEEN convert(datetime, '" + dtp_From.Value.ToString("yyyy-MM-dd") + "') AND convert(datetime, '" + dtp_To.Value.ToString("yyyy-MM-dd") + "') ";
                }
            } //Check if Date filter was selected

            //Variables used to check if the Departments, Concepts or Labors was selected
            bool depState = string.IsNullOrEmpty(inDepartments);
            bool conState = string.IsNullOrEmpty(inConcepts);
            bool labState = string.IsNullOrEmpty(inLabors);
            bool datesState = string.IsNullOrEmpty(dates);

            if (!depState)
            {
                inDepartments = inDepartments.Remove(inDepartments.Length - 1); //Remove the last comma.
            }// If is not empty, remove the remanescent comma.
            if (!conState)
            {
                inConcepts = inConcepts.Remove(inConcepts.Length - 1); //Remove the last comma.
            }// If is not empty, remove the remanescent comma.
            if (!labState)
            {
                inLabors = inLabors.Remove(inLabors.Length - 1); //Remove the last comma.

            }// If is not empty, remove the remanescent comma.

            //Date
            if (!datesState && depState && conState && labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND " + dates + " " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
                MessageBox.Show("entrou no if 1");
            }
            //Date + Department
            else if (!datesState && !depState && conState && labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND " + dates + " " +
                                "AND fun.centro IN (" + inDepartments + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }
            //Date + Concepts
            else if (!datesState && depState && !conState && labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND " + dates + " " +
                                "AND fun.concepto IN (" + inConcepts + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }
            //Date + Labor Type
            else if (!datesState && depState && conState && !labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND " + dates + " " +
                                "AND e.manoobra IN (" + inLabors + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }
            //Date + Department + Concept
            else if (!datesState && !depState && !conState && labState)
            {
                 query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND " + dates + " " +
                                "AND fun.centro IN (" + inDepartments + ") " +
                                "AND fun.concepto IN (" + inConcepts + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }
            //Date + Department + Labor Type
            else if (!datesState && !depState && conState && !labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND " + dates + " " +
                                "AND fun.centro IN (" + inDepartments + ") " +
                                "AND e.manoobra IN (" + inLabors + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }
            //Date + Department + Concept + Labor Type
            else if (!datesState && !depState && !conState && !labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND " + dates + " " +
                                "AND fun.concepto IN (" + inConcepts + ") " +
                                "AND fun.centro IN (" + inDepartments + ") " +
                                "AND e.manoobra IN (" + inLabors + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }
            //Department
            else if (datesState && !depState && conState && labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND fun.centro IN (" + inDepartments + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }
            //Department + Concept
            else if (datesState && !depState && !conState && labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND fun.centro IN (" + inDepartments + ") " +
                                "AND fun.concepto IN (" + inConcepts + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }
            //Department + Labor Type
            else if (datesState && !depState && conState && !labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND fun.centro IN (" + inDepartments + ") " +
                                "AND e.manoobra IN (" + inLabors + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";

            }
            //Department + Concept + Labor Type
            else if (datesState && !depState && !conState && !labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND fun.centro IN (" + inDepartments + ") " +
                                "AND fun.concepto IN (" + inConcepts + ") " +
                                "AND e.manoobra IN (" + inLabors + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }
            //Concepts
            else if (datesState && depState && !conState && labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND fun.concepto IN (" + inConcepts + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }
            //Concepts + Labor Type
            else if (datesState && depState && !conState && !labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND fun.concepto IN (" + inConcepts + ") " +
                                "AND e.manoobra IN (" + inLabors + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }
            //Labor Type
            else if (datesState && depState && conState && !labState)
            {
                query_summary = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                "FROM empleados e " +
                                "INNER JOIN fun_u_01 fun " +
                                "ON fun.empresa = 1 " +
                                "AND e.manoobra IN (" + inLabors + ") " +
                                "and e.codigo = fun.codigo " +
                                "INNER JOIN cuentas c " +
                                "ON c.empresa = fun.empresa " +
                                "GROUP BY fun.concepto";
            }

            return query_summary;
        }

        public string save_summary_query_without_filter()
        {
            string summary_query = "SELECT  fun.concepto Concept, SUM(fun.importe) Money, SUM(fun.horas) Hours, SUM(e.sueldo) Wages, COUNT(fun.dias) Days " +
                                   "FROM empleados e " +
                                   "INNER JOIN fun_u_01 fun " +
                                   "ON fun.empresa = 1 " +
                                   "and e.codigo = fun.codigo " +
                                   "INNER JOIN cuentas c " +
                                   "ON c.empresa = fun.empresa " +
                                   "GROUP BY fun.concepto";

            return summary_query;
        } //method to save the query without any filter for summary

        private void btnSaveFilters_Click(object sender, EventArgs e)
        {
            if (AnyFilterSelected())
            {
                Filters_Parameters.filter_query = constructQuery();
                Filters_Parameters.summary_query = construct_SummaryQuery();
                this.Close();
            }
            else
            {
                MessageBox.Show("Please select at least 1 filter.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            Filters_Parameters.cb_allDepartments = cb_AllDepartments;
            Filters_Parameters.cb_allConcepts = cb_AllConcepts;
            Filters_Parameters.cb_allLabors = cb_AllLabors;
            Filters_Parameters.checkBox_Days = cb_Days;
        }

        //Labors filters methods
        public string selectAllLabors()
        {
            var inLabors = "";

            using (SqlConnection conn = db.OpenSQLConn())
            {
                try
                {
                    string query = "SELECT DISTINCT e.manoobra FROM dbo.empleados e " +
                                   "WHERE e.empresa = 1" +
                                   "AND e.manoobra NOT IN (' ')" +
                                   "ORDER BY e.manoobra ASC";
                    SqlCommand command = new SqlCommand(query, conn);
                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                       inLabors = inLabors + ("'" + dr.GetString(0) + "',"); //Add every labor in the inLabors variable
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            Filters_Parameters.todos_Labors = inLabors;
            return inLabors;
           
        } //Method to load all labor types when 'TODOS' labors checkbox is checked

        public string selectAllLabors_ForHeaderExport()
        {
            var labors_with_commas = "";

            using (SqlConnection conn = db.OpenSQLConn())
            {
                try
                {
                    string query = "SELECT DISTINCT e.manoobra FROM dbo.empleados e " +
                                   "WHERE e.empresa = 1" +
                                   "AND e.manoobra NOT IN (' ')" +
                                   "ORDER BY e.manoobra ASC";
                    SqlCommand command = new SqlCommand(query, conn);
                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        labors_with_commas = labors_with_commas + ("" + dr.GetString(0) + ","); //Add every labor in the inLabors variable
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            labors_with_commas.Remove(labors_with_commas.Length - 1);
            Filters_Parameters.todos_Labors = labors_with_commas;
            return Filters_Parameters.todos_Labors;

        } //Method to load all labors when AllLabors checkbox is checked, separated with commas to be used on the Header of exportations
        //End Labors filters methods

        //Concepts filters methods
        public void hideConcepts_Components()
        {
            cmb_Concept1.Visible = false;
            lbl_CloseConcept1.Visible = false;
            cmb_Concept2.Visible = false;
            lbl_CloseConcept2.Visible = false;
            cmb_Concept3.Visible = false;
            lbl_CloseConcept3.Visible = false;
            cmb_Concept4.Visible = false;
            lbl_CloseConcept4.Visible = false;
            cmb_Concept5.Visible = false;
            lbl_CloseConcept5.Visible = false;
            cmb_Concept6.Visible = false;
            lbl_CloseConcept6.Visible = false;
            cmb_Concept7.Visible = false;
            lbl_CloseConcept7.Visible = false;
            cmb_Concept8.Visible = false;
            lbl_CloseConcept8.Visible = false;
        } //Make combobox and lbl of concepts hide

        public string selectAllConcepts()
        {
            using (SqlConnection conn = db.OpenSQLConn())
            {
                try
                {
                    string query = "SELECT DISTINCT fun.concepto FROM dbo.fun_u_01 fun " +
                                   "WHERE fun.empresa = 1" +
                                   "ORDER BY fun.concepto ASC";
                    SqlCommand command = new SqlCommand(query, conn);
                    SqlDataReader dr = command.ExecuteReader();

                    List<String> lst_Concepts = new List<string>();

                    while (dr.Read())
                    {
                        lst_Concepts.Add("'" + dr.GetString(0) + "',"); //Add every concept in the lst_Concepts
                    }

                    foreach(string concept in lst_Concepts) // loop thru lst_Concepts and add each one to allConcepts string
                    {
                        allConcepts = allConcepts + concept;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            return allConcepts;
        } //Method to load all concepts when AllConcepts checkbox is checked

        public string selectAllConcepts_ForHeaderExport()
        {
            var concepts_with_commas = "";
            using (SqlConnection conn = db.OpenSQLConn())
            {
                try
                {
                    string query = "SELECT DISTINCT fun.concepto FROM dbo.fun_u_01 fun " +
                                   "WHERE fun.empresa = 1" +
                                   "ORDER BY fun.concepto ASC";
                    SqlCommand command = new SqlCommand(query, conn);
                    SqlDataReader dr = command.ExecuteReader();

                    List<string> lst_ConceptsWithCommas = new List<string>();

                    while (dr.Read())
                    {
                        lst_ConceptsWithCommas.Add("" + dr.GetString(0) + ","); //Add every concept in the lst_Concepts
                    }

                    foreach (string concept in lst_ConceptsWithCommas) // loop thru lst_Concepts and add each one to allConcepts string
                    {
                        concepts_with_commas = concepts_with_commas + concept;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            concepts_with_commas = concepts_with_commas.Remove(concepts_with_commas.Length -1);
            Filters_Parameters.todos_Concepts = concepts_with_commas;
            return Filters_Parameters.todos_Concepts;
        } //Method to load all concepts when AllConcepts checkbox is checked, separated with commas to be used on the Header of exportations

        public void updateConceptsLeft()
        {
            lbl_ConceptsLeftValue.Text = cont_Concepts.ToString();
        }

        public void AddOneMoreConcept()
        {
            cont_Concepts--;

            if (cont_Concepts == 7)
            {
                fillConceptComboBox(cmb_Concept1);
                cmb_Concept1.Visible = true;
                lbl_CloseConcept1.Visible = true;
                updateConceptsLeft();
            }
            else if (cont_Concepts == 6)
            {
                fillConceptComboBox(cmb_Concept2);
                cmb_Concept2.Visible = true;
                lbl_CloseConcept2.Visible = true;
                lbl_CloseConcept1.Enabled = false;
                updateConceptsLeft();
            }
            else if (cont_Concepts == 5)
            {
                fillConceptComboBox(cmb_Concept3);
                cmb_Concept3.Visible = true;
                lbl_CloseConcept3.Visible = true;
                lbl_CloseConcept2.Enabled = false;
                updateConceptsLeft();
            }
            else if (cont_Concepts == 4)
            {
                fillConceptComboBox(cmb_Concept4);
                cmb_Concept4.Visible = true;
                lbl_CloseConcept4.Visible = true;
                lbl_CloseConcept3.Enabled = false;
                updateConceptsLeft();
            }
            else if (cont_Concepts == 3)
            {
                fillConceptComboBox(cmb_Concept5);
                cmb_Concept5.Visible = true;
                lbl_CloseConcept5.Visible = true;
                lbl_CloseConcept4.Enabled = false;
                updateConceptsLeft();
            }
            else if (cont_Concepts == 2)
            {
                fillConceptComboBox(cmb_Concept6);
                cmb_Concept6.Visible = true;
                lbl_CloseConcept6.Visible = true;
                lbl_CloseConcept5.Enabled = false;
                updateConceptsLeft();
            }
            else if (cont_Concepts == 1)
            {
                fillConceptComboBox(cmb_Concept7);
                cmb_Concept7.Visible = true;
                lbl_CloseConcept7.Visible = true;
                lbl_CloseConcept6.Enabled = false;
                updateConceptsLeft();
            }
            else
            {
                fillConceptComboBox(cmb_Concept8);
                cmb_Concept8.Visible = true;
                lbl_CloseConcept8.Visible = true;
                lbl_CloseConcept7.Enabled = false;
                btn_AddConcept.Visible = false;
                updateConceptsLeft();
            }
        } //Make the comboboxes appears accordingly the counter

        public void fillConceptComboBox(ComboBox comboBox) //Fill the combobox with Departments data
        {
            using (SqlConnection conn = db.OpenSQLConn())
            {
                try
                {
                    string query = "SELECT DISTINCT fun.concepto FROM dbo.fun_u_01 fun " +
                                   "WHERE fun.empresa = 1" +
                                   "ORDER BY fun.concepto ASC";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Concept");
                    comboBox.DisplayMember = "concepto";
                    comboBox.ValueMember = "concepto";
                    comboBox.DataSource = ds.Tables["Concept"];
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            comboBox.SelectedIndex = -1; //Make it appear with blank selected
        }

        private void btn_AddConcepts_Click(object sender, EventArgs e)
        {
            AddOneMoreConcept();
        }
        //End concepts filters methods

        //Departments filters methods
        public void hideDepartments_Components()
        {
            cmb_Department1.Visible = false;
            lbl_CloseDepartment1.Visible = false;
            cmb_Department2.Visible = false;
            lbl_CloseDepartment2.Visible = false;
            cmb_Department3.Visible = false;
            lbl_CloseDepartment3.Visible = false;
            cmb_Department4.Visible = false;
            lbl_CloseDepartment4.Visible = false;
            cmb_Department5.Visible = false;
            lbl_CloseDepartment5.Visible = false;
            cmb_Department6.Visible = false;
            lbl_CloseDepartment6.Visible = false;
            cmb_Department7.Visible = false;
            lbl_CloseDepartment7.Visible = false;
            cmb_Department8.Visible = false;
            lbl_CloseDepartment8.Visible = false;
        } //Make combobox and lbl of departments hide

        public string selectAllDepartments()
        {
            using (SqlConnection conn = db.OpenSQLConn())
            {
                try
                {
                    string query = "SELECT DISTINCT fun.centro FROM dbo.fun_u_01 fun " +
                                   "WHERE fun.empresa = 1" +
                                   "ORDER BY fun.centro ASC";
                    SqlCommand command = new SqlCommand(query, conn);
                    SqlDataReader dr = command.ExecuteReader();

                    List<String> lst_Departments = new List<string>();

                    while (dr.Read())
                    {
                        lst_Departments.Add("'" + dr.GetString(0) + "',"); //Add every concept in the lst_Departments
                    }

                    foreach (string department in lst_Departments) // loop thru lst_Concepts and add each one to allConcepts string
                    {
                        allDepartments = allDepartments + department;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            return allDepartments;
        } //Method to load all departments when 'TODOS' Departments is checked

        public string selectAllDepartments_ForHeaderExport()
        {
            var departments_with_commas = "";
            using (SqlConnection conn = db.OpenSQLConn())
            {
                try
                {
                    string query = "SELECT DISTINCT fun.centro FROM dbo.fun_u_01 fun " +
                                   "WHERE fun.empresa = 1" +
                                   "ORDER BY fun.centro ASC";
                    SqlCommand command = new SqlCommand(query, conn);
                    SqlDataReader dr = command.ExecuteReader();

                    List<string> lst_DepartmentsWithCommas = new List<string>();

                    while (dr.Read())
                    {
                        lst_DepartmentsWithCommas.Add("" + dr.GetString(0) + ","); //Add every concept in the lst_Concepts
                    }

                    foreach (string concept in lst_DepartmentsWithCommas) // loop thru lst_Concepts and add each one to allConcepts string
                    {
                        departments_with_commas = departments_with_commas + concept;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            departments_with_commas = departments_with_commas.Remove(departments_with_commas.Length - 1);
            Filters_Parameters.todos_Departments = departments_with_commas;
            return Filters_Parameters.todos_Departments;
        } //Method to load all concepts when AllConcepts checkbox is checked, separated with commas to be used on the Header of exportations

        public void updateDepartmentsLeft()
        {
            lbl_DepartmentsLeftValue.Text = cont_Departments.ToString();
        }

        public void AddOneMoreDepartment()
        {
            cont_Departments--;

            if (cont_Departments == 7)
            {
                fillDepartmentComboBox(cmb_Department1);
                cmb_Department1.Visible = true;
                lbl_CloseDepartment1.Visible = true;
                updateDepartmentsLeft();
            }
            else if (cont_Departments == 6)
            {
                fillDepartmentComboBox(cmb_Department2);
                cmb_Department2.Visible = true;
                lbl_CloseDepartment2.Visible = true;
                lbl_CloseDepartment1.Enabled = false;
                updateDepartmentsLeft();
            }
            else if(cont_Departments == 5)
            {
                fillDepartmentComboBox(cmb_Department3);
                cmb_Department3.Visible = true;
                lbl_CloseDepartment3.Visible = true;
                lbl_CloseDepartment2.Enabled = false;
                updateDepartmentsLeft();
            }
            else if(cont_Departments == 4)
            {
                fillDepartmentComboBox(cmb_Department4);
                cmb_Department4.Visible = true;
                lbl_CloseDepartment4.Visible = true;
                lbl_CloseDepartment3.Enabled = false;
                updateDepartmentsLeft();
            }
            else if(cont_Departments == 3)
            {
                fillDepartmentComboBox(cmb_Department5);
                cmb_Department5.Visible = true;
                lbl_CloseDepartment5.Visible = true;
                lbl_CloseDepartment4.Enabled = false;
                updateDepartmentsLeft();
            }
            else if(cont_Departments == 2)
            {
                fillDepartmentComboBox(cmb_Department6);
                cmb_Department6.Visible = true;
                lbl_CloseDepartment6.Visible = true;
                lbl_CloseDepartment5.Enabled = false;
                updateDepartmentsLeft();
            }
            else if(cont_Departments == 1)
            {
                fillDepartmentComboBox(cmb_Department7);
                cmb_Department7.Visible = true;
                lbl_CloseDepartment7.Visible = true;
                lbl_CloseDepartment6.Enabled = false;
                updateDepartmentsLeft();
            }
            else
            {
                fillDepartmentComboBox(cmb_Department8);
                cmb_Department8.Visible = true;
                lbl_CloseDepartment8.Visible = true;
                lbl_CloseDepartment7.Enabled = false;
                btn_AddDepartment.Visible = false;
                updateDepartmentsLeft();
            }
        } //Make the comboboxes appears accordingly the counter

        public void fillDepartmentComboBox(ComboBox comboBox) //Fill the combobox with Departments data
        {
            using (SqlConnection conn = db.OpenSQLConn())
            {
                try
                {
                    string query = "SELECT DISTINCT fun.centro FROM dbo.fun_u_01 fun " +
                                   "WHERE fun.empresa = 1" +
                                   "ORDER BY fun.centro ASC";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Department");
                    comboBox.DisplayMember = "centro";
                    comboBox.ValueMember = "centro";
                    comboBox.DataSource = ds.Tables["Department"];
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            comboBox.SelectedIndex = -1; //Make it appear with blank selected
        }

        private void btn_AddDepartment_Click(object sender, EventArgs e)
        {
            AddOneMoreDepartment();
        }
        //End departments filters methods

        private void frmFilters_Shown(object sender, EventArgs e)
        {
            this.CenterToScreen();

            Filters_Parameters.date_From = dtp_From;
            Filters_Parameters.date_To = dtp_To;

            lbl_DepartmentsLeftValue.Text = cont_Departments.ToString();
            lbl_ConceptsLeftValue.Text = cont_Concepts.ToString();

            cb_Days.Checked = false;

            //Setting dtp_From format to blank
            dtp_From.Value = DateTime.Today;
            dtp_From.Format = DateTimePickerFormat.Custom;
            dtp_From.FormatCustom = " ";
            dtp_From.Enabled = false;

            //Setting dtp_To format to blank
            dtp_To.Value = DateTime.Today;
            dtp_To.Format = DateTimePickerFormat.Custom;
            dtp_To.FormatCustom = " ";
            dtp_To.Enabled = false;

            hideDepartments_Components();
            hideConcepts_Components();

        }

        private void dtp_From_onValueChanged(object sender, EventArgs e)
        {
            //Changing the format of dtp_From in ManagerReport form
            dtp_From.Format = DateTimePickerFormat.Custom;
            dtp_From.FormatCustom = "yyyy-MM-dd"; 
        }

        private void dtp_To_onValueChanged(object sender, EventArgs e)
        {
            //Changing the format of dtp_To in ManagerReport form
            dtp_To.Format = DateTimePickerFormat.Custom;
            dtp_To.FormatCustom = "yyyy-MM-dd";
        }

        private void cb_Days_OnChange(object sender, EventArgs e)
        {
            if (cb_Days.Checked == false)
            {
                dtp_From.Format = DateTimePickerFormat.Custom;
                dtp_From.FormatCustom = " ";
                dtp_To.Format = DateTimePickerFormat.Custom;
                dtp_To.FormatCustom = " ";
                dtp_From.Enabled = false;
                dtp_To.Enabled = false;
                lbl_cb_days.Text = "Enable Calendars";
            }
            else
            {
                dtp_From.Format = DateTimePickerFormat.Custom;
                dtp_From.FormatCustom = " ";
                dtp_To.Format = DateTimePickerFormat.Custom;
                dtp_To.FormatCustom = " ";
                dtp_From.Enabled = true;
                dtp_To.Enabled = true;
                lbl_cb_days.Text = "Disable Calendars";
            }
        }

        private void frmFilters_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void lbl_CloseDepartment1_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseDepartment1.Font = new Font(lbl_CloseDepartment1.Font.Name, lbl_CloseDepartment1.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseDepartment1_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseDepartment1.Font = new Font(lbl_CloseDepartment1.Font.Name, lbl_CloseDepartment1.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseDepartment2_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseDepartment2.Font = new Font(lbl_CloseDepartment2.Font.Name, lbl_CloseDepartment2.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseDepartment2_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseDepartment2.Font = new Font(lbl_CloseDepartment2.Font.Name, lbl_CloseDepartment2.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseDepartment3_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseDepartment3.Font = new Font(lbl_CloseDepartment3.Font.Name, lbl_CloseDepartment3.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseDepartment3_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseDepartment3.Font = new Font(lbl_CloseDepartment3.Font.Name, lbl_CloseDepartment3.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseDepartment4_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseDepartment4.Font = new Font(lbl_CloseDepartment4.Font.Name, lbl_CloseDepartment4.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseDepartment4_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseDepartment4.Font = new Font(lbl_CloseDepartment4.Font.Name, lbl_CloseDepartment4.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseDepartment5_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseDepartment5.Font = new Font(lbl_CloseDepartment5.Font.Name, lbl_CloseDepartment5.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseDepartment5_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseDepartment5.Font = new Font(lbl_CloseDepartment5.Font.Name, lbl_CloseDepartment5.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseDepartment6_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseDepartment6.Font = new Font(lbl_CloseDepartment6.Font.Name, lbl_CloseDepartment6.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseDepartment6_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseDepartment6.Font = new Font(lbl_CloseDepartment6.Font.Name, lbl_CloseDepartment6.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseDepartment7_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseDepartment7.Font = new Font(lbl_CloseDepartment7.Font.Name, lbl_CloseDepartment7.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseDepartment7_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseDepartment7.Font = new Font(lbl_CloseDepartment7.Font.Name, lbl_CloseDepartment7.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseDepartment8_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseDepartment8.Font = new Font(lbl_CloseDepartment8.Font.Name, lbl_CloseDepartment8.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseDepartment8_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseDepartment8.Font = new Font(lbl_CloseDepartment8.Font.Name, lbl_CloseDepartment8.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseConcept1_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseConcept1.Font = new Font(lbl_CloseConcept1.Font.Name, lbl_CloseConcept1.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseConcept1_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseConcept1.Font = new Font(lbl_CloseConcept1.Font.Name, lbl_CloseConcept1.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseConcept2_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseConcept2.Font = new Font(lbl_CloseConcept2.Font.Name, lbl_CloseConcept2.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseConcept2_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseConcept2.Font = new Font(lbl_CloseConcept2.Font.Name, lbl_CloseConcept2.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseConcept3_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseConcept3.Font = new Font(lbl_CloseConcept3.Font.Name, lbl_CloseConcept3.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseConcept3_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseConcept3.Font = new Font(lbl_CloseConcept3.Font.Name, lbl_CloseConcept3.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseConcept4_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseConcept4.Font = new Font(lbl_CloseConcept4.Font.Name, lbl_CloseConcept4.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseConcept4_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseConcept4.Font = new Font(lbl_CloseConcept4.Font.Name, lbl_CloseConcept4.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseConcept5_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseConcept5.Font = new Font(lbl_CloseConcept5.Font.Name, lbl_CloseConcept5.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseConcept5_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseConcept5.Font = new Font(lbl_CloseConcept5.Font.Name, lbl_CloseConcept5.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseConcept6_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseConcept6.Font = new Font(lbl_CloseConcept6.Font.Name, lbl_CloseConcept6.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseConcept6_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseConcept6.Font = new Font(lbl_CloseConcept6.Font.Name, lbl_CloseConcept6.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseConcept7_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseConcept7.Font = new Font(lbl_CloseConcept7.Font.Name, lbl_CloseConcept7.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseConcept7_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseConcept7.Font = new Font(lbl_CloseConcept7.Font.Name, lbl_CloseConcept7.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseConcept8_MouseEnter(object sender, EventArgs e)
        {
            lbl_CloseConcept8.Font = new Font(lbl_CloseConcept8.Font.Name, lbl_CloseConcept8.Font.SizeInPoints, FontStyle.Bold);
        }

        private void lbl_CloseConcept8_MouseLeave(object sender, EventArgs e)
        {
            lbl_CloseConcept8.Font = new Font(lbl_CloseConcept8.Font.Name, lbl_CloseConcept8.Font.SizeInPoints, FontStyle.Regular);
        }

        private void lbl_CloseDepartment1_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, increase the counter, update how many still left.
            cmb_Department1.Visible = false;
            lbl_CloseDepartment1.Visible = false;
            cmb_Department1.ResetText(); //Reset the text
            cont_Departments++;
            updateDepartmentsLeft();
        }

        private void lbl_CloseDepartment2_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Department2.Visible = false;
            lbl_CloseDepartment2.Visible = false;

            if(lbl_CloseDepartment1.Enabled == false)
            {
                lbl_CloseDepartment1.Enabled = true;
            }
            cmb_Department2.ResetText(); //Reset the text
            cont_Departments++;
            updateDepartmentsLeft();
        }

        private void lbl_CloseDepartment3_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Department3.Visible = false;
            lbl_CloseDepartment3.Visible = false;

            if (lbl_CloseDepartment2.Enabled == false)
            {
                lbl_CloseDepartment2.Enabled = true;
            }

            cmb_Department3.ResetText(); //Reset the text
            cont_Departments++;
            updateDepartmentsLeft();
        }

        private void lbl_CloseDepartment4_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Department4.Visible = false;
            lbl_CloseDepartment4.Visible = false;

            if (lbl_CloseDepartment3.Enabled == false)
            {
                lbl_CloseDepartment3.Enabled = true;
            }

            cmb_Department4.ResetText(); //Reset the text
            cont_Departments++;
            updateDepartmentsLeft();
        }

        private void lbl_CloseDepartment5_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Department5.Visible = false;
            lbl_CloseDepartment5.Visible = false;

            if (lbl_CloseDepartment4.Enabled == false)
            {
                lbl_CloseDepartment4.Enabled = true;
            }
            cmb_Department5.ResetText(); //Reset the text
            cont_Departments++;
            updateDepartmentsLeft();
        }

        private void lbl_CloseDepartment6_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Department6.Visible = false;
            lbl_CloseDepartment6.Visible = false;

            if (lbl_CloseDepartment5.Enabled == false)
            {
                lbl_CloseDepartment5.Enabled = true;
            }
            cmb_Department6.ResetText(); //Reset the text
            cont_Departments++;
            updateDepartmentsLeft();
        }

        private void lbl_CloseDepartment7_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Department7.Visible = false;
            lbl_CloseDepartment7.Visible = false;

            if (lbl_CloseDepartment6.Enabled == false)
            {
                lbl_CloseDepartment6.Enabled = true;
            }
            cmb_Department7.ResetText(); //Reset the text
            cont_Departments++;
            updateDepartmentsLeft();
        }

        private void lbl_CloseDepartment8_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left and enable the 'Add' button
            cmb_Department8.Visible = false;
            lbl_CloseDepartment8.Visible = false;

            if (lbl_CloseDepartment7.Enabled == false)
            {
                lbl_CloseDepartment7.Enabled = true;
            }
            cmb_Department8.ResetText(); //Reset the text
            cont_Departments++;
            updateDepartmentsLeft();
            btn_AddDepartment.Visible = true;
        }

        private void lbl_CloseConcept1_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, increase the counter, update how many still left.
            cmb_Concept1.Visible = false;
            lbl_CloseConcept1.Visible = false;
            cmb_Concept1.ResetText(); //Reset the text
            cont_Concepts++;
            updateConceptsLeft();
        }

        private void lbl_CloseConcept2_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Concept2.Visible = false;
            lbl_CloseConcept2.Visible = false;

            if (lbl_CloseConcept1.Enabled == false)
            {
                lbl_CloseConcept1.Enabled = true;
            }
            cmb_Concept2.ResetText(); //Reset the text
            cont_Concepts++;
            updateConceptsLeft();
        }

        private void lbl_CloseConcept3_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Concept3.Visible = false;
            lbl_CloseConcept3.Visible = false;

            if (lbl_CloseConcept2.Enabled == false)
            {
                lbl_CloseConcept2.Enabled = true;
            }
            cmb_Concept3.ResetText(); //Reset the text
            cont_Concepts++;
            updateConceptsLeft();
        }

        private void lbl_CloseConcept4_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Concept4.Visible = false;
            lbl_CloseConcept4.Visible = false;

            if (lbl_CloseConcept3.Enabled == false)
            {
                lbl_CloseConcept3.Enabled = true;
            }
            cmb_Concept4.ResetText(); //Reset the text
            cont_Concepts++;
            updateConceptsLeft();
        }

        private void lbl_CloseConcept5_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Concept5.Visible = false;
            lbl_CloseConcept5.Visible = false;

            if (lbl_CloseConcept4.Enabled == false)
            {
                lbl_CloseConcept4.Enabled = true;
            }
            cmb_Concept5.ResetText(); //Reset the text
            cont_Concepts++;
            updateConceptsLeft();
        }

        private void lbl_CloseConcept6_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Concept6.Visible = false;
            lbl_CloseConcept6.Visible = false;

            if (lbl_CloseConcept5.Enabled == false)
            {
                lbl_CloseConcept5.Enabled = true;
            }
            cmb_Concept6.ResetText(); //Reset the text
            cont_Concepts++;
            updateConceptsLeft();
        }

        private void lbl_CloseConcept7_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left.
            cmb_Concept7.Visible = false;
            lbl_CloseConcept7.Visible = false;

            if (lbl_CloseConcept6.Enabled == false)
            {
                lbl_CloseConcept6.Enabled = true;
            }
            cmb_Concept7.ResetText(); //Reset the text
            cont_Concepts++;
            updateConceptsLeft();
        }

        private void lbl_CloseConcept8_Click(object sender, EventArgs e)
        {
            //Hide the combobox and the label, enable the previous label so that can be accessible, increase the counter, update how many still left and enable the 'Add' button
            cmb_Concept8.Visible = false;
            lbl_CloseConcept8.Visible = false;

            if (lbl_CloseConcept7.Enabled == false)
            {
                lbl_CloseConcept7.Enabled = true;
            }
            cmb_Concept8.ResetText();
            cont_Concepts++;
            updateConceptsLeft();
            btn_AddConcept.Visible = true;
        }

        private void cb_AllLabors_CheckedChanged(object sender, EventArgs e)
        {
            if(cb_AllLabors.Checked == true)
            {
                //Disable all other checkboxes
                cb_LaborDirecto.Enabled = false;
                cb_LaborIndirecto.Enabled = false;
                cb_LaborSalario.Enabled = false;
                cb_LaborDirecto.Checked = false;
                cb_LaborIndirecto.Checked = false;
                cb_LaborSalario.Checked = false;
            }
            else //Keep the other labors options active
            {
                cb_LaborDirecto.Enabled = true;
                cb_LaborIndirecto.Enabled = true;
                cb_LaborSalario.Enabled = true;
            }
        }

        private void cb_AllConcepts_CheckedChanged(object sender, EventArgs e)
        {
            string cmbName;
            char identifier;
            if (cb_AllConcepts.Checked == true)
            {
                btn_AddConcept.Enabled = false; //Disable button to add more concepts
                disableAllCloseConceptsLabels();

                foreach(ComboBox combobox in lst_cmbUsed()) //Reset the text(to avoid problems) and disable all the selected comboboxes
                {
                    cmbName = combobox.Name.ToString();
                    identifier = cmbName[4]; //Grab the 4th character to check if it's concept

                    if(identifier.ToString() == "C" && combobox.Visible == true)
                    {
                        combobox.Enabled = false;
                    }
                }
            }
            else
            {
                btn_AddConcept.Enabled = true; //Enable the button and let it follow the normal process
                enableAllCloseConceptsLabels();

                foreach (ComboBox combobox in lst_cmbUsed()) //Reset the text(to avoid problems) and disable all the selected comboboxes
                {
                    cmbName = combobox.Name.ToString();
                    identifier = cmbName[4]; //Grab the 4th character to check if it's concept

                    if (identifier.ToString() == "C" && combobox.Enabled == false)
                    {
                        combobox.Enabled = true;
                    }
                }
            }

            //Filters_Parameters.cb_allConcepts = cb_AllConcepts;
        }

        private void cb_AllDepartments_CheckedChanged(object sender, EventArgs e)
        {
            string cmbName;
            char identifier;
            if (cb_AllDepartments.Checked == true)
            {
                btn_AddDepartment.Enabled = false; //Disable button to add more concepts
                disableAllCloseDepartmentsLabels();

                foreach (ComboBox combobox in lst_cmbUsed()) //Reset the text(to avoid problems) and disable all the selected comboboxes
                {
                    cmbName = combobox.Name.ToString();
                    identifier = cmbName[4]; //Grab the 4th character to check if is concept

                    if (identifier.ToString() == "D" && combobox.Visible == true)
                    {
                        combobox.Enabled = false;
                    }
                }
            }
            else
            {
                btn_AddDepartment.Enabled = true; //Enable the button and let it follow the normal process
                enableAllCloseDepartmentsLabels();

                foreach (ComboBox combobox in lst_cmbUsed()) //Reset the text(to avoid problems) and disable all the selected comboboxes
                {
                    cmbName = combobox.Name.ToString();
                    identifier = cmbName[4]; //Grab the 4th character to check if is department

                    if (identifier.ToString() == "D" && combobox.Enabled == false)
                    {
                        combobox.Enabled = true;
                    }
                }
            }

            //Filters_Parameters.cb_allDepartments = cb_AllDepartments;
        }

        public void disableAllCloseConceptsLabels()
        {
            lbl_CloseConcept1.Enabled = false;
            lbl_CloseConcept2.Enabled = false;
            lbl_CloseConcept3.Enabled = false;
            lbl_CloseConcept4.Enabled = false;
            lbl_CloseConcept5.Enabled = false;
            lbl_CloseConcept6.Enabled = false;
            lbl_CloseConcept7.Enabled = false;
            lbl_CloseConcept8.Enabled = false;
        }

        public void enableAllCloseConceptsLabels()
        {
            lbl_CloseConcept1.Enabled = true;
            lbl_CloseConcept2.Enabled = true;
            lbl_CloseConcept3.Enabled = true;
            lbl_CloseConcept4.Enabled = true;
            lbl_CloseConcept5.Enabled = true;
            lbl_CloseConcept6.Enabled = true;
            lbl_CloseConcept7.Enabled = true;
            lbl_CloseConcept8.Enabled = true;
        }

        public void disableAllCloseDepartmentsLabels()
        {
            lbl_CloseDepartment1.Enabled = false;
            lbl_CloseDepartment2.Enabled = false;
            lbl_CloseDepartment3.Enabled = false;
            lbl_CloseDepartment4.Enabled = false;
            lbl_CloseDepartment5.Enabled = false;
            lbl_CloseDepartment6.Enabled = false;
            lbl_CloseDepartment7.Enabled = false;
            lbl_CloseDepartment8.Enabled = false;
        }

        public void enableAllCloseDepartmentsLabels()
        {
            lbl_CloseDepartment1.Enabled = true;
            lbl_CloseDepartment2.Enabled = true;
            lbl_CloseDepartment3.Enabled = true;
            lbl_CloseDepartment4.Enabled = true;
            lbl_CloseDepartment5.Enabled = true;
            lbl_CloseDepartment6.Enabled = true;
            lbl_CloseDepartment7.Enabled = true;
            lbl_CloseDepartment8.Enabled = true;
        }
    }

}
