﻿namespace Acument_Reports
{
    partial class frmFilters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFilters));
            this.grpDate = new System.Windows.Forms.GroupBox();
            this.lbl_cb_days = new System.Windows.Forms.Label();
            this.cb_Days = new Bunifu.Framework.UI.BunifuCheckbox();
            this.dtp_To = new Bunifu.Framework.UI.BunifuDatepicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.dtp_From = new Bunifu.Framework.UI.BunifuDatepicker();
            this.lblFrom = new System.Windows.Forms.Label();
            this.grpDepartments = new System.Windows.Forms.GroupBox();
            this.lbl_DepartmentsLeftValue = new System.Windows.Forms.Label();
            this.lbl_DepartmentsLeft = new System.Windows.Forms.Label();
            this.lbl_CloseDepartment8 = new System.Windows.Forms.Label();
            this.lbl_CloseDepartment6 = new System.Windows.Forms.Label();
            this.lbl_CloseDepartment7 = new System.Windows.Forms.Label();
            this.lbl_CloseDepartment5 = new System.Windows.Forms.Label();
            this.lbl_CloseDepartment4 = new System.Windows.Forms.Label();
            this.lbl_CloseDepartment3 = new System.Windows.Forms.Label();
            this.lbl_CloseDepartment2 = new System.Windows.Forms.Label();
            this.lbl_CloseDepartment1 = new System.Windows.Forms.Label();
            this.btn_AddDepartment = new System.Windows.Forms.Button();
            this.cmb_Department6 = new System.Windows.Forms.ComboBox();
            this.cmb_Department8 = new System.Windows.Forms.ComboBox();
            this.cmb_Department5 = new System.Windows.Forms.ComboBox();
            this.cmb_Department7 = new System.Windows.Forms.ComboBox();
            this.cmb_Department4 = new System.Windows.Forms.ComboBox();
            this.cmb_Department3 = new System.Windows.Forms.ComboBox();
            this.cmb_Department2 = new System.Windows.Forms.ComboBox();
            this.cb_AllDepartments = new System.Windows.Forms.CheckBox();
            this.cmb_Department1 = new System.Windows.Forms.ComboBox();
            this.grpConcepts = new System.Windows.Forms.GroupBox();
            this.lbl_ConceptsLeftValue = new System.Windows.Forms.Label();
            this.lbl_CloseConcept8 = new System.Windows.Forms.Label();
            this.lbl_ConceptsLeft = new System.Windows.Forms.Label();
            this.lbl_CloseConcept7 = new System.Windows.Forms.Label();
            this.lbl_CloseConcept6 = new System.Windows.Forms.Label();
            this.lbl_CloseConcept5 = new System.Windows.Forms.Label();
            this.lbl_CloseConcept4 = new System.Windows.Forms.Label();
            this.lbl_CloseConcept3 = new System.Windows.Forms.Label();
            this.btn_AddConcept = new System.Windows.Forms.Button();
            this.lbl_CloseConcept2 = new System.Windows.Forms.Label();
            this.lbl_CloseConcept1 = new System.Windows.Forms.Label();
            this.cmb_Concept4 = new System.Windows.Forms.ComboBox();
            this.cmb_Concept3 = new System.Windows.Forms.ComboBox();
            this.cmb_Concept2 = new System.Windows.Forms.ComboBox();
            this.cb_AllConcepts = new System.Windows.Forms.CheckBox();
            this.cmb_Concept8 = new System.Windows.Forms.ComboBox();
            this.cmb_Concept1 = new System.Windows.Forms.ComboBox();
            this.cmb_Concept5 = new System.Windows.Forms.ComboBox();
            this.cmb_Concept7 = new System.Windows.Forms.ComboBox();
            this.cmb_Concept6 = new System.Windows.Forms.ComboBox();
            this.grpLaborType = new System.Windows.Forms.GroupBox();
            this.cb_AllLabors = new System.Windows.Forms.CheckBox();
            this.cb_LaborSalario = new System.Windows.Forms.CheckBox();
            this.cb_LaborDirecto = new System.Windows.Forms.CheckBox();
            this.cb_LaborIndirecto = new System.Windows.Forms.CheckBox();
            this.btnSaveFilters = new Bunifu.Framework.UI.BunifuThinButton2();
            this.grpDate.SuspendLayout();
            this.grpDepartments.SuspendLayout();
            this.grpConcepts.SuspendLayout();
            this.grpLaborType.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDate
            // 
            this.grpDate.Controls.Add(this.lbl_cb_days);
            this.grpDate.Controls.Add(this.cb_Days);
            this.grpDate.Controls.Add(this.dtp_To);
            this.grpDate.Controls.Add(this.lblTo);
            this.grpDate.Controls.Add(this.dtp_From);
            this.grpDate.Controls.Add(this.lblFrom);
            this.grpDate.Location = new System.Drawing.Point(12, 12);
            this.grpDate.Name = "grpDate";
            this.grpDate.Size = new System.Drawing.Size(736, 121);
            this.grpDate.TabIndex = 7;
            this.grpDate.TabStop = false;
            this.grpDate.Text = "Date";
            // 
            // lbl_cb_days
            // 
            this.lbl_cb_days.AutoSize = true;
            this.lbl_cb_days.Location = new System.Drawing.Point(60, 36);
            this.lbl_cb_days.Name = "lbl_cb_days";
            this.lbl_cb_days.Size = new System.Drawing.Size(90, 13);
            this.lbl_cb_days.TabIndex = 20;
            this.lbl_cb_days.Text = "Enable Calendars";
            // 
            // cb_Days
            // 
            this.cb_Days.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.cb_Days.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.cb_Days.Checked = false;
            this.cb_Days.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.cb_Days.ForeColor = System.Drawing.Color.White;
            this.cb_Days.Location = new System.Drawing.Point(38, 32);
            this.cb_Days.Name = "cb_Days";
            this.cb_Days.Size = new System.Drawing.Size(20, 20);
            this.cb_Days.TabIndex = 19;
            this.cb_Days.OnChange += new System.EventHandler(this.cb_Days_OnChange);
            // 
            // dtp_To
            // 
            this.dtp_To.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.dtp_To.BorderRadius = 0;
            this.dtp_To.ForeColor = System.Drawing.Color.White;
            this.dtp_To.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_To.FormatCustom = null;
            this.dtp_To.Location = new System.Drawing.Point(270, 69);
            this.dtp_To.Name = "dtp_To";
            this.dtp_To.Size = new System.Drawing.Size(197, 34);
            this.dtp_To.TabIndex = 9;
            this.dtp_To.Value = new System.DateTime(2017, 12, 6, 0, 0, 0, 0);
            this.dtp_To.onValueChanged += new System.EventHandler(this.dtp_To_onValueChanged);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(247, 77);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(23, 13);
            this.lblTo.TabIndex = 6;
            this.lblTo.Text = "To:";
            // 
            // dtp_From
            // 
            this.dtp_From.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.dtp_From.BorderRadius = 0;
            this.dtp_From.ForeColor = System.Drawing.Color.White;
            this.dtp_From.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_From.FormatCustom = null;
            this.dtp_From.Location = new System.Drawing.Point(38, 69);
            this.dtp_From.Name = "dtp_From";
            this.dtp_From.Size = new System.Drawing.Size(197, 34);
            this.dtp_From.TabIndex = 4;
            this.dtp_From.Value = new System.DateTime(2017, 12, 20, 0, 0, 0, 0);
            this.dtp_From.onValueChanged += new System.EventHandler(this.dtp_From_onValueChanged);
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(6, 77);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(33, 13);
            this.lblFrom.TabIndex = 5;
            this.lblFrom.Text = "From:";
            // 
            // grpDepartments
            // 
            this.grpDepartments.Controls.Add(this.lbl_DepartmentsLeftValue);
            this.grpDepartments.Controls.Add(this.lbl_DepartmentsLeft);
            this.grpDepartments.Controls.Add(this.lbl_CloseDepartment8);
            this.grpDepartments.Controls.Add(this.lbl_CloseDepartment6);
            this.grpDepartments.Controls.Add(this.lbl_CloseDepartment7);
            this.grpDepartments.Controls.Add(this.lbl_CloseDepartment5);
            this.grpDepartments.Controls.Add(this.lbl_CloseDepartment4);
            this.grpDepartments.Controls.Add(this.lbl_CloseDepartment3);
            this.grpDepartments.Controls.Add(this.lbl_CloseDepartment2);
            this.grpDepartments.Controls.Add(this.lbl_CloseDepartment1);
            this.grpDepartments.Controls.Add(this.btn_AddDepartment);
            this.grpDepartments.Controls.Add(this.cmb_Department6);
            this.grpDepartments.Controls.Add(this.cmb_Department8);
            this.grpDepartments.Controls.Add(this.cmb_Department5);
            this.grpDepartments.Controls.Add(this.cmb_Department7);
            this.grpDepartments.Controls.Add(this.cmb_Department4);
            this.grpDepartments.Controls.Add(this.cmb_Department3);
            this.grpDepartments.Controls.Add(this.cmb_Department2);
            this.grpDepartments.Controls.Add(this.cb_AllDepartments);
            this.grpDepartments.Controls.Add(this.cmb_Department1);
            this.grpDepartments.Location = new System.Drawing.Point(12, 139);
            this.grpDepartments.Name = "grpDepartments";
            this.grpDepartments.Size = new System.Drawing.Size(235, 274);
            this.grpDepartments.TabIndex = 21;
            this.grpDepartments.TabStop = false;
            this.grpDepartments.Text = "Departments";
            // 
            // lbl_DepartmentsLeftValue
            // 
            this.lbl_DepartmentsLeftValue.AutoSize = true;
            this.lbl_DepartmentsLeftValue.Location = new System.Drawing.Point(135, 18);
            this.lbl_DepartmentsLeftValue.Name = "lbl_DepartmentsLeftValue";
            this.lbl_DepartmentsLeftValue.Size = new System.Drawing.Size(0, 13);
            this.lbl_DepartmentsLeftValue.TabIndex = 43;
            // 
            // lbl_DepartmentsLeft
            // 
            this.lbl_DepartmentsLeft.AutoSize = true;
            this.lbl_DepartmentsLeft.Location = new System.Drawing.Point(109, 18);
            this.lbl_DepartmentsLeft.Name = "lbl_DepartmentsLeft";
            this.lbl_DepartmentsLeft.Size = new System.Drawing.Size(28, 13);
            this.lbl_DepartmentsLeft.TabIndex = 42;
            this.lbl_DepartmentsLeft.Text = "Left:";
            // 
            // lbl_CloseDepartment8
            // 
            this.lbl_CloseDepartment8.AutoSize = true;
            this.lbl_CloseDepartment8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseDepartment8.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseDepartment8.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseDepartment8.Location = new System.Drawing.Point(156, 239);
            this.lbl_CloseDepartment8.Name = "lbl_CloseDepartment8";
            this.lbl_CloseDepartment8.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseDepartment8.TabIndex = 41;
            this.lbl_CloseDepartment8.Text = "X";
            this.lbl_CloseDepartment8.Click += new System.EventHandler(this.lbl_CloseDepartment8_Click);
            this.lbl_CloseDepartment8.MouseEnter += new System.EventHandler(this.lbl_CloseDepartment8_MouseEnter);
            this.lbl_CloseDepartment8.MouseLeave += new System.EventHandler(this.lbl_CloseDepartment8_MouseLeave);
            // 
            // lbl_CloseDepartment6
            // 
            this.lbl_CloseDepartment6.AutoSize = true;
            this.lbl_CloseDepartment6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseDepartment6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseDepartment6.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseDepartment6.Location = new System.Drawing.Point(156, 185);
            this.lbl_CloseDepartment6.Name = "lbl_CloseDepartment6";
            this.lbl_CloseDepartment6.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseDepartment6.TabIndex = 33;
            this.lbl_CloseDepartment6.Text = "X";
            this.lbl_CloseDepartment6.Click += new System.EventHandler(this.lbl_CloseDepartment6_Click);
            this.lbl_CloseDepartment6.MouseEnter += new System.EventHandler(this.lbl_CloseDepartment6_MouseEnter);
            this.lbl_CloseDepartment6.MouseLeave += new System.EventHandler(this.lbl_CloseDepartment6_MouseLeave);
            // 
            // lbl_CloseDepartment7
            // 
            this.lbl_CloseDepartment7.AutoSize = true;
            this.lbl_CloseDepartment7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseDepartment7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseDepartment7.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseDepartment7.Location = new System.Drawing.Point(156, 212);
            this.lbl_CloseDepartment7.Name = "lbl_CloseDepartment7";
            this.lbl_CloseDepartment7.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseDepartment7.TabIndex = 40;
            this.lbl_CloseDepartment7.Text = "X";
            this.lbl_CloseDepartment7.Click += new System.EventHandler(this.lbl_CloseDepartment7_Click);
            this.lbl_CloseDepartment7.MouseEnter += new System.EventHandler(this.lbl_CloseDepartment7_MouseEnter);
            this.lbl_CloseDepartment7.MouseLeave += new System.EventHandler(this.lbl_CloseDepartment7_MouseLeave);
            // 
            // lbl_CloseDepartment5
            // 
            this.lbl_CloseDepartment5.AutoSize = true;
            this.lbl_CloseDepartment5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseDepartment5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseDepartment5.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseDepartment5.Location = new System.Drawing.Point(156, 158);
            this.lbl_CloseDepartment5.Name = "lbl_CloseDepartment5";
            this.lbl_CloseDepartment5.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseDepartment5.TabIndex = 32;
            this.lbl_CloseDepartment5.Text = "X";
            this.lbl_CloseDepartment5.Click += new System.EventHandler(this.lbl_CloseDepartment5_Click);
            this.lbl_CloseDepartment5.MouseEnter += new System.EventHandler(this.lbl_CloseDepartment5_MouseEnter);
            this.lbl_CloseDepartment5.MouseLeave += new System.EventHandler(this.lbl_CloseDepartment5_MouseLeave);
            // 
            // lbl_CloseDepartment4
            // 
            this.lbl_CloseDepartment4.AutoSize = true;
            this.lbl_CloseDepartment4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseDepartment4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseDepartment4.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseDepartment4.Location = new System.Drawing.Point(156, 131);
            this.lbl_CloseDepartment4.Name = "lbl_CloseDepartment4";
            this.lbl_CloseDepartment4.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseDepartment4.TabIndex = 31;
            this.lbl_CloseDepartment4.Text = "X";
            this.lbl_CloseDepartment4.Click += new System.EventHandler(this.lbl_CloseDepartment4_Click);
            this.lbl_CloseDepartment4.MouseEnter += new System.EventHandler(this.lbl_CloseDepartment4_MouseEnter);
            this.lbl_CloseDepartment4.MouseLeave += new System.EventHandler(this.lbl_CloseDepartment4_MouseLeave);
            // 
            // lbl_CloseDepartment3
            // 
            this.lbl_CloseDepartment3.AutoSize = true;
            this.lbl_CloseDepartment3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseDepartment3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseDepartment3.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseDepartment3.Location = new System.Drawing.Point(156, 104);
            this.lbl_CloseDepartment3.Name = "lbl_CloseDepartment3";
            this.lbl_CloseDepartment3.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseDepartment3.TabIndex = 30;
            this.lbl_CloseDepartment3.Text = "X";
            this.lbl_CloseDepartment3.Click += new System.EventHandler(this.lbl_CloseDepartment3_Click);
            this.lbl_CloseDepartment3.MouseEnter += new System.EventHandler(this.lbl_CloseDepartment3_MouseEnter);
            this.lbl_CloseDepartment3.MouseLeave += new System.EventHandler(this.lbl_CloseDepartment3_MouseLeave);
            // 
            // lbl_CloseDepartment2
            // 
            this.lbl_CloseDepartment2.AutoSize = true;
            this.lbl_CloseDepartment2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseDepartment2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseDepartment2.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseDepartment2.Location = new System.Drawing.Point(156, 77);
            this.lbl_CloseDepartment2.Name = "lbl_CloseDepartment2";
            this.lbl_CloseDepartment2.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseDepartment2.TabIndex = 29;
            this.lbl_CloseDepartment2.Text = "X";
            this.lbl_CloseDepartment2.Click += new System.EventHandler(this.lbl_CloseDepartment2_Click);
            this.lbl_CloseDepartment2.MouseEnter += new System.EventHandler(this.lbl_CloseDepartment2_MouseEnter);
            this.lbl_CloseDepartment2.MouseLeave += new System.EventHandler(this.lbl_CloseDepartment2_MouseLeave);
            // 
            // lbl_CloseDepartment1
            // 
            this.lbl_CloseDepartment1.AutoSize = true;
            this.lbl_CloseDepartment1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseDepartment1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseDepartment1.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseDepartment1.Location = new System.Drawing.Point(156, 50);
            this.lbl_CloseDepartment1.Name = "lbl_CloseDepartment1";
            this.lbl_CloseDepartment1.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseDepartment1.TabIndex = 28;
            this.lbl_CloseDepartment1.Text = "X";
            this.lbl_CloseDepartment1.Click += new System.EventHandler(this.lbl_CloseDepartment1_Click);
            this.lbl_CloseDepartment1.MouseEnter += new System.EventHandler(this.lbl_CloseDepartment1_MouseEnter);
            this.lbl_CloseDepartment1.MouseLeave += new System.EventHandler(this.lbl_CloseDepartment1_MouseLeave);
            // 
            // btn_AddDepartment
            // 
            this.btn_AddDepartment.Location = new System.Drawing.Point(154, 13);
            this.btn_AddDepartment.Name = "btn_AddDepartment";
            this.btn_AddDepartment.Size = new System.Drawing.Size(75, 23);
            this.btn_AddDepartment.TabIndex = 27;
            this.btn_AddDepartment.Text = "Add";
            this.btn_AddDepartment.UseVisualStyleBackColor = true;
            this.btn_AddDepartment.Click += new System.EventHandler(this.btn_AddDepartment_Click);
            // 
            // cmb_Department6
            // 
            this.cmb_Department6.FormattingEnabled = true;
            this.cmb_Department6.Location = new System.Drawing.Point(9, 185);
            this.cmb_Department6.Name = "cmb_Department6";
            this.cmb_Department6.Size = new System.Drawing.Size(141, 21);
            this.cmb_Department6.TabIndex = 10;
            // 
            // cmb_Department8
            // 
            this.cmb_Department8.FormattingEnabled = true;
            this.cmb_Department8.Location = new System.Drawing.Point(9, 239);
            this.cmb_Department8.Name = "cmb_Department8";
            this.cmb_Department8.Size = new System.Drawing.Size(141, 21);
            this.cmb_Department8.TabIndex = 26;
            // 
            // cmb_Department5
            // 
            this.cmb_Department5.FormattingEnabled = true;
            this.cmb_Department5.Location = new System.Drawing.Point(9, 158);
            this.cmb_Department5.Name = "cmb_Department5";
            this.cmb_Department5.Size = new System.Drawing.Size(141, 21);
            this.cmb_Department5.TabIndex = 9;
            // 
            // cmb_Department7
            // 
            this.cmb_Department7.FormattingEnabled = true;
            this.cmb_Department7.Location = new System.Drawing.Point(9, 212);
            this.cmb_Department7.Name = "cmb_Department7";
            this.cmb_Department7.Size = new System.Drawing.Size(141, 21);
            this.cmb_Department7.TabIndex = 25;
            // 
            // cmb_Department4
            // 
            this.cmb_Department4.FormattingEnabled = true;
            this.cmb_Department4.Location = new System.Drawing.Point(9, 131);
            this.cmb_Department4.Name = "cmb_Department4";
            this.cmb_Department4.Size = new System.Drawing.Size(141, 21);
            this.cmb_Department4.TabIndex = 8;
            // 
            // cmb_Department3
            // 
            this.cmb_Department3.FormattingEnabled = true;
            this.cmb_Department3.Location = new System.Drawing.Point(9, 104);
            this.cmb_Department3.Name = "cmb_Department3";
            this.cmb_Department3.Size = new System.Drawing.Size(141, 21);
            this.cmb_Department3.TabIndex = 7;
            // 
            // cmb_Department2
            // 
            this.cmb_Department2.FormattingEnabled = true;
            this.cmb_Department2.Location = new System.Drawing.Point(9, 77);
            this.cmb_Department2.Name = "cmb_Department2";
            this.cmb_Department2.Size = new System.Drawing.Size(141, 21);
            this.cmb_Department2.TabIndex = 6;
            // 
            // cb_AllDepartments
            // 
            this.cb_AllDepartments.AutoSize = true;
            this.cb_AllDepartments.Location = new System.Drawing.Point(9, 19);
            this.cb_AllDepartments.Name = "cb_AllDepartments";
            this.cb_AllDepartments.Size = new System.Drawing.Size(64, 17);
            this.cb_AllDepartments.TabIndex = 5;
            this.cb_AllDepartments.Text = "TODOS";
            this.cb_AllDepartments.UseVisualStyleBackColor = true;
            this.cb_AllDepartments.CheckedChanged += new System.EventHandler(this.cb_AllDepartments_CheckedChanged);
            // 
            // cmb_Department1
            // 
            this.cmb_Department1.FormattingEnabled = true;
            this.cmb_Department1.Location = new System.Drawing.Point(9, 50);
            this.cmb_Department1.Name = "cmb_Department1";
            this.cmb_Department1.Size = new System.Drawing.Size(141, 21);
            this.cmb_Department1.TabIndex = 0;
            // 
            // grpConcepts
            // 
            this.grpConcepts.Controls.Add(this.lbl_ConceptsLeftValue);
            this.grpConcepts.Controls.Add(this.lbl_CloseConcept8);
            this.grpConcepts.Controls.Add(this.lbl_ConceptsLeft);
            this.grpConcepts.Controls.Add(this.lbl_CloseConcept7);
            this.grpConcepts.Controls.Add(this.lbl_CloseConcept6);
            this.grpConcepts.Controls.Add(this.lbl_CloseConcept5);
            this.grpConcepts.Controls.Add(this.lbl_CloseConcept4);
            this.grpConcepts.Controls.Add(this.lbl_CloseConcept3);
            this.grpConcepts.Controls.Add(this.btn_AddConcept);
            this.grpConcepts.Controls.Add(this.lbl_CloseConcept2);
            this.grpConcepts.Controls.Add(this.lbl_CloseConcept1);
            this.grpConcepts.Controls.Add(this.cmb_Concept4);
            this.grpConcepts.Controls.Add(this.cmb_Concept3);
            this.grpConcepts.Controls.Add(this.cmb_Concept2);
            this.grpConcepts.Controls.Add(this.cb_AllConcepts);
            this.grpConcepts.Controls.Add(this.cmb_Concept8);
            this.grpConcepts.Controls.Add(this.cmb_Concept1);
            this.grpConcepts.Controls.Add(this.cmb_Concept5);
            this.grpConcepts.Controls.Add(this.cmb_Concept7);
            this.grpConcepts.Controls.Add(this.cmb_Concept6);
            this.grpConcepts.Location = new System.Drawing.Point(262, 139);
            this.grpConcepts.Name = "grpConcepts";
            this.grpConcepts.Size = new System.Drawing.Size(235, 274);
            this.grpConcepts.TabIndex = 22;
            this.grpConcepts.TabStop = false;
            this.grpConcepts.Text = "Concepts";
            // 
            // lbl_ConceptsLeftValue
            // 
            this.lbl_ConceptsLeftValue.AutoSize = true;
            this.lbl_ConceptsLeftValue.Location = new System.Drawing.Point(135, 18);
            this.lbl_ConceptsLeftValue.Name = "lbl_ConceptsLeftValue";
            this.lbl_ConceptsLeftValue.Size = new System.Drawing.Size(0, 13);
            this.lbl_ConceptsLeftValue.TabIndex = 45;
            // 
            // lbl_CloseConcept8
            // 
            this.lbl_CloseConcept8.AutoSize = true;
            this.lbl_CloseConcept8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseConcept8.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseConcept8.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseConcept8.Location = new System.Drawing.Point(198, 233);
            this.lbl_CloseConcept8.Name = "lbl_CloseConcept8";
            this.lbl_CloseConcept8.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseConcept8.TabIndex = 33;
            this.lbl_CloseConcept8.Text = "X";
            this.lbl_CloseConcept8.Click += new System.EventHandler(this.lbl_CloseConcept8_Click);
            this.lbl_CloseConcept8.MouseEnter += new System.EventHandler(this.lbl_CloseConcept8_MouseEnter);
            this.lbl_CloseConcept8.MouseLeave += new System.EventHandler(this.lbl_CloseConcept8_MouseLeave);
            // 
            // lbl_ConceptsLeft
            // 
            this.lbl_ConceptsLeft.AutoSize = true;
            this.lbl_ConceptsLeft.Location = new System.Drawing.Point(109, 18);
            this.lbl_ConceptsLeft.Name = "lbl_ConceptsLeft";
            this.lbl_ConceptsLeft.Size = new System.Drawing.Size(28, 13);
            this.lbl_ConceptsLeft.TabIndex = 44;
            this.lbl_ConceptsLeft.Text = "Left:";
            // 
            // lbl_CloseConcept7
            // 
            this.lbl_CloseConcept7.AutoSize = true;
            this.lbl_CloseConcept7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseConcept7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseConcept7.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseConcept7.Location = new System.Drawing.Point(198, 209);
            this.lbl_CloseConcept7.Name = "lbl_CloseConcept7";
            this.lbl_CloseConcept7.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseConcept7.TabIndex = 42;
            this.lbl_CloseConcept7.Text = "X";
            this.lbl_CloseConcept7.Click += new System.EventHandler(this.lbl_CloseConcept7_Click);
            this.lbl_CloseConcept7.MouseEnter += new System.EventHandler(this.lbl_CloseConcept7_MouseEnter);
            this.lbl_CloseConcept7.MouseLeave += new System.EventHandler(this.lbl_CloseConcept7_MouseLeave);
            // 
            // lbl_CloseConcept6
            // 
            this.lbl_CloseConcept6.AutoSize = true;
            this.lbl_CloseConcept6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseConcept6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseConcept6.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseConcept6.Location = new System.Drawing.Point(198, 185);
            this.lbl_CloseConcept6.Name = "lbl_CloseConcept6";
            this.lbl_CloseConcept6.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseConcept6.TabIndex = 39;
            this.lbl_CloseConcept6.Text = "X";
            this.lbl_CloseConcept6.Click += new System.EventHandler(this.lbl_CloseConcept6_Click);
            this.lbl_CloseConcept6.MouseEnter += new System.EventHandler(this.lbl_CloseConcept6_MouseEnter);
            this.lbl_CloseConcept6.MouseLeave += new System.EventHandler(this.lbl_CloseConcept6_MouseLeave);
            // 
            // lbl_CloseConcept5
            // 
            this.lbl_CloseConcept5.AutoSize = true;
            this.lbl_CloseConcept5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseConcept5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseConcept5.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseConcept5.Location = new System.Drawing.Point(198, 158);
            this.lbl_CloseConcept5.Name = "lbl_CloseConcept5";
            this.lbl_CloseConcept5.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseConcept5.TabIndex = 38;
            this.lbl_CloseConcept5.Text = "X";
            this.lbl_CloseConcept5.Click += new System.EventHandler(this.lbl_CloseConcept5_Click);
            this.lbl_CloseConcept5.MouseEnter += new System.EventHandler(this.lbl_CloseConcept5_MouseEnter);
            this.lbl_CloseConcept5.MouseLeave += new System.EventHandler(this.lbl_CloseConcept5_MouseLeave);
            // 
            // lbl_CloseConcept4
            // 
            this.lbl_CloseConcept4.AutoSize = true;
            this.lbl_CloseConcept4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseConcept4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseConcept4.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseConcept4.Location = new System.Drawing.Point(198, 131);
            this.lbl_CloseConcept4.Name = "lbl_CloseConcept4";
            this.lbl_CloseConcept4.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseConcept4.TabIndex = 37;
            this.lbl_CloseConcept4.Text = "X";
            this.lbl_CloseConcept4.Click += new System.EventHandler(this.lbl_CloseConcept4_Click);
            this.lbl_CloseConcept4.MouseEnter += new System.EventHandler(this.lbl_CloseConcept4_MouseEnter);
            this.lbl_CloseConcept4.MouseLeave += new System.EventHandler(this.lbl_CloseConcept4_MouseLeave);
            // 
            // lbl_CloseConcept3
            // 
            this.lbl_CloseConcept3.AutoSize = true;
            this.lbl_CloseConcept3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseConcept3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseConcept3.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseConcept3.Location = new System.Drawing.Point(198, 104);
            this.lbl_CloseConcept3.Name = "lbl_CloseConcept3";
            this.lbl_CloseConcept3.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseConcept3.TabIndex = 36;
            this.lbl_CloseConcept3.Text = "X";
            this.lbl_CloseConcept3.Click += new System.EventHandler(this.lbl_CloseConcept3_Click);
            this.lbl_CloseConcept3.MouseEnter += new System.EventHandler(this.lbl_CloseConcept3_MouseEnter);
            this.lbl_CloseConcept3.MouseLeave += new System.EventHandler(this.lbl_CloseConcept3_MouseLeave);
            // 
            // btn_AddConcept
            // 
            this.btn_AddConcept.Location = new System.Drawing.Point(154, 13);
            this.btn_AddConcept.Name = "btn_AddConcept";
            this.btn_AddConcept.Size = new System.Drawing.Size(75, 23);
            this.btn_AddConcept.TabIndex = 28;
            this.btn_AddConcept.Text = "Add";
            this.btn_AddConcept.UseVisualStyleBackColor = true;
            this.btn_AddConcept.Click += new System.EventHandler(this.btn_AddConcepts_Click);
            // 
            // lbl_CloseConcept2
            // 
            this.lbl_CloseConcept2.AutoSize = true;
            this.lbl_CloseConcept2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseConcept2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseConcept2.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseConcept2.Location = new System.Drawing.Point(198, 77);
            this.lbl_CloseConcept2.Name = "lbl_CloseConcept2";
            this.lbl_CloseConcept2.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseConcept2.TabIndex = 35;
            this.lbl_CloseConcept2.Text = "X";
            this.lbl_CloseConcept2.Click += new System.EventHandler(this.lbl_CloseConcept2_Click);
            this.lbl_CloseConcept2.MouseEnter += new System.EventHandler(this.lbl_CloseConcept2_MouseEnter);
            this.lbl_CloseConcept2.MouseLeave += new System.EventHandler(this.lbl_CloseConcept2_MouseLeave);
            // 
            // lbl_CloseConcept1
            // 
            this.lbl_CloseConcept1.AutoSize = true;
            this.lbl_CloseConcept1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_CloseConcept1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CloseConcept1.ForeColor = System.Drawing.Color.Red;
            this.lbl_CloseConcept1.Location = new System.Drawing.Point(198, 50);
            this.lbl_CloseConcept1.Name = "lbl_CloseConcept1";
            this.lbl_CloseConcept1.Size = new System.Drawing.Size(19, 21);
            this.lbl_CloseConcept1.TabIndex = 34;
            this.lbl_CloseConcept1.Text = "X";
            this.lbl_CloseConcept1.Click += new System.EventHandler(this.lbl_CloseConcept1_Click);
            this.lbl_CloseConcept1.MouseEnter += new System.EventHandler(this.lbl_CloseConcept1_MouseEnter);
            this.lbl_CloseConcept1.MouseLeave += new System.EventHandler(this.lbl_CloseConcept1_MouseLeave);
            // 
            // cmb_Concept4
            // 
            this.cmb_Concept4.FormattingEnabled = true;
            this.cmb_Concept4.Location = new System.Drawing.Point(6, 131);
            this.cmb_Concept4.Name = "cmb_Concept4";
            this.cmb_Concept4.Size = new System.Drawing.Size(190, 21);
            this.cmb_Concept4.TabIndex = 30;
            // 
            // cmb_Concept3
            // 
            this.cmb_Concept3.FormattingEnabled = true;
            this.cmb_Concept3.Location = new System.Drawing.Point(6, 104);
            this.cmb_Concept3.Name = "cmb_Concept3";
            this.cmb_Concept3.Size = new System.Drawing.Size(190, 21);
            this.cmb_Concept3.TabIndex = 29;
            // 
            // cmb_Concept2
            // 
            this.cmb_Concept2.FormattingEnabled = true;
            this.cmb_Concept2.Location = new System.Drawing.Point(6, 77);
            this.cmb_Concept2.Name = "cmb_Concept2";
            this.cmb_Concept2.Size = new System.Drawing.Size(190, 21);
            this.cmb_Concept2.TabIndex = 28;
            // 
            // cb_AllConcepts
            // 
            this.cb_AllConcepts.AutoSize = true;
            this.cb_AllConcepts.Location = new System.Drawing.Point(6, 19);
            this.cb_AllConcepts.Name = "cb_AllConcepts";
            this.cb_AllConcepts.Size = new System.Drawing.Size(64, 17);
            this.cb_AllConcepts.TabIndex = 27;
            this.cb_AllConcepts.Text = "TODOS";
            this.cb_AllConcepts.UseVisualStyleBackColor = true;
            this.cb_AllConcepts.CheckedChanged += new System.EventHandler(this.cb_AllConcepts_CheckedChanged);
            // 
            // cmb_Concept8
            // 
            this.cmb_Concept8.FormattingEnabled = true;
            this.cmb_Concept8.Location = new System.Drawing.Point(6, 236);
            this.cmb_Concept8.Name = "cmb_Concept8";
            this.cmb_Concept8.Size = new System.Drawing.Size(190, 21);
            this.cmb_Concept8.TabIndex = 34;
            // 
            // cmb_Concept1
            // 
            this.cmb_Concept1.FormattingEnabled = true;
            this.cmb_Concept1.Location = new System.Drawing.Point(6, 50);
            this.cmb_Concept1.Name = "cmb_Concept1";
            this.cmb_Concept1.Size = new System.Drawing.Size(190, 21);
            this.cmb_Concept1.TabIndex = 27;
            // 
            // cmb_Concept5
            // 
            this.cmb_Concept5.FormattingEnabled = true;
            this.cmb_Concept5.Location = new System.Drawing.Point(6, 158);
            this.cmb_Concept5.Name = "cmb_Concept5";
            this.cmb_Concept5.Size = new System.Drawing.Size(190, 21);
            this.cmb_Concept5.TabIndex = 31;
            // 
            // cmb_Concept7
            // 
            this.cmb_Concept7.FormattingEnabled = true;
            this.cmb_Concept7.Location = new System.Drawing.Point(6, 209);
            this.cmb_Concept7.Name = "cmb_Concept7";
            this.cmb_Concept7.Size = new System.Drawing.Size(190, 21);
            this.cmb_Concept7.TabIndex = 33;
            // 
            // cmb_Concept6
            // 
            this.cmb_Concept6.FormattingEnabled = true;
            this.cmb_Concept6.Location = new System.Drawing.Point(6, 185);
            this.cmb_Concept6.Name = "cmb_Concept6";
            this.cmb_Concept6.Size = new System.Drawing.Size(190, 21);
            this.cmb_Concept6.TabIndex = 32;
            // 
            // grpLaborType
            // 
            this.grpLaborType.Controls.Add(this.cb_AllLabors);
            this.grpLaborType.Controls.Add(this.cb_LaborSalario);
            this.grpLaborType.Controls.Add(this.cb_LaborDirecto);
            this.grpLaborType.Controls.Add(this.cb_LaborIndirecto);
            this.grpLaborType.Location = new System.Drawing.Point(513, 139);
            this.grpLaborType.Name = "grpLaborType";
            this.grpLaborType.Size = new System.Drawing.Size(235, 274);
            this.grpLaborType.TabIndex = 23;
            this.grpLaborType.TabStop = false;
            this.grpLaborType.Text = "Labor Type";
            // 
            // cb_AllLabors
            // 
            this.cb_AllLabors.AutoSize = true;
            this.cb_AllLabors.Location = new System.Drawing.Point(6, 19);
            this.cb_AllLabors.Name = "cb_AllLabors";
            this.cb_AllLabors.Size = new System.Drawing.Size(64, 17);
            this.cb_AllLabors.TabIndex = 4;
            this.cb_AllLabors.Text = "TODOS";
            this.cb_AllLabors.UseVisualStyleBackColor = true;
            this.cb_AllLabors.CheckedChanged += new System.EventHandler(this.cb_AllLabors_CheckedChanged);
            // 
            // cb_LaborSalario
            // 
            this.cb_LaborSalario.AutoSize = true;
            this.cb_LaborSalario.Location = new System.Drawing.Point(6, 100);
            this.cb_LaborSalario.Name = "cb_LaborSalario";
            this.cb_LaborSalario.Size = new System.Drawing.Size(77, 17);
            this.cb_LaborSalario.TabIndex = 3;
            this.cb_LaborSalario.Text = "SALARIAL";
            this.cb_LaborSalario.UseVisualStyleBackColor = true;
            // 
            // cb_LaborDirecto
            // 
            this.cb_LaborDirecto.AutoSize = true;
            this.cb_LaborDirecto.Location = new System.Drawing.Point(6, 77);
            this.cb_LaborDirecto.Name = "cb_LaborDirecto";
            this.cb_LaborDirecto.Size = new System.Drawing.Size(74, 17);
            this.cb_LaborDirecto.TabIndex = 2;
            this.cb_LaborDirecto.Text = "DIRECTO";
            this.cb_LaborDirecto.UseVisualStyleBackColor = true;
            // 
            // cb_LaborIndirecto
            // 
            this.cb_LaborIndirecto.AutoSize = true;
            this.cb_LaborIndirecto.Location = new System.Drawing.Point(6, 54);
            this.cb_LaborIndirecto.Name = "cb_LaborIndirecto";
            this.cb_LaborIndirecto.Size = new System.Drawing.Size(85, 17);
            this.cb_LaborIndirecto.TabIndex = 1;
            this.cb_LaborIndirecto.Text = "INDIRECTO";
            this.cb_LaborIndirecto.UseVisualStyleBackColor = true;
            // 
            // btnSaveFilters
            // 
            this.btnSaveFilters.ActiveBorderThickness = 1;
            this.btnSaveFilters.ActiveCornerRadius = 20;
            this.btnSaveFilters.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnSaveFilters.ActiveForecolor = System.Drawing.Color.White;
            this.btnSaveFilters.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnSaveFilters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveFilters.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSaveFilters.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSaveFilters.BackgroundImage")));
            this.btnSaveFilters.ButtonText = "Save";
            this.btnSaveFilters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveFilters.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveFilters.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnSaveFilters.IdleBorderThickness = 1;
            this.btnSaveFilters.IdleCornerRadius = 20;
            this.btnSaveFilters.IdleFillColor = System.Drawing.Color.White;
            this.btnSaveFilters.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnSaveFilters.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(156)))), ((int)(((byte)(239)))));
            this.btnSaveFilters.Location = new System.Drawing.Point(282, 421);
            this.btnSaveFilters.Margin = new System.Windows.Forms.Padding(5);
            this.btnSaveFilters.Name = "btnSaveFilters";
            this.btnSaveFilters.Size = new System.Drawing.Size(193, 41);
            this.btnSaveFilters.TabIndex = 24;
            this.btnSaveFilters.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSaveFilters.Click += new System.EventHandler(this.btnSaveFilters_Click);
            // 
            // frmFilters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(767, 467);
            this.Controls.Add(this.btnSaveFilters);
            this.Controls.Add(this.grpLaborType);
            this.Controls.Add(this.grpConcepts);
            this.Controls.Add(this.grpDepartments);
            this.Controls.Add(this.grpDate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmFilters";
            this.Text = "Filters for Report";
            this.Shown += new System.EventHandler(this.frmFilters_Shown);
            this.grpDate.ResumeLayout(false);
            this.grpDate.PerformLayout();
            this.grpDepartments.ResumeLayout(false);
            this.grpDepartments.PerformLayout();
            this.grpConcepts.ResumeLayout(false);
            this.grpConcepts.PerformLayout();
            this.grpLaborType.ResumeLayout(false);
            this.grpLaborType.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDate;
        private System.Windows.Forms.Label lbl_cb_days;
        private Bunifu.Framework.UI.BunifuCheckbox cb_Days;
        private Bunifu.Framework.UI.BunifuDatepicker dtp_To;
        private System.Windows.Forms.Label lblTo;
        private Bunifu.Framework.UI.BunifuDatepicker dtp_From;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.GroupBox grpDepartments;
        private System.Windows.Forms.GroupBox grpConcepts;
        private System.Windows.Forms.GroupBox grpLaborType;
        private Bunifu.Framework.UI.BunifuThinButton2 btnSaveFilters;
        private System.Windows.Forms.CheckBox cb_AllLabors;
        private System.Windows.Forms.CheckBox cb_LaborSalario;
        private System.Windows.Forms.CheckBox cb_LaborDirecto;
        private System.Windows.Forms.CheckBox cb_LaborIndirecto;
        private System.Windows.Forms.ComboBox cmb_Department6;
        private System.Windows.Forms.ComboBox cmb_Department8;
        private System.Windows.Forms.ComboBox cmb_Department5;
        private System.Windows.Forms.ComboBox cmb_Department7;
        private System.Windows.Forms.ComboBox cmb_Department4;
        private System.Windows.Forms.ComboBox cmb_Department3;
        private System.Windows.Forms.ComboBox cmb_Department2;
        private System.Windows.Forms.CheckBox cb_AllDepartments;
        private System.Windows.Forms.ComboBox cmb_Department1;
        private System.Windows.Forms.ComboBox cmb_Concept4;
        private System.Windows.Forms.ComboBox cmb_Concept3;
        private System.Windows.Forms.ComboBox cmb_Concept2;
        private System.Windows.Forms.CheckBox cb_AllConcepts;
        private System.Windows.Forms.ComboBox cmb_Concept1;
        private System.Windows.Forms.ComboBox cmb_Concept8;
        private System.Windows.Forms.ComboBox cmb_Concept7;
        private System.Windows.Forms.ComboBox cmb_Concept6;
        private System.Windows.Forms.ComboBox cmb_Concept5;
        private System.Windows.Forms.Button btn_AddDepartment;
        private System.Windows.Forms.Button btn_AddConcept;
        private System.Windows.Forms.Label lbl_CloseDepartment8;
        private System.Windows.Forms.Label lbl_CloseDepartment6;
        private System.Windows.Forms.Label lbl_CloseDepartment7;
        private System.Windows.Forms.Label lbl_CloseDepartment5;
        private System.Windows.Forms.Label lbl_CloseDepartment4;
        private System.Windows.Forms.Label lbl_CloseDepartment3;
        private System.Windows.Forms.Label lbl_CloseDepartment2;
        private System.Windows.Forms.Label lbl_CloseDepartment1;
        private System.Windows.Forms.Label lbl_CloseConcept8;
        private System.Windows.Forms.Label lbl_CloseConcept7;
        private System.Windows.Forms.Label lbl_CloseConcept6;
        private System.Windows.Forms.Label lbl_CloseConcept5;
        private System.Windows.Forms.Label lbl_CloseConcept4;
        private System.Windows.Forms.Label lbl_CloseConcept3;
        private System.Windows.Forms.Label lbl_CloseConcept2;
        private System.Windows.Forms.Label lbl_CloseConcept1;
        private System.Windows.Forms.Label lbl_DepartmentsLeft;
        private System.Windows.Forms.Label lbl_DepartmentsLeftValue;
        private System.Windows.Forms.Label lbl_ConceptsLeftValue;
        private System.Windows.Forms.Label lbl_ConceptsLeft;
    }
}