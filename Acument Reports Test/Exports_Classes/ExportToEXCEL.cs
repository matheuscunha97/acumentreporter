﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Acument_Reports.System_Variables;
using System.Data.SqlClient;

namespace Acument_Reports
{
    class ExportToEXCEL
    {
        frmFilters filters_for_headers = new frmFilters();
        DBConnection dataBaseConn = new DBConnection();

        private string date_Header()
        {
            string date_message;
            if (Filters_Parameters.checkBox_Days.Checked == true && Filters_Parameters.date_From.Value != DateTime.Today && Filters_Parameters.date_To.Value != DateTime.Today)
            {
               date_message = "Date:                     " + Filters_Parameters.date_From.Value.ToString("yyyy-MM-dd") + " to " + Filters_Parameters.date_To.Value.ToString("yyyy-MM-dd") + ".";
            }
            else //Date not applied on filter
            {
                date_message = "Date:                     (NOT APPLIED).";
            }
            return date_message;
        } //Get the date passed to the filter to the header

        private string department_Header()
        {
            string department_message;

            if (Filters_Parameters.cb_allDepartments.Checked == true)
            {
                department_message = "Departments:          " + filters_for_headers.selectAllDepartments_ForHeaderExport() + ".";
            }
            else if (!string.IsNullOrEmpty(Filters_Parameters.selected_ConceptsFilters))
            {
                Filters_Parameters.selected_DepartmentsFilters = Filters_Parameters.selected_DepartmentsFilters.Remove(Filters_Parameters.selected_DepartmentsFilters.Length - 1);
                department_message = "Departments:          " + Filters_Parameters.selected_DepartmentsFilters + ".";
            }
            else
            {
                department_message = "Departments:        (NOT APPLIED).";
            }

            return department_message;
        }

        private string concept_Header()
        {
            string concept_message;

            if (Filters_Parameters.cb_allConcepts.Checked == true)
            {
                concept_message = "Concepts:             " + filters_for_headers.selectAllConcepts_ForHeaderExport() + ".";
            }
            else if (!string.IsNullOrEmpty(Filters_Parameters.selected_ConceptsFilters))
            {
                Filters_Parameters.selected_ConceptsFilters = Filters_Parameters.selected_ConceptsFilters.Remove(Filters_Parameters.selected_ConceptsFilters.Length - 1);
                concept_message = "Concepts:             " + Filters_Parameters.selected_ConceptsFilters + ".";
            }
            else
            {
                concept_message = "Concepts:             (NOT APPLIED).";
            }

            return concept_message;
        }

        private string labor_header()
        {
            string labor_message;
            if (Filters_Parameters.cb_allLabors.Checked == true)
            {
                labor_message = "Labors:               " + filters_for_headers.selectAllLabors_ForHeaderExport() + ".";
            }
            else if (!string.IsNullOrEmpty(Filters_Parameters.selected_LaborsFilters))
            {
                Filters_Parameters.selected_LaborsFilters = Filters_Parameters.selected_LaborsFilters.Remove(Filters_Parameters.selected_LaborsFilters.Length - 1);
                labor_message = "Labors:               " + Filters_Parameters.selected_LaborsFilters + ".";
            }
            else
            {
                labor_message = "Labors:               (NOT APPLIED).";
            }

            return labor_message;
        }

        public void Export(ListView listView)
        {
            //Code to export the data from the ListView to .xlsx option (EXCEL) using Interop reference
            try
            {
                using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx", ValidateNames = true }) //open the dialog to the user browse the file path destiny
                {
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        //Create the header
                        Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
                        Workbook wb = app.Workbooks.Add(XlSheetType.xlWorksheet);
                        Worksheet ws = (Worksheet)app.ActiveSheet;
                        app.Visible = false;

                        //Title
                        ws.Cells[1, 1] = "Hours by Labor Account (Excel)";
                        ws.Cells[1, 1].EntireRow.Font.Bold = true;
                        ws.Range[ws.Cells[1, 1], ws.Cells[1, 13]].Merge();
                        ws.Cells[1, 1].Font.Size = 12;
                        ws.Cells[1, 1].Font.Name = "Arial Unicode";

                        //Date
                        ws.Cells[2, 1] = date_Header();
                        ws.Range[ws.Cells[2, 1], ws.Cells[2, 13]].Merge();
                        ws.Cells[2, 1].Font.Size = 8;
                        ws.Cells[2, 1].Font.Name = "Arial Unicode";

                        //Departments
                        ws.Cells[3, 1] = department_Header();
                        ws.Range[ws.Cells[3, 1], ws.Cells[3, 13]].Merge();
                        ws.Cells[3, 1].Font.Size = 8;
                        ws.Cells[3, 1].Font.Name = "Arial Unicode";

                        // Concepts
                        ws.Cells[4, 1] = concept_Header();
                        ws.Range[ws.Cells[4, 1], ws.Cells[4, 13]].Merge();
                        ws.Cells[4, 1].Font.Size = 8;
                        ws.Cells[4, 1].Font.Name = "Arial Unicode";

                        // Labors
                        ws.Cells[5, 1] = labor_header();
                        ws.Range[ws.Cells[5, 1], ws.Cells[5, 13]].Merge();
                        ws.Cells[5, 1].Font.Size = 8;
                        ws.Cells[5, 1].Font.Name = "Arial Unicode";

                        //Executed on
                        ws.Cells[6, 1] = "Executed on:         " + DateTime.Now;
                        ws.Range[ws.Cells[6, 1], ws.Cells[6, 13]].Merge();
                        ws.Cells[6, 1].Font.Size = 8;
                        ws.Cells[6, 1].Font.Name = "Arial Unicode";

                        //Creating bottom border
                        for (int col = 1; col <= 13; col++)
                        {
                            ws.Cells[6, col].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        }

                        //Creating right border
                        for (int row = 1; row <= 6; row++)
                        {
                            ws.Cells[row, 13].Borders[XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        }

                        //Creating the blank between the header and body
                        ws.Range[ws.Cells[7, 1], ws.Cells[7, 13]].Merge();
                        //End header

                        //Columns names body-header
                        ws.Cells[8, 1] = "Field 1";
                        ws.Cells[8, 2] = "Field 2";
                        ws.Cells[8, 3] = "Field 3";
                        ws.Cells[8, 4] = "Account";
                        ws.Cells[8, 5] = "Department";
                        ws.Cells[8, 6] = "Labor Type";
                        ws.Cells[8, 7] = "Nº Concept";
                        ws.Cells[8, 8] = "Concept";
                        ws.Cells[8, 9] = "Money";
                        ws.Cells[8, 10] = "Hours";
                        ws.Cells[8, 11] = "Wages";
                        ws.Cells[8, 12] = "Days";
                        ws.Cells[8, 13] = "Date";

                        int i = 9;
                        int cont_i = 9; //Counter to see on which row the file ends to check on which row the Summaryze header should start

                        //Loop to fill each row of the file with the database data
                        foreach (ListViewItem item in listView.Items)
                        {
                            ws.Cells[i, 1] = item.SubItems[0].Text;
                            ws.Cells[i, 2] = item.SubItems[1].Text;
                            ws.Cells[i, 3] = item.SubItems[2].Text;
                            ws.Cells[i, 4] = item.SubItems[3].Text;
                            ws.Cells[i, 5] = item.SubItems[4].Text;
                            ws.Cells[i, 6] = item.SubItems[5].Text;
                            ws.Cells[i, 7] = item.SubItems[6].Text;
                            ws.Cells[i, 8] = item.SubItems[7].Text;
                            ws.Cells[i, 9] = item.SubItems[8].Text;
                            ws.Cells[i, 10] = item.SubItems[9].Text;
                            ws.Cells[i, 11] = item.SubItems[10].Text;
                            ws.Cells[i, 12] = item.SubItems[11].Text;
                            ws.Cells[i, 13] = item.SubItems[12].Text;

                            //Increase counters
                            i++;
                            cont_i++;
                        }

                        int sumColHead = 8;

                        //Use the query of summaryze to create itself on the bottom of the file
                        try
                        {
                            using(SqlConnection conn = dataBaseConn.OpenSQLConn())
                            {
                                SqlDataAdapter da = new SqlDataAdapter(Filters_Parameters.summary_query, conn);

                                System.Data.DataTable dt = new System.Data.DataTable();
                                da.Fill(dt);
                                
                                //Summary Header
                                for (int sumHead = 1; sumHead < dt.Columns.Count + 1; sumHead++)
                                {
                                    ws.Cells[cont_i + 1, sumColHead] = dt.Columns[sumHead - 1].ColumnName;
                                    ws.Cells[cont_i + 1, sumColHead].EntireRow.Font.Bold = true;
                                    sumColHead++;
                                }

                                //Summary Body
                                for(int j = 0; j < dt.Rows.Count; j++)
                                {
                                    for (int sumColBody = 0; sumColBody < dt.Columns.Count; sumColBody++)
                                    {
                                        ws.Cells[cont_i + 2, 8 + sumColBody] = dt.Rows[j].ItemArray[sumColBody].ToString(); //8 + sumColBody = start writing on the column 8 and go on the next columns of datatable
                                    }
                                    cont_i++;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //Message if there's an error
                            MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        ////Creating Pay Code types
                        //ws.Cells[i + 2, 8] = "Job Differential Pay";
                        //ws.Cells[i + 3, 8] = "Overtime";
                        //ws.Cells[i + 4, 8] = "Regular";
                        //ws.Cells[i + 5, 8] = "SD.20";
                        //ws.Cells[i + 6, 8] = "SD.30";
                        //ws.Cells[i + 7, 8] = "Vacation";

                        ////Creating Borders to Summary
                        //for(int colSumHeader = 8; colSumHeader <=12; colSumHeader++)
                        //{
                        //    //Borders-header
                        //    ws.Cells[i + 1, colSumHeader].Borders[XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //    ws.Cells[i + 1, colSumHeader].Borders[XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //    ws.Cells[i + 1, colSumHeader].Borders[XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //    ws.Cells[i + 1, colSumHeader].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        //    //Border-bottom-content
                        //    ws.Cells[i + 7, colSumHeader].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //}

                        //for (int rowSumContent = 2; rowSumContent <= 7; rowSumContent++)
                        //{
                        //    //Border-left-content
                        //    ws.Cells[i + rowSumContent, 8].Borders[XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        //    //Border-right-content
                        //    ws.Cells[i + rowSumContent, 12].Borders[XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //}
                        ////End Summary Borders

                        //Merging the rest of the file
                        ws.Range[ws.Cells[1, 14], ws.Cells[i + 7, 22]].Merge();

                        //Saving file
                        wb.SaveAs(sfd.FileName, XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, true, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                        app.Quit();
                        MessageBox.Show("Your data has been exported with success.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information); //Sucess message if success
                    }
                }
            }
            catch (Exception ex)
            {
                //Error message if there's an error
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
