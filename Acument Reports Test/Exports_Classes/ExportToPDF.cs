﻿using Acument_Reports.System_Variables;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace Acument_Reports
{
    class ExportToPDF : ITextEvents
    {
        DBConnection dataBaseConn = new DBConnection();
        frmFilters filters_for_bottom = new frmFilters();

        private string date_Bottom()
        {
            string date_message;
            if (Filters_Parameters.checkBox_Days.Checked == true && Filters_Parameters.date_From.Value != DateTime.Today && Filters_Parameters.date_To.Value != DateTime.Today)
            {
                date_message = "Date:                     " + Filters_Parameters.date_From.Value.ToString("yyyy-MM-dd") + " to " + Filters_Parameters.date_To.Value.ToString("yyyy-MM-dd") + ".";
            }
            else //Date not applied on filter
            {
                date_message = "Date:                     (NOT APPLIED).";
            }
            return date_message;
        } //Get the date passed to the filter to the header

        private string department_Bottom()
        {
            string department_message;

            if (Filters_Parameters.cb_allDepartments.Checked == true)
            {
                department_message = "Departments:          " + filters_for_bottom.selectAllDepartments_ForHeaderExport() + ".";
            }
            else if (!string.IsNullOrEmpty(Filters_Parameters.selected_ConceptsFilters))
            {
                Filters_Parameters.selected_DepartmentsFilters = Filters_Parameters.selected_DepartmentsFilters.Remove(Filters_Parameters.selected_DepartmentsFilters.Length - 1);
                department_message = "Departments:          " + Filters_Parameters.selected_DepartmentsFilters + ".";
            }
            else
            {
                department_message = "Departments:        (NOT APPLIED).";
            }

            return department_message;
        }

        private string concept_Bottom()
        {
            string concept_message;

            if (Filters_Parameters.cb_allConcepts.Checked == true)
            {
                concept_message = "Concepts:             " + filters_for_bottom.selectAllConcepts_ForHeaderExport() + ".";
            }
            else if (!string.IsNullOrEmpty(Filters_Parameters.selected_ConceptsFilters))
            {
                Filters_Parameters.selected_ConceptsFilters = Filters_Parameters.selected_ConceptsFilters.Remove(Filters_Parameters.selected_ConceptsFilters.Length - 1);
                concept_message = "Concepts:             " + Filters_Parameters.selected_ConceptsFilters + ".";
            }
            else
            {
                concept_message = "Concepts:             (NOT APPLIED).";
            }

            return concept_message;
        }

        private string labor_Bottom()
        {
            string labor_message;
            if (Filters_Parameters.cb_allLabors.Checked == true)
            {
                labor_message = "Labors:               " + filters_for_bottom.selectAllLabors_ForHeaderExport() + ".";
            }
            else if (!string.IsNullOrEmpty(Filters_Parameters.selected_LaborsFilters))
            {
                Filters_Parameters.selected_LaborsFilters = Filters_Parameters.selected_LaborsFilters.Remove(Filters_Parameters.selected_LaborsFilters.Length - 1);
                labor_message = "Labors:               " + Filters_Parameters.selected_LaborsFilters + ".";
            }
            else
            {
                labor_message = "Labors:               (NOT APPLIED).";
            }

            return labor_message;
        }

        public float[] GetHeaderWidths(Font font, params string[] headers)
        {
            var total = 0;
            var columns = headers.Length;
            var widths = new int[columns];
            for (var i = 0; i < columns; ++i)
            {
                var w = font.GetCalculatedBaseFont(true).GetWidth(headers[i]);
                total += w;
                widths[i] = w;
            }
            var result = new float[columns];
            for (var i = 0; i < columns; ++i)
            {
                result[i] = (float)widths[i] / total * 100;
            }
            return result;
        }

        public void Export(ListView listview)
        {
            try
            {
                //Adding Header row structure
                List<String> headers = new List<String>();

                //Add each column on the list
                foreach (ColumnHeader column in listview.Columns)
                {
                    headers.Add(column.Text);
                }

                //Creating iTextSharp Table from the DataTable data
                iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
                PdfPTable pdfTable = new PdfPTable(headers.ToArray().Length);

                //pdfTable.DefaultCell.Padding = 3;
                pdfTable.WidthPercentage = 100;
                pdfTable.SetWidths(GetHeaderWidths(baseFontNormal, headers.ToArray()));

                //pdfTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfTable.DefaultCell.BorderWidth = 1;

                //Adding columns on header
                for(int i = 0; i < headers.ToArray().Length; i++)
                {
                    pdfTable.AddCell(new PdfPCell(new Phrase(headers[i], baseFontNormal)));
                }

                //Adding DataRow
                foreach (ListViewItem itemRow in listview.Items)
                {
                    for (int i = 0; i < itemRow.SubItems.Count; i++)
                    {
                        pdfTable.AddCell(itemRow.SubItems[i].Text);
                    }
                }

                //Create used filters part
                PdfPCell used_Dates = new PdfPCell(new Phrase(date_Bottom(), baseFontNormal));
                used_Dates.VerticalAlignment = Element.ALIGN_BOTTOM;
                pdfTable.AddCell(used_Dates);

                //Create Summary table
                PdfPTable pdfSummaryTable = new PdfPTable(5);
                pdfSummaryTable.DefaultCell.Padding = 2;

                //Setting Summaryze Title
                iTextSharp.text.Font baseFontVeryBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 25f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
                PdfPCell SummaryTitle = new PdfPCell(new Phrase("Summary", baseFontVeryBig));
                SummaryTitle.HorizontalAlignment = Element.ALIGN_CENTER;
                SummaryTitle.VerticalAlignment = Element.ALIGN_BOTTOM;
                SummaryTitle.Colspan = 5;
                SummaryTitle.Border = 0;

                //Adding summary title row
                pdfSummaryTable.AddCell(SummaryTitle);
                
                try
                {
                    using (SqlConnection conn = dataBaseConn.OpenSQLConn())
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Filters_Parameters.summary_query, conn);

                        System.Data.DataTable dt = new System.Data.DataTable();
                        da.Fill(dt);

                        //Summary Header
                        foreach (DataColumn c in dt.Columns)
                        {
                            pdfSummaryTable.AddCell(new Phrase(c.ColumnName, baseFontNormal));
                        }

                        //Summary Body
                        foreach (DataRow r in dt.Rows)
                        {
                            if(dt.Rows.Count > 0)
                            {
                                pdfSummaryTable.AddCell(new Phrase(r[0].ToString(), baseFontNormal));
                                pdfSummaryTable.AddCell(new Phrase(r[1].ToString(), baseFontNormal));
                                pdfSummaryTable.AddCell(new Phrase(r[2].ToString(), baseFontNormal));
                                pdfSummaryTable.AddCell(new Phrase(r[3].ToString(), baseFontNormal));
                                pdfSummaryTable.AddCell(new Phrase(r[4].ToString(), baseFontNormal));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Message if there's an error
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //Exporting to PDF
                using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "PDF|*.pdf", ValidateNames = true })
                {
                    string fileName = sfd.FileName;
                    DateTime fileCreationDatetime = DateTime.Now;
                    fileName = string.Format("{0}.pdf", fileCreationDatetime.ToString(@"yyyy/MM/dd") + "_" + fileCreationDatetime.ToString(@"HHmmss"));

                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        using (FileStream stream = new FileStream(sfd.FileName, FileMode.Create))
                        {
                            //step 1
                            Document pdfDoc = new Document(iTextSharp.text.PageSize.A4.Rotate(), 10f, 10f, 110f, 45f);
                            //step 2
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, stream);

                            pdfWriter.PageEvent = new ITextEvents();
                            //open the stream
                            pdfDoc.Open();
                        
                           // pdfDoc.Add(cb);
                            pdfDoc.Add(pdfTable);

                            pdfSummaryTable.SpacingBefore = 30;
                            pdfDoc.Add(pdfSummaryTable);

                            pdfDoc.Close();
                            stream.Close();
                            MessageBox.Show("Data has been exported with success.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
