﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acument_Reports_Test
{
    class ExportToCSV
    {
        public async void Export(ListView listView)
        {
            //Code to export the data from the ListView to .CSV option
            try
            {
                using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "CSV|*.csv", ValidateNames = true }) //Open the dialog to the user browse the path destiny for the file
                {
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        using (StreamWriter sw = new StreamWriter(new FileStream(sfd.FileName, FileMode.Create), Encoding.UTF8))
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendLine("Field 1,Field 2,Field 3,Account,Labor Type,Department,Nº Concept,Concept,Money,Hours,Wages,Days,Date");//Create the header of the file

                            //Loop to go through the ListView and create a new row by the .AppendLine method with the data
                            foreach (ListViewItem item in listView.Items)
                            {
                                sb.AppendLine(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}",
                                                            item.SubItems[0].Text,
                                                            item.SubItems[1].Text,
                                                            item.SubItems[2].Text,
                                                            item.SubItems[3].Text,
                                                            item.SubItems[4].Text,
                                                            item.SubItems[5].Text,
                                                            item.SubItems[6].Text,
                                                            item.SubItems[7].Text,
                                                            item.SubItems[8].Text,
                                                            item.SubItems[9].Text,
                                                            item.SubItems[10].Text,
                                                            item.SubItems[11].Text,
                                                            item.SubItems[12].Text));
                            }
                            await sw.WriteLineAsync(sb.ToString());
                            MessageBox.Show("Your data has been exported with success.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information); //Sucess message if success
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Error message if there's an error
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
