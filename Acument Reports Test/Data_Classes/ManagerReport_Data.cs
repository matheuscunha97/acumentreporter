﻿using Acument_Reports_Test;
using Bunifu.Framework.UI;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acument_Reports
{
    class ManagerReport_Data
    {
        DBConnection dataBaseConn = new DBConnection(); //Db Class Instance

        public void Fill(SqlDataReader dataReader, ListView listView)
        {
            //Loop to read each line of the database
            while (dataReader.Read())
            {
                try
                {
                    //Create a new item for each row to the ListView
                    ListViewItem item = new ListViewItem(dataReader[0].ToString());
                    item.SubItems.Add(dataReader[1].ToString());
                    item.SubItems.Add(dataReader[2].ToString());
                    item.SubItems.Add(dataReader[3].ToString());
                    item.SubItems.Add(dataReader[4].ToString());
                    item.SubItems.Add(dataReader[5].ToString());
                    item.SubItems.Add(dataReader[6].ToString());
                    item.SubItems.Add(dataReader[7].ToString());
                    item.SubItems.Add(dataReader[8].ToString());
                    item.SubItems.Add(dataReader[9].ToString());
                    item.SubItems.Add(dataReader[10].ToString());
                    item.SubItems.Add(dataReader[11].ToString());
                    item.SubItems.Add(DateTime.Parse(dataReader[12].ToString()).ToString("yyyy-MM-dd"));

                    //Add the row to the listview
                    listView.Items.Add(item);
                }
                catch (Exception ex)
                {
                    //Message if there's an error
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        } //Add data from the DB to Listview

        public void populateManagerRepoLST(ListView listView)
        {
            listView.Items.Clear();

            try
            {
                using (SqlConnection conn = dataBaseConn.OpenSQLConn()) //Open connection
                {
                    SqlCommand Query = new SqlCommand("SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                                                      "FROM empleados e " +
                                                      "INNER JOIN fun_u_01 fun " +
                                                      "ON fun.empresa = 1 " +
                                                      "and fun.fch_pago BETWEEN convert(datetime, '2017-11-30') AND convert(datetime, '2018-02-10') " +
                                                      "and e.codigo = fun.codigo " +
                                                      "INNER JOIN cuentas c " +
                                                      "ON c.empresa = fun.empresa " +
                                                      "GROUP BY c.nomcuenta, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                                                      "ORDER BY fun.fch_pago DESC", conn);

                    //Load the data from the database dataReader
                    SqlDataReader dataReader = Query.ExecuteReader();
                    //Fill the Listview with dataReader content
                    Fill(dataReader, listView);
                }
            }
            catch (Exception ex)
            {
                //Message if there's an error
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } //Query the data from database

        public void execute_with_filter(string query, ListView listView)
        {
            listView.Items.Clear();
            try
            {
                using (SqlConnection conn = dataBaseConn.OpenSQLConn())//Open connection
                {
                    SqlCommand Query = new SqlCommand(query, conn);

                    //Load the data from the database on the ListView
                    SqlDataReader dataReader = Query.ExecuteReader();
                    Fill(dataReader, listView);
                }
            }
            catch (Exception ex)
            {
                //Message if there's an error
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //If there's no records between the dates, display a message.
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("There's no records between this intervals.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void execute_without_filter(ListView listView)
        {
            listView.Items.Clear();
            try
            {
                using (SqlConnection conn = dataBaseConn.OpenSQLConn())//Open connection
                {
                    SqlCommand Query = new SqlCommand("SELECT '00','00','XXG', c.cuenta1, fun.centro, e.manoobra, fun.num_conc, fun.concepto, SUM(fun.importe), SUM(fun.horas), SUM(e.sueldo), COUNT(fun.dias), fun.fch_pago " +
                                                      "FROM empleados e " +
                                                      "INNER JOIN fun_u_01 fun " +
                                                      "ON fun.empresa = 1 " +
                                                      "and e.codigo = fun.codigo " +
                                                      "INNER JOIN cuentas c " +
                                                      "ON c.empresa = fun.empresa " +
                                                      "GROUP BY c.cuenta1, e.manoobra, fun.centro, fun.num_conc, fun.concepto, fun.importe, fun.horas, e.sueldo, fun.dias, fun.fch_pago " +
                                                      "ORDER BY fun.fch_pago, fun.centro, c.cuenta1 ASC", conn);

                    //Load the data from the database on the ListView
                    SqlDataReader dataReader = Query.ExecuteReader();
                    Fill(dataReader, listView);
                }
            }
            catch (Exception ex)
            {
                //Message if there's an error
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //If there's no records between the dates, display a message.
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("There's no records between this intervals.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        
    }
}
