﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Acument_Reports;

namespace Acument_Reports_Test
{
    class LstMainRepoData
    {
        DBConnection dataBaseConn = new DBConnection(); //Class containing PostgreSQL instance.

        public void Fill(NpgsqlDataReader dataReader, ListView listView)
        {
            //Loop to read each line of the database
            while (dataReader.Read())
            {
                //Create a new item for each row to the ListView
                ListViewItem item = new ListViewItem(dataReader["field_one_rep"].ToString());
                item.SubItems.Add(dataReader["field_two_rep"].ToString());
                item.SubItems.Add(dataReader["field_three_rep"].ToString());
                item.SubItems.Add(dataReader["acc_rep"].ToString());
                item.SubItems.Add(dataReader["area_rep"].ToString());
                item.SubItems.Add(dataReader["dept_rep"].ToString());
                item.SubItems.Add(dataReader["labor_type_rep"].ToString());
                item.SubItems.Add(dataReader["payc_rep"].ToString());
                item.SubItems.Add(dataReader["money_rep"].ToString().Substring(1));
                item.SubItems.Add(dataReader["hours_rep"].ToString().Replace(',', '.'));
                item.SubItems.Add(dataReader["wages_rep"].ToString().Substring(1));
                item.SubItems.Add(dataReader["days_rep"].ToString());
                //item.SubItems.Add(DateTime.Parse(dataReader["dat_beg_rep"].ToString()).ToString("dd/MM/yyyy"));
                //item.SubItems.Add(DateTime.Parse(dataReader["dat_end_rep"].ToString()).ToString("dd/MM/yyyy"));

                //Add the row to the listview
                listView.Items.Add(item);
            }
        } //Add data from the DB to Listview

        public void populateMainRepoLST(ListView listView)
        {
            listView.Items.Clear();

            try
            {
                using (NpgsqlConnection conn = dataBaseConn.Open()) //Open connection
                {
                    NpgsqlCommand Query = new NpgsqlCommand("SELECT * FROM final_report", conn);

                    //Load the data from the database on the ListView
                    NpgsqlDataReader dataReader = Query.ExecuteReader();
                    Fill(dataReader, listView);
                }
            }
            catch (Exception ex)
            {
                //Message if there's an error
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } //Query the data from database
    }
}
