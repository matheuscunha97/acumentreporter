CREATE TABLE IF NOT EXISTS sights(
    sg_id SERIAL PRIMARY KEY,
    sg_name VARCHAR(60) NOT NULL,
    sg_street VARCHAR(80) NOT NULL,
    sg_comp VARCHAR(20) NOT NULL,
    sg_state CHAR(3) NOT NULL,
    sg_country VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS reports(
    rp_cod SERIAL PRIMARY KEY,
    rp_dat_begin TIMESTAMP NOT NULL,
    rp_dat_end TIMESTAMP NOT NULL,
    rp_hextras_total FLOAT NOT NULL,
    rp_hvaca_total FLOAT NOT NULL,
    rp_htotals FLOAT NOT NULL,
    rp_vlextra_total FLOAT NOT NULL,
    rp_vlvaca_total FLOAT NOT NULL,
    rp_vltotals FLOAT NOT NULL
);

CREATE TABLE IF NOT EXISTS users(
    user_id SERIAL PRIMARY KEY,
    username VARCHAR(20) NOT NULL,
    password VARCHAR(30) NOT NULL
);

-- Function to import the data from .csv file --
\COPY reports(rp_dat_begin,rp_dat_end,rp_hextras_total,rp_hvaca_total,rp_htotals,rp_vlextra_total,rp_vlvaca_total,rp_vltotals) FROM 'C:\Users\Cunhamath\Desktop\Projects\ReportTEST2.csv' DELIMITER ';' CSV;

-- Tests Inserts --
INSERT INTO users(username, password) VALUES('user-test', '123');

-- Alter tables --
ALTER TABLE reports
    ALTER COLUMN rp_dat_begin type timestamp without time zone 
    using current_date + rp_dat_begin;

ALTER TABLE reports
    ALTER COLUMN rp_dat_end type timestamp without time zone 
    using current_date + rp_dat_end;