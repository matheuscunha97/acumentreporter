-- Database: acument-reporter
CREATE TABLE IF NOT EXISTS final_report(
	field_one_rep CHAR(2),
	field_two_rep CHAR(2),
    field_three_rep CHAR(3) NOT NULL,
    acc_rep INTEGER NOT NULL,
    area_rep CHAR(5) NOT NULL,
    cosc_rep CHAR(6) NOT NULL,
    field_seven_rep CHAR(5) NOT NULL,
    payc_rep VARCHAR(30) NOT NULL,
    money_rep TEXT NOT NULL,
    hours_rep TEXT NOT NULL,
    wages_rep TEXT NOT NULL,
    days_rep TEXT NOT NULL,
    dat_beg_rep DATE NOT NULL,
    dat_end_rep DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS users(
    user_id SERIAL PRIMARY KEY,
    username VARCHAR(20) NOT NULL,
    password VARCHAR(30) NOT NULL
);

--Users inserts--
INSERT INTO users(username, password) VALUES('david', 'admin');

--Filters--
SELECT * FROM final_report
WHERE dat_beg_rep BETWEEN '%'27/11/2017'%' AND '%'29/11/2017'%'
ORDER BY cosc_rep ASC;-- Filter Dates

SELECT * FROM final_report
WHERE dat_beg_rep BETWEEN '%'27/11/2017'%' AND '%'29/11/2017'%' AND cosc_rep = '221119' AND payc_rep = 'Regular'
ORDER BY cosc_rep ASC; -- All Filters

--Fill combobox--
SELECT DISTINCT cosc_rep
FROM final_report ORDER BY cosc_rep ASC; -- Select to fill the Cost Center combobox.

SELECT DISTINCT payc_rep
FROM final_report ORDER BY payc_rep ASC; -- Select to fill the Pay Code combobox.


-- Copying the data from the spreadsheet --
\COPY final_report (field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) FROM 'C:\Users\Cunhamath\Desktop\Projects\Acument Reports Test\Files\ReportTEST2.csv' DELIMITER ';' CSV HEADER;

--Alter Tables--
ALTER TABLE final_report RENAME cosc_rep TO dept_rep;
ALTER TABLE final_report RENAME field_seven_rep TO labor_type_rep;

--Updates--
UPDATE users SET username = 'admin', password='david' WHERE user_id=1;
UPDATE users SET password='manhulas' WHERE username = 'matheus';

INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191578,'5014' ,'222410','90610','Regular','$0.00','8','$148.00','0.00','27/11/2017','27/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221119','90110','Overtime','$0.00','5,1','$169.38','0.00','27/11/2017','27/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221119','90110','Regular','$0.00','37,3','$760.70','0.00','27/11/2017','27/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221119','90110','Vacation','$0.00','8','$161.36','0.00','27/11/2017','27/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221150','90110','Vacation','$0.00','8','$144.40','0.00','27/11/2017','27/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221219','90110','Overtime','$0.00','6','$162.45','0.00','28/11/2017','28/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221219','90110','Regular','$0.00','34','$534.80','0.00','28/11/2017','28/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221239','90110','Job Differential Pay','$1.50','0','$1.50','0.00','28/11/2017','28/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221239','90110','Overtime','$0.00','0,1','$2.71','0.00','28/11/2017','28/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221239','90110','Regular','$0.00','104','$1,614.87','0.00','28/11/2017','28/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221239','90110','Vacation','$0.00','25','$421.67','0.00','28/11/2017','28/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221305','90110','Regular','$0.00','40,5','$746.28','0.00','28/11/2017','28/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221305','90110','SD.20','$0.00','16','$3.20','0.00','28/11/2017','28/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221305','90110','SD.30','$0.00','8','$2.40','0.00','28/11/2017','28/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221339','90110','Regular','$0.00','16','$194.24','0.00','29/11/2017','29/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'221700','90110','Regular','$0.00','24','$389.04','0.00','29/11/2017','29/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000191820,'5014' ,'222410','90610','Regular','$0.00','40','$785.28','0.00','29/11/2017','29/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000231586,'5014' ,'222430','90610','Regular','$0.00','72,1','$1,472.98','0.00','30/11/2017','30/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000231586,'5014' ,'222430','90610','SD.20','$0.00','16,1','$3.22','0.00','30/11/2017','30/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000231586,'5014' ,'222430','90610','SD.30','$0.00','16','$4.80','0.00','30/11/2017','30/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000233821,'5014' ,'222321','90610','Regular','$0.00','10,1','$186.85','0.00','30/11/2017','30/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000233821,'5014' ,'222475','90610','Overtime','$0.00','0,2','$3.47','0.00','30/11/2017','30/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000233821,'5014' ,'222475','90610','Regular','$0.00','34','$440.30','0.00','30/11/2017','30/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000233821,'5014' ,'222475','90610','SD.30','$0.00','8','$2.40','0.00','30/11/2017','30/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000233821,'5014' ,'222475','90610','Vacation','$0.00','8','$117.52','0.00','30/11/2017','30/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000233821,'5014' ,'222484','90610','Regular','$0.00','8,1','$135.35','0.00','30/11/2017','30/11/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822148,'5014' ,'221249','90110','Overtime','$0.00','0,6','$15.46','0.00','01/12/2017','01/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822148,'5014' ,'221249','90110','Regular','$0.00','71,4','$971.85','0.00','01/12/2017','01/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822148,'5014' ,'221249','90610','Regular','$0.00','10','$129.60','0.00','01/12/2017','01/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822148,'5014' ,'222440','90610','Regular','$0.00','58,1','$985.85','0.00','01/12/2017','01/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822148,'5014' ,'222440','90610','SD.20','$0.00','8','$1.60','0.00','01/12/2017','01/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822148,'5014' ,'222440','90610','SD.30','$0.00','8,1','$2.43','0.00','01/12/2017','01/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822751,'5014' ,'221119','90110','Regular','$0.00','32,6','$617.23','0.00','01/12/2017','01/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822751,'5014' ,'221119','90110','SD.30','$0.00','32,6','$9.78','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822751,'5014' ,'221219','90110','Regular','$0.00','10','$185.00','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822751,'5014' ,'221219','90110','SD.30','$0.00','10','$3.00','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822751,'5014' ,'221239','90110','Regular','$0.00','80,2','$1,171.32','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822751,'5014' ,'221239','90110','SD.30','$0.00','80,2','$24.06','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822751,'5014' ,'222410','90610','Regular','$0.00','10,2','$0.00','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822751,'5014' ,'222410','90610','SD.30','$0.00','10,2','$3.06','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822946,'5014' ,'221119','90110','Regular','$0.00','42,1','$648.52','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822946,'5014' ,'221119','90110','SD.20','$0.00','42,1','$8.42','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822946,'5014' ,'221219','90110','Regular','$0.00','30','$426.20','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822946,'5014' ,'221219','90110','SD.20','$0.00','30','$6.00','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822946,'5014' ,'221239','90110','Regular','$0.00','64,9','$933.59','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822946,'5014' ,'221239','90110','SD.20','$0.00','64,9','$12.98','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822946,'5014' ,'222410','90610','Regular','$0.00','16','$227.84','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000822946,'5014' ,'222410','90610','SD.20','$0.00','16','$3.20','0.00','02/12/2017','02/12/2017');
INSERT INTO final_report(field_one_rep,field_two_rep,field_three_rep,acc_rep,area_rep,cosc_rep,field_seven_rep,payc_rep,money_rep,hours_rep,wages_rep,days_rep,dat_beg_rep,dat_end_rep) VALUES('0' ,'0' ,'XXG',1000823092,'5014' ,'228250','90860','Regular','$0.00','9','$147.51','0.00','02/12/2017','02/12/2017');